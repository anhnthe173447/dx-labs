/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author 84989
 */
public class StatusIntern {

    private int status_intern;
    private String status_name;

    public StatusIntern() {
    }

    public StatusIntern(int status_intern, String status_name) {
        this.status_intern = status_intern;
        this.status_name = status_name;
    }

    public int getStatus_intern() {
        return status_intern;
    }

    public void setStatus_intern(int status_intern) {
        this.status_intern = status_intern;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }
    
    
}
