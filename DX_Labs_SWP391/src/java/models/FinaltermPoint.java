/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author admin
 */
public class FinaltermPoint {

    public int finalTermPID;
    public String fullName;
    public Float majorPoints;
    public Float softSkillPoints;
    public Float attitudePoints;
    public String comment;
    public int internID;

    public FinaltermPoint() {
    }

    public FinaltermPoint(int finalTermPID, String fullName, Float majorPoints, Float softSkillPoints, Float attitudePoints, String comment, int internID) {
        this.finalTermPID = finalTermPID;
        this.fullName = fullName;
        this.majorPoints = majorPoints;
        this.softSkillPoints = softSkillPoints;
        this.attitudePoints = attitudePoints;
        this.comment = comment;
        this.internID = internID;
    }

    public int getFinalTermPID() {
        return finalTermPID;
    }

    public void setFinalTermPID(int finalTermPID) {
        this.finalTermPID = finalTermPID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Float getMajorPoints() {
        return majorPoints;
    }

    public void setMajorPoints(Float majorPoints) {
        this.majorPoints = majorPoints;
    }

    public Float getSoftSkillPoints() {
        return softSkillPoints;
    }

    public void setSoftSkillPoints(Float softSkillPoints) {
        this.softSkillPoints = softSkillPoints;
    }

    public Float getAttitudePoints() {
        return attitudePoints;
    }

    public void setAttitudePoints(Float attitudePoints) {
        this.attitudePoints = attitudePoints;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getInternID() {
        return internID;
    }

    public void setInternID(int internID) {
        this.internID = internID;
    }

    @Override
    public String toString() {
        return "FinaltermPoint{" + "finalTermPID=" + finalTermPID + ", fullName=" + fullName + ", majorPoints=" + majorPoints + ", softSkillPoints=" + softSkillPoints + ", attitudePoints=" + attitudePoints + ", comment=" + comment + ", internID=" + internID + '}';
    }

}
