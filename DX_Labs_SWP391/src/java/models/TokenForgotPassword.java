/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.time.LocalDateTime;

/**
 *
 * @author admin
 */
public class TokenForgotPassword {

    private int TokenID, UserID;
    private String Token;
    private LocalDateTime expiryTime;
    private boolean isUsed;

    public TokenForgotPassword() {
    }

    public TokenForgotPassword(int TokenID, int UserID, String Token, LocalDateTime expiryTime, boolean isUsed) {
        this.TokenID = TokenID;
        this.UserID = UserID;
        this.Token = Token;
        this.expiryTime = expiryTime;
        this.isUsed = isUsed;
    }

    public TokenForgotPassword(int UserID, String Token, LocalDateTime expiryTime, boolean isUsed) {

        this.UserID = UserID;
        this.Token = Token;
        this.expiryTime = expiryTime;
        this.isUsed = isUsed;
    }

    public int getTokenID() {
        return TokenID;
    }

    public void setTokenID(int TokenID) {
        this.TokenID = TokenID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    public LocalDateTime getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(LocalDateTime expiryTime) {
        this.expiryTime = expiryTime;
    }

    public boolean isIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }

}
