package models;

public class Interns {

    private String idIntern;
    private int userID;
    private String idStudent;
    private int groupID;
    private int semesterID;
    private String division;
    private int isLeader;
    private boolean status;

    private String mentorName;
    private String email;
    private String groupName;
    private String fullname;

    public Interns() {
    }

    public Interns(String idIntern, int userID, String idStudent, int groupID, int semesterID, String division,
            int isLeader, boolean status) {
        this.idIntern = idIntern;
        this.userID = userID;
        this.idStudent = idStudent;
        this.groupID = groupID;
        this.semesterID = semesterID;
        this.division = division;
        this.isLeader = isLeader;
        this.status = status;
    }

    public Interns(String idIntern, int userID, String idStudent, int groupID, int semesterID, boolean status) {
        this.idIntern = idIntern;
        this.userID = userID;
        this.idStudent = idStudent;
        this.groupID = groupID;
        this.semesterID = semesterID;
        this.status = status;
    }

    public Interns(String idStudent, int groupID, int isLeader, String email, String groupName, String fullname) {
        this.idStudent = idStudent;
        this.groupID = groupID;
        this.isLeader = isLeader;
        this.email = email;
        this.groupName = groupName;
        this.fullname = fullname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getIdIntern() {
        return idIntern;
    }

    public void setIdIntern(String idIntern) {
        this.idIntern = idIntern;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public int getSemesterID() {
        return semesterID;
    }

    public void setSemesterID(int semesterID) {
        this.semesterID = semesterID;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getIsLeader() {
        return isLeader;
    }

    public void setIsLeader(int isLeader) {
        this.isLeader = isLeader;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}