/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

public class Attendance_Status {
    private int attID;
    private String attName;

    public Attendance_Status() {
    }

    public Attendance_Status(int attID, String attName) {
        this.attID = attID;
        this.attName = attName;
    }

    public int getAttID() {
        return attID;
    }

    public void setAttID(int attID) {
        this.attID = attID;
    }

    public String getAttName() {
        return attName;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }
    
}
