/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * @author 84989
 */
public class Student {

    private String roll_number;
    private String fullname;
    private String address;
    private String phone_number;
    private String email;
    private String id_card;
    private Date dob;
    private String major;
    private String company;
    private String job_tile;
    private String link_cv;
    private int status_cv;
    private LocalDateTime checkInTime;
    private LocalDateTime checkOutTime;
    private Date checkInDate;
    private Date checkOutDate;
    private String internID;
    private String checkingStatus;

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public LocalDateTime getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(LocalDateTime checkInTime) {
        this.checkInTime = checkInTime;
    }

    public LocalDateTime getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(LocalDateTime checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getInternID() {
        return internID;
    }

    public void setInternID(String internID) {
        this.internID = internID;
    }

    public String getCheckingStatus() {
        return checkingStatus;
    }

    public void setCheckingStatus(String checkingStatus) {
        this.checkingStatus = checkingStatus;
    }

    public Student(String roll_number, String fullname, String email, String internID, String checkingStatus) {
        this.roll_number = roll_number;
        this.fullname = fullname;
        this.email = email;
        this.internID = internID;
        this.checkingStatus = checkingStatus;
    }

    public Student() {
    }

    public Student(String roll_number, String fullname, String address, String phone_number, String email, String id_card, Date dob, String major, String company, String job_tile, String link_cv, int status_cv) {
        this.roll_number = roll_number;
        this.fullname = fullname;
        this.address = address;
        this.phone_number = phone_number;
        this.email = email;
        this.id_card = id_card;
        this.dob = dob;
        this.major = major;
        this.company = company;
        this.job_tile = job_tile;
        this.link_cv = link_cv;
        this.status_cv = status_cv;
    }

    public String getRoll_number() {
        return roll_number;
    }

    public void setRoll_number(String roll_number) {
        this.roll_number = roll_number;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_card() {
        return id_card;
    }

    public void setId_card(String id_card) {
        this.id_card = id_card;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJob_tile() {
        return job_tile;
    }

    public void setJob_tile(String job_tile) {
        this.job_tile = job_tile;
    }

    public String getLink_cv() {
        return link_cv;
    }

    public void setLink_cv(String link_cv) {
        this.link_cv = link_cv;
    }

    public int getStatus_cv() {
        return status_cv;
    }

    public void setStatus_cv(int status_cv) {
        this.status_cv = status_cv;
    }

}
