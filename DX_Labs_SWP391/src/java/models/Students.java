package models;

/**
 *
 * @author ADMIN
 */
public class Students {

    private String rollNumber;   
    private String fullName;     
    private String address;    
    private String phoneNumber;  
    private String Email;
    private String idCard;       
    private String dob;         
    private String major;        
    private String company;      
    private String jobTitle;     
    private String linkCV;       
    private String statusCV;

    public Students() {
    }

    public Students(String rollNumber, String fullName, String address, String phoneNumber, String Email, String idCard, String dob, String major, String company, String jobTitle, String linkCV, String statusCV) {
        this.rollNumber = rollNumber;
        this.fullName = fullName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.Email = Email;
        this.idCard = idCard;
        this.dob = dob;
        this.major = major;
        this.company = company;
        this.jobTitle = jobTitle;
        this.linkCV = linkCV;
        this.statusCV = statusCV;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLinkCV() {
        return linkCV;
    }

    public void setLinkCV(String linkCV) {
        this.linkCV = linkCV;
    }

    public String getStatusCV() {
        return statusCV;
    }

    public void setStatusCV(String statusCV) {
        this.statusCV = statusCV;
    }

   
}
