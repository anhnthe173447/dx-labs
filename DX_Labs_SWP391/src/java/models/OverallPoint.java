/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author admin
 */
public class OverallPoint {

    public int OverallPointID;
    public float majorSkillPoint;
    public float softSkillPoint;
    public float attitudePoint;
    public float finalResult;
    public int internID;
    public String internName;

    // Constructors
    public OverallPoint() {
    }

    public OverallPoint(int OverallPointID, float majorSkillPoint, float softSkillPoint, float attitudePoint, float finalResult, int internID, boolean isDelete, String internName) {
        this.OverallPointID = OverallPointID;
        this.majorSkillPoint = majorSkillPoint;
        this.softSkillPoint = softSkillPoint;
        this.attitudePoint = attitudePoint;
        this.finalResult = finalResult;
        this.internID = internID;
        this.internName = internName;
    }

    public OverallPoint(int OverallPointID, float majorSkillPoint, float softSkillPoint, float attitudePoint, float finalResult, int internID, String internName) {
        this.OverallPointID = OverallPointID;
        this.majorSkillPoint = majorSkillPoint;
        this.softSkillPoint = softSkillPoint;
        this.attitudePoint = attitudePoint;
        this.finalResult = finalResult;
        this.internID = internID;
        this.internName = internName;
    }

    public int getOverallPointID() {
        return OverallPointID;
    }

    public void setOverallPointID(int OverallPointID) {
        this.OverallPointID = OverallPointID;
    }

    public float getMajorSkillPoint() {
        return majorSkillPoint;
    }

    public void setMajorSkillPoint(float majorSkillPoint) {
        this.majorSkillPoint = majorSkillPoint;
    }

    public float getSoftSkillPoint() {
        return softSkillPoint;
    }

    public void setSoftSkillPoint(float softSkillPoint) {
        this.softSkillPoint = softSkillPoint;
    }

    public float getAttitudePoint() {
        return attitudePoint;
    }

    public void setAttitudePoint(float attitudePoint) {
        this.attitudePoint = attitudePoint;
    }

    public float getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(float finalResult) {
        this.finalResult = finalResult;
    }

    public int getInternID() {
        return internID;
    }

    public void setInternID(int internID) {
        this.internID = internID;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    @Override
    public String toString() {
        return "OverallPoint{" + "OverallPointID=" + OverallPointID + ", majorSkillPoint=" + majorSkillPoint + ", softSkillPoint=" + softSkillPoint + ", attitudePoint=" + attitudePoint + ", finalResult=" + finalResult + ", internID=" + internID + ", internName=" + internName + '}';
    }

}
