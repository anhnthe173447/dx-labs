package models;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author Laptop K1
 */
public class Task {
    private String taskID;
    private String mentorID;
    private int groupID;
    private String groupName;
    private String taskName;
    private String description;
    private Date startDate;
    private Date enddate;
    private String statusName;
    private String mentorName;
    private int answerID;
    private String answerContent;
    private Timestamp answertime;
    private String comment;
    
 

    public Task() {
    }

    public Task(String taskID, String mentorID , int groupID, String groupName, String taskName, String description, Date startDate, Date enddate, String statusName) {
        this.taskID = taskID;
        this.mentorID = mentorID;
        this.groupID = groupID;
        this.groupName = groupName;
        this.taskName = taskName;
        this.description = description;
        this.startDate = startDate;
        this.enddate = enddate;   
        this.statusName = statusName;
       
    }

    public Task(String taskID, String mentorID, String taskName, String description, Date startDate, Date enddate, String statusName, String mentorName) {
        this.taskID = taskID;
        this.mentorID = mentorID;
        this.taskName = taskName;
        this.description = description;
        this.startDate = startDate;
        this.enddate = enddate;
        this.statusName = statusName;
        this.mentorName = mentorName;
    }

    public Task(String taskID, String mentorID, int groupID, String groupName, String taskName, String description, Date startDate, Date enddate, String statusName, String mentorName) {
        this.taskID = taskID;
        this.mentorID = mentorID;
        this.groupID = groupID;
        this.groupName = groupName;
        this.taskName = taskName;
        this.description = description;
        this.startDate = startDate;
        this.enddate = enddate;
        this.statusName = statusName;
        this.mentorName = mentorName;
    }

    public Task(String taskID, String mentorID, int groupID, String groupName, String taskName, String description, Date startDate, Date enddate, String statusName, int answerID, String answerContent, Timestamp answertime, String comment) {
        this.taskID = taskID;
        this.mentorID = mentorID;
        this.groupID = groupID;
        this.groupName = groupName;
        this.taskName = taskName;
        this.description = description;
        this.startDate = startDate;
        this.enddate = enddate;
        this.statusName = statusName;   
        this.answerID = answerID;
        this.answerContent = answerContent;
        this.answertime = answertime;
        this.comment = comment;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public Timestamp getAnswertime() {
        return answertime;
    }

    public void setAnswertime(Timestamp answertime) {
        this.answertime = answertime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
   

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public String getMentorID() {
        return mentorID;
    }

    public void setMentorID(String mentorID) {
        this.mentorID = mentorID;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    

    
}