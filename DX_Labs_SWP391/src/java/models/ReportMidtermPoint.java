/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ADMIN
 */
public class ReportMidtermPoint {

    private String studentID;
    private String internID;
    private String internName;
    private String division;
    private String mentorName;
    private String evaluate;
    private String comment;

    public ReportMidtermPoint() {
    }

    public ReportMidtermPoint(String studentID, String internID, String internName, String division, String mentorName, String evaluate, String comment) {
        this.studentID = studentID;
        this.internID = internID;
        this.internName = internName;
        this.division = division;
        this.mentorName = mentorName;
        this.evaluate = evaluate;
        this.comment = comment;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getInternID() {
        return internID;
    }

    public void setInternID(String internID) {
        this.internID = internID;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    
}
