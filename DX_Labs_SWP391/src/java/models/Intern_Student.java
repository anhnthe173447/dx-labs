/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author ThinkPro
 */
public class Intern_Student {

    private int Intern_ID, User_ID, Group_ID, Semester_ID, Mentor_ID, OverallEval_ID;
    private String StudentID, Division, Group_Name, FullName, Comment_Midterm, Comment_Final, Address, Phone_number, Email, ID_Card, Major, Job_tile, Company, Link_CV;
    private Date DOB;
    private Boolean IsLeader, InternStatus, CV_Status;
    private float SoftSkillPoint_Midterm, SoftSkillPoint_Final,
            MajorSkillPoint_Midterm, MajorSkillPoint_Final,
            AttitudePoint_Midterm, AttitudePoint_Final,
            MajorSkillPoint_Overall, SoftSkillPoint_Overall, AttitudePoint_Overall, FinalResult_Overall,
            SemesterID_Overall;

    public Intern_Student(int Intern_ID, int User_ID, int Group_ID, int Semester_ID, int Mentor_ID, int OverallEval_ID, String StudentID, String Division, String Group_Name, String FullName, String Comment_Midterm, String Comment_Final, String Address, String Phone_number, String Email, String ID_Card, String Major, String Job_tile, String Company, String Link_CV, Date DOB, Boolean IsLeader, Boolean InternStatus, Boolean CV_Status, float SoftSkillPoint_Midterm, float SoftSkillPoint_Final, float MajorSkillPoint_Midterm, float MajorSkillPoint_Final, float AttitudePoint_Midterm, float AttitudePoint_Final, float MajorSkillPoint_Overall, float SoftSkillPoint_Overall, float AttitudePoint_Overall, float FinalResult_Overall, float SemesterID_Overall) {
        this.Intern_ID = Intern_ID;
        this.User_ID = User_ID;
        this.Group_ID = Group_ID;
        this.Semester_ID = Semester_ID;
        this.Mentor_ID = Mentor_ID;
        this.OverallEval_ID = OverallEval_ID;
        this.StudentID = StudentID;
        this.Division = Division;
        this.Group_Name = Group_Name;
        this.FullName = FullName;
        this.Comment_Midterm = Comment_Midterm;
        this.Comment_Final = Comment_Final;
        this.Address = Address;
        this.Phone_number = Phone_number;
        this.Email = Email;
        this.ID_Card = ID_Card;
        this.Major = Major;
        this.Job_tile = Job_tile;
        this.Company = Company;
        this.Link_CV = Link_CV;
        this.DOB = DOB;
        this.IsLeader = IsLeader;
        this.InternStatus = InternStatus;
        this.CV_Status = CV_Status;
        this.SoftSkillPoint_Midterm = SoftSkillPoint_Midterm;
        this.SoftSkillPoint_Final = SoftSkillPoint_Final;
        this.MajorSkillPoint_Midterm = MajorSkillPoint_Midterm;
        this.MajorSkillPoint_Final = MajorSkillPoint_Final;
        this.AttitudePoint_Midterm = AttitudePoint_Midterm;
        this.AttitudePoint_Final = AttitudePoint_Final;
        this.MajorSkillPoint_Overall = MajorSkillPoint_Overall;
        this.SoftSkillPoint_Overall = SoftSkillPoint_Overall;
        this.AttitudePoint_Overall = AttitudePoint_Overall;
        this.FinalResult_Overall = FinalResult_Overall;
        this.SemesterID_Overall = SemesterID_Overall;
    }

    public String getComment_Midterm() {
        return Comment_Midterm;
    }

    public void setComment_Midterm(String Comment_Midterm) {
        this.Comment_Midterm = Comment_Midterm;
    }

    public int getOverallEval_ID() {
        return OverallEval_ID;
    }

    public void setOverallEval_ID(int OverallEval_ID) {
        this.OverallEval_ID = OverallEval_ID;
    }

    public float getMajorSkillPoint_Overall() {
        return MajorSkillPoint_Overall;
    }

    public void setMajorSkillPoint_Overall(float MajorSkillPoint_Overall) {
        this.MajorSkillPoint_Overall = MajorSkillPoint_Overall;
    }

    public float getSoftSkillPoint_Overall() {
        return SoftSkillPoint_Overall;
    }

    public void setSoftSkillPoint_Overall(float SoftSkillPoint_Overall) {
        this.SoftSkillPoint_Overall = SoftSkillPoint_Overall;
    }

    public float getAttitudePoint_Overall() {
        return AttitudePoint_Overall;
    }

    public void setAttitudePoint_Overall(float AttitudePoint_Overall) {
        this.AttitudePoint_Overall = AttitudePoint_Overall;
    }

    public float getFinalResult_Overall() {
        return FinalResult_Overall;
    }

    public void setFinalResult_Overall(float FinalResult_Overall) {
        this.FinalResult_Overall = FinalResult_Overall;
    }

    public float getSemesterID_Overall() {
        return SemesterID_Overall;
    }

    public void setSemesterID_Overall(float SemesterID_Overall) {
        this.SemesterID_Overall = SemesterID_Overall;
    }

    public String getComment_Final() {
        return Comment_Final;
    }

    public void setComment_Final(String Comment_Final) {
        this.Comment_Final = Comment_Final;
    }

    public float getSoftSkillPoint_Midterm() {
        return SoftSkillPoint_Midterm;
    }

    public void setSoftSkillPoint_Midterm(float SoftSkillPoint_Midterm) {
        this.SoftSkillPoint_Midterm = SoftSkillPoint_Midterm;
    }

    public float getSoftSkillPoint_Final() {
        return SoftSkillPoint_Final;
    }

    public void setSoftSkillPoint_Final(float SoftSkillPoint_Final) {
        this.SoftSkillPoint_Final = SoftSkillPoint_Final;
    }

    public float getMajorSkillPoint_Midterm() {
        return MajorSkillPoint_Midterm;
    }

    public void setMajorSkillPoint_Midterm(float MajorSkillPoint_Midterm) {
        this.MajorSkillPoint_Midterm = MajorSkillPoint_Midterm;
    }

    public float getMajorSkillPoint_Final() {
        return MajorSkillPoint_Final;
    }

    public void setMajorSkillPoint_Final(float MajorSkillPoint_Final) {
        this.MajorSkillPoint_Final = MajorSkillPoint_Final;
    }

    public float getAttitudePoint_Midterm() {
        return AttitudePoint_Midterm;
    }

    public void setAttitudePoint_Midterm(float AttitudePoint_Midterm) {
        this.AttitudePoint_Midterm = AttitudePoint_Midterm;
    }

    public float getAttitudePoint_Final() {
        return AttitudePoint_Final;
    }

    public void setAttitudePoint_Final(float AttitudePoint_Final) {
        this.AttitudePoint_Final = AttitudePoint_Final;
    }

    public Intern_Student() {
    }

    public int getMentor_ID() {
        return Mentor_ID;
    }

    public void setMentor_ID(int Mentor_ID) {
        this.Mentor_ID = Mentor_ID;
    }

    public String getGroup_Name() {
        return Group_Name;
    }

    public void setGroup_Name(String Group_Name) {
        this.Group_Name = Group_Name;
    }

    public int getIntern_ID() {
        return Intern_ID;
    }

    public void setIntern_ID(int Intern_ID) {
        this.Intern_ID = Intern_ID;
    }

    public int getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(int User_ID) {
        this.User_ID = User_ID;
    }

    public int getGroup_ID() {
        return Group_ID;
    }

    public void setGroup_ID(int Group_ID) {
        this.Group_ID = Group_ID;
    }

    public int getSemester_ID() {
        return Semester_ID;
    }

    public void setSemester_ID(int Semester_ID) {
        this.Semester_ID = Semester_ID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String StudentID) {
        this.StudentID = StudentID;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPhone_number() {
        return Phone_number;
    }

    public void setPhone_number(String Phone_number) {
        this.Phone_number = Phone_number;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getID_Card() {
        return ID_Card;
    }

    public void setID_Card(String ID_Card) {
        this.ID_Card = ID_Card;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String Major) {
        this.Major = Major;
    }

    public String getJob_tile() {
        return Job_tile;
    }

    public void setJob_tile(String Job_tile) {
        this.Job_tile = Job_tile;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String Company) {
        this.Company = Company;
    }

    public String getLink_CV() {
        return Link_CV;
    }

    public void setLink_CV(String Link_CV) {
        this.Link_CV = Link_CV;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public Boolean getIsLeader() {
        return IsLeader;
    }

    public void setIsLeader(Boolean IsLeader) {
        this.IsLeader = IsLeader;
    }

    public Boolean getInternStatus() {
        return InternStatus;
    }

    public void setInternStatus(Boolean InternStatus) {
        this.InternStatus = InternStatus;
    }

    public Boolean getCV_Status() {
        return CV_Status;
    }

    public void setCV_Status(Boolean CV_Status) {
        this.CV_Status = CV_Status;
    }

}
