/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Semester {

    public int semester_ID;
    public String semester_Name;
    public Date start_date;
    public Date end_date;

    public Semester() {
    }

    public Semester(int semester_ID, String semester_Name, Date start_date, Date end_date) {
        this.semester_ID = semester_ID;
        this.semester_Name = semester_Name;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public int getSemester_ID() {
        return semester_ID;
    }

    public void setSemester_ID(int semester_ID) {
        this.semester_ID = semester_ID;
    }

    public String getSemester_Name() {
        return semester_Name;
    }

    public void setSemester_Name(String semester_Name) {
        this.semester_Name = semester_Name;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    @Override
    public String toString() {
        return "Semester{" + "semester_ID=" + semester_ID + ", semester_Name=" + semester_Name + ", start_date=" + start_date + ", end_date=" + end_date + '}';
    }

    

}
