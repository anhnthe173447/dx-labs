/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author admin
 */
public class MidtermPoint {

    public int midtermID;
    public String fullName;
    public float majorPoints;
    public float softSkillPoints;
    public float attitudePoints;
    public String comment;
    public int internID;

    public MidtermPoint() {
    }

    public MidtermPoint(int midtermID, String fullName, float majorPoints, float softSkillPoints, float attitudePoints, String comment, int internID) {
        this.midtermID = midtermID;
        this.fullName = fullName;
        this.majorPoints = majorPoints;
        this.softSkillPoints = softSkillPoints;
        this.attitudePoints = attitudePoints;
        this.comment = comment;
        this.internID = internID;
    }

    public int getMidtermID() {
        return midtermID;
    }

    public void setMidtermID(int midtermID) {
        this.midtermID = midtermID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public float getMajorPoints() {
        return majorPoints;
    }

    public void setMajorPoints(float majorPoints) {
        this.majorPoints = majorPoints;
    }

    public float getSoftSkillPoints() {
        return softSkillPoints;
    }

    public void setSoftSkillPoints(float softSkillPoints) {
        this.softSkillPoints = softSkillPoints;
    }

    public float getAttitudePoints() {
        return attitudePoints;
    }

    public void setAttitudePoints(float attitudePoints) {
        this.attitudePoints = attitudePoints;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getInternID() {
        return internID;
    }

    public void setInternID(int internID) {
        this.internID = internID;
    }


    @Override
    public String toString() {
        return "MidtermPoint{" + "midtermID=" + midtermID + ", fullName=" + fullName + ", majorPoints=" + majorPoints + ", softSkillPoints=" + softSkillPoints + ", attitudePoints=" + attitudePoints + ", comment=" + comment + ", internID=" + internID ;
    }

}
