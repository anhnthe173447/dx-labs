package models;

public class Group {

    private int group_id;
    private String group_name;
    private int mentor_id;

    public Group() {
    }

    public Group(int group_id, String group_name, int mentor_id) {

        this.group_id = group_id;
        this.group_name = group_name;
        this.mentor_id = mentor_id;
    }
    public int getGroup_id() {
        return group_id;
    }
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public int getMentor_id() {
        return mentor_id;
    }

    public void setMentor_id(int mentor_id) {
        this.mentor_id = mentor_id;
    }

}
