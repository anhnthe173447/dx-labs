/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

public class Attendance {
    private int attendanceID;
    private int attendanceStatusid;
    private String idIntern;
    private String IDStudent;
    private String fullname;
    private String attendanceName;
   private String reason;

    public Attendance() {
    }

    public Attendance(int attendanceID, int attendanceStatusid, String idIntern, String IDStudent, String fullname, String attendanceName, String reason) {
        this.attendanceID = attendanceID;
        this.attendanceStatusid = attendanceStatusid;
        this.idIntern = idIntern;
        this.IDStudent = IDStudent;
        this.fullname = fullname;
        this.attendanceName = attendanceName;
        this.reason = reason;
    }

    public int getAttendanceID() {
        return attendanceID;
    }

    public void setAttendanceID(int attendanceID) {
        this.attendanceID = attendanceID;
    }

    public int getAttendanceStatusid() {
        return attendanceStatusid;
    }

    public void setAttendanceStatusid(int attendanceStatusid) {
        this.attendanceStatusid = attendanceStatusid;
    }

    public String getIdIntern() {
        return idIntern;
    }

    public void setIdIntern(String idIntern) {
        this.idIntern = idIntern;
    }

    public String getIDStudent() {
        return IDStudent;
    }

    public void setIDStudent(String IDStudent) {
        this.IDStudent = IDStudent;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAttendanceName() {
        return attendanceName;
    }

    public void setAttendanceName(String attendanceName) {
        this.attendanceName = attendanceName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
}
