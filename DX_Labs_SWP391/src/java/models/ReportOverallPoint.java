/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ADMIN
 */
public class ReportOverallPoint {
    private String StudentID;
    private String internID;
    private String internName;
    private String division;
    private String mentorName;
    private String allowance;
    private String comment;
    private String majorSkillPoint;
    private String softSkillPoint;
    private String attitudePoint;
    private String finalResult;

    public ReportOverallPoint() {
    }

    public ReportOverallPoint(String StudentID, String internID, String internName, String division, String mentorName, String allowance, String comment, String majorSkillPoint, String softSkillPoint, String attitudePoint, String finalResult) {
        this.StudentID = StudentID;
        this.internID = internID;
        this.internName = internName;
        this.division = division;
        this.mentorName = mentorName;
        this.allowance = allowance;
        this.comment = comment;
        this.majorSkillPoint = majorSkillPoint;
        this.softSkillPoint = softSkillPoint;
        this.attitudePoint = attitudePoint;
        this.finalResult = finalResult;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String StudentID) {
        this.StudentID = StudentID;
    }

    public String getInternID() {
        return internID;
    }

    public void setInternID(String internID) {
        this.internID = internID;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    public String getAllowance() {
        return allowance;
    }

    public void setAllowance(String allowance) {
        this.allowance = allowance;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMajorSkillPoint() {
        return majorSkillPoint;
    }

    public void setMajorSkillPoint(String majorSkillPoint) {
        this.majorSkillPoint = majorSkillPoint;
    }

    public String getSoftSkillPoint() {
        return softSkillPoint;
    }

    public void setSoftSkillPoint(String softSkillPoint) {
        this.softSkillPoint = softSkillPoint;
    }

    public String getAttitudePoint() {
        return attitudePoint;
    }

    public void setAttitudePoint(String attitudePoint) {
        this.attitudePoint = attitudePoint;
    }

    public String getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(String finalResult) {
        this.finalResult = finalResult;
    }
    
    
    
    
}
