/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author FPT
 */
public class InternAttendance {
     private String internID;
    private String status; 

    public String getInternID() {
        return internID;
    }

    public void setInternID(String internID) {
        this.internID = internID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
