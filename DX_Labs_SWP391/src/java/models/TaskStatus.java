/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author Laptop K1
 */
public class TaskStatus {
    private String status_id;
    private String Status_name;

    public TaskStatus() {
    }

    public TaskStatus(String status_id, String Status_name) {
        this.status_id = status_id;
        this.Status_name = Status_name;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getStatus_name() {
        return Status_name;
    }

    public void setStatus_name(String Status_name) {
        this.Status_name = Status_name;
    }
    
    
}
