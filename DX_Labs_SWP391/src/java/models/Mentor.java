package models;
public class Mentor {

    public int mentorId;
    public String userId;
    public String fullName;
    public String email;
    public String phone;
    public String gender;
    public String dob;
    public String idCard;
    public String address;
    public String groupid;
    public String groupName;
    public String taskID;
    public String taskName;
    public String description;
    public String endtime;
    public int status ;
    public String answerContent;
    public String answerTime;
    public String comment;

    public Mentor() {
    }

    public Mentor(int mentorId, String userId, String fullName, String email, String phone, String gender, String dob, String idCard, String address) {
        this.mentorId = mentorId;
        this.userId = userId;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.dob = dob;
        this.idCard = idCard;
        this.address = address;
    }

    public Mentor(String userId, String groupid, String groupName, String taskID, String taskName, String description, String endtime, int status, String answerContent, String answerTime, String comment) {
        this.userId = userId;
        this.groupid = groupid;
        this.groupName = groupName;
        this.taskID = taskID;
        this.taskName = taskName;
        this.description = description;
        this.endtime = endtime;
        this.status = status;
        this.answerContent = answerContent;
        this.answerTime = answerTime;
        this.comment = comment;
    }

    // Getters and Setters
    public int getMentorId() {
        return mentorId;
    }

    public void setMentorId(int mentorId) {
        this.mentorId = mentorId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public String getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(String answerTime) {
        this.answerTime = answerTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
