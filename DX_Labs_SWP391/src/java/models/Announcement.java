/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Announcement {
    public int Announcement_ID;
    public String Announcement_content;
    public String Description;
   
    public Date Date;

    public Announcement() {
    }

    public Announcement(int Announcement_ID, String Announcement_content, String Description,  Date Date) {
        this.Announcement_ID = Announcement_ID;
        this.Announcement_content = Announcement_content;
        this.Description = Description;
      
        this.Date = Date;
    }

    public int getAnnouncement_ID() {
        return Announcement_ID;
    }

    public void setAnnouncement_ID(int Announcement_ID) {
        this.Announcement_ID = Announcement_ID;
    }

    public String getAnnouncement_content() {
        return Announcement_content;
    }

    public void setAnnouncement_content(String Announcement_content) {
        this.Announcement_content = Announcement_content;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    @Override
    public String toString() {
        return "Announcement{" + "Announcement_ID=" + Announcement_ID + ", Announcement_content=" + Announcement_content + ", Description=" + Description + " Date=" + Date + '}';
    }
    
    
}
