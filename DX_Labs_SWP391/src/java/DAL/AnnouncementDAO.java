/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Announcement;

/**
 *
 * @author admin
 */
public class AnnouncementDAO extends DBContext {
 Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    protected Connection connection;

    public List<Announcement> getAnnouncementList() {
        connection = this.getConnection();
        List<Announcement> list = new ArrayList<>();
        try {
            String sql = "select *\n"
                    + "from Announcement";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Announcement AnnouncementList = new Announcement(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        
                        rs.getDate(4));

                list.add(AnnouncementList);
            }

        } catch (Exception e) {
        }
        return list;
    }

    public void addnewAnnouncement(String announcement_content, String description, String date) {
        String query = "INSERT INTO [DXLab10].[dbo].[Announcement] ( [AnnouncementContent], [Description], [Date])\n" +
"VALUES (?,?,?);";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, announcement_content);
            ps.setString(2, description);
            ps.setString(3, date);
            ps.executeUpdate();
            System.out.println("Announcement added successfully!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to add announcement.");
        } finally {
            try {
                if (ps != null) ps.close();
                if (conn != null) conn.close();
        } catch (Exception e) {
        }

}
    }
    
    public void deleteAnnouncement(String aid){
        String query = "delete from Announcement where Announcement_ID = ?";
        
        try {
             conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, aid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
  public Announcement getAnnouncementByID(String aid) {
        connection = this.getConnection();
        
        try {
            
            String sql = "select * from Announcement where Announcement_ID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, aid);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return new Announcement(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),                  
                        rs.getDate(4));

               
            }

        } catch (Exception e) {
        
        }
     return null;
  }
         public static void main(String[] args) {
        AnnouncementDAO announcementDAO = new AnnouncementDAO();
        String testID = "1"; // Replace with an actual ID from your database

        Announcement announcement = announcementDAO.getAnnouncementByID(testID);

        if (announcement != null) {
            System.out.println("Announcement found:");
            System.out.println("ID: " + announcement.getAnnouncement_ID());
            System.out.println("Content: " + announcement.getAnnouncement_content());
            System.out.println("Description: " + announcement.getDescription());
            System.out.println("Date: " + announcement.getDate());
        } else {
            System.out.println("No announcement found with ID: " + testID);
        }
    }
    

}
   
         



