/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Interns;

/**
 *
 * @author Laptop K1
 */
public class DAOListGroupMember extends DBContext {
    public List<Interns> listGroupMate(int groupID) {
        connection = this.getConnection();
        List<Interns> listGroupMate = new ArrayList<>();
        try {
            String sql = "select i.StudentID, i.Group_ID, i.IsLeader, s.FullName, s.Email, g.Group_Name "
                    + "from Intern i "
                    + "join Student s on i.StudentID = s.StudentID "
                    + "join [Group] g on i.Group_ID = g.Group_ID "
                    + "where i.Group_ID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, groupID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Interns list = new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(4));                                   
                listGroupMate.add(list);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listGroupMate;
    }
}
