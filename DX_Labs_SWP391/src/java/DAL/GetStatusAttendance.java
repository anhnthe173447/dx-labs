/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.Attendance_Status;

/**
 *
 * @author Laptop K1
 */
public class GetStatusAttendance extends DBContext {

    public List<Attendance_Status> getAttendanceStatus() {
        connection = this.getConnection();
        List<Attendance_Status> list = new ArrayList<>();
        try {
            String sql = "select * from Attendance_status";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Attendance_Status list1 = new Attendance_Status(rs.getInt(1),
                        rs.getString(2)
                );
                list.add(list1);
            }
        } catch (Exception e) {
        }
        return list;
    }

   
}
