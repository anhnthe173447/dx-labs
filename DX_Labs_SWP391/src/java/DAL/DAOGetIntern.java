package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.Attendance;

public class DAOGetIntern extends DBContext {

    protected Connection connection;

    public List<Attendance> getIntern() {
        connection = this.getConnection();
        List<Attendance> list = new ArrayList<>();
        try {
            String sql = "SELECT a.attendance_ID ,a.attendance_statusID, a.Id_intern,i.ID_student,s.FullName,att.attendance_name,a.reason\n"
                    + "FROM Attendance a\n"
                    + "JOIN Intern i ON a.Id_intern = i.Intern_ID\n"
                    + "JOIN Attendance_Status att ON a.Attendance_statusID = att.Attendance_statusID\n"
                    + "JOIN Student s ON i.ID_student = s.Roll_number;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Attendance listIntern = new Attendance(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                list.add(listIntern);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        DAOGetIntern getStatusAttendance = new DAOGetIntern();
        List<Attendance> list = getStatusAttendance.getIntern();
        for (Attendance a : list) {
            System.out.println("Attendance ID: " + a.getAttendanceID());
            System.out.println("Student ID: " + a.getIDStudent());
            System.out.println("Full Name: " + a.getFullname());
            System.out.println("Attendance Name: " + a.getAttendanceName());
            System.out.println("Intern ID: " + a.getIdIntern());
            System.out.println("Attendance Status ID: " + a.getAttendanceStatusid());
            System.out.println("Reason: " + a.getReason());
            System.out.println("------------");
        }
    }

}
