/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.FinaltermPoint;

/**
 *
 * @author admin
 */
public class FinalTermPointDAO extends DBContext {

    protected Connection connection;

    public List<FinaltermPoint> getListFinalTermPointByUserID(int userID) {

        Connection connection = null;
        List<FinaltermPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql
                    = " SELECT Student.FullName, FinalEvaluation.FinalEval_ID, FinalEvaluation.MajorSkillPoint, FinalEvaluation.SoftSkillPoint, FinalEvaluation.AttitudePoint,\n"
                    + "FinalEvaluation.Comment, FinalEvaluation.InternID \n"
                    + "FROM FinalEvaluation JOIN Intern ON FinalEvaluation.InternID = Intern.Intern_ID\n"
                    + "JOIN Student ON Intern.StudentID = Student.StudentID\n"
                    + "Join [Group] on [Group].Group_ID = Intern.Group_ID\n"
                    + "where [Group].Mentor_ID =(select Mentor_ID\n"
                    + "from Mentor join Account on Mentor.UserID = Account.User_ID\n"
                    + "where Account.User_ID = ?)";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            rs = ps.executeQuery();

            while (rs.next()) {
                FinaltermPoint endingStage = new FinaltermPoint(
                        rs.getInt("FinalEval_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID")
                );

                list.add(endingStage);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public FinaltermPoint getFinalPointByInternID(int internID) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        FinaltermPoint endingStage = null;

        try {
            connection = this.getConnection();
            String sql = "SELECT FinalEvaluation.FinalEval_ID, FinalEvaluation.MajorSkillPoint, FinalEvaluation.SoftSkillPoint, "
                    + "FinalEvaluation.AttitudePoint,\n"
                    + "FinalEvaluation.Comment, "
                    + "FinalEvaluation.InternID, FinalEvaluation.IsDeleted, Student.FullName\n"
                    + "FROM FinalEvaluation JOIN Intern ON FinalEvaluation.InternID = Intern.Intern_ID\n"
                    + "JOIN Student ON Intern.StudentID = Student.StudentID\n"
                    + "where FinalEvaluation.InternID = ? and FinalEvaluation.IsDeleted = 0";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, internID);
            rs = ps.executeQuery();

            if (rs.next()) {
                endingStage = new FinaltermPoint(
                        rs.getInt("FinalEval_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return endingStage;
    }

    public boolean insertFinalPoint(String internID, int endingID, float majorPoints, float softSkillPoints, float attitudePoints, String comment) {
        String sql = "SET IDENTITY_INSERT [Ending_stage] ON;"
                + "INSERT INTO Ending_stage (Id_intern, ID_ending_stage, Major_skills_point, Soft_skill_point, Attitude_point, Comment) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, internID);
            ps.setInt(2, endingID);
            ps.setFloat(3, majorPoints);
            ps.setFloat(4, softSkillPoints);
            ps.setFloat(5, attitudePoints);
            ps.setString(6, comment);

            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getMaxFinalPointID() {
        String sql = "SELECT MAX(ID_ending_stage) FROM Ending_stage";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0; // Trả về 0 nếu không có bản ghi nào
    }

    public boolean updateFinalPoint(FinaltermPoint finalPoint) {
        String sql = "update FinalEvaluation\n"
                + "set MajorSkillPoint = ?,\n"
                + "SoftSkillPoint = ?,\n"
                + "AttitudePoint = ?,\n"
                + "Comment = ?\n"
                + "where InternID = ? and IsDeleted = 0";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setFloat(1, finalPoint.getMajorPoints());
            ps.setFloat(2, finalPoint.getSoftSkillPoints());
            ps.setFloat(3, finalPoint.getAttitudePoints());
            ps.setString(4, finalPoint.getComment());
            ps.setInt(5, finalPoint.getInternID());

            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int saveFinaltermPoint(FinaltermPoint finaltermPoint) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();

            // Check if the record already exists
            String checkSql = "SELECT COUNT(*) AS count FROM FinalEvaluation WHERE InternID = ?";
            ps = connection.prepareStatement(checkSql);
            ps.setInt(1, finaltermPoint.getInternID());
            rs = ps.executeQuery();

            if (rs.next() && rs.getInt("count") > 0) {
                // Update the existing record
                String updateSql = "UPDATE FinalEvaluation SET MajorSkillPoint = ?, SoftSkillPoint = ?, AttitudePoint = ?, "
                        + "Comment = ? "
                        + "WHERE InternID = ? ";
                ps = connection.prepareStatement(updateSql);
                ps.setFloat(1, finaltermPoint.getMajorPoints());
                ps.setFloat(2, finaltermPoint.getSoftSkillPoints());
                ps.setFloat(3, finaltermPoint.getAttitudePoints());
                ps.setString(4, finaltermPoint.getComment());
                ps.setInt(5, finaltermPoint.getInternID());

                return ps.executeUpdate(); // Return the number of rows affected (should be 1 if successful)
            } else {
                // Insert new record if it doesn't exist
                String insertSql = "INSERT INTO FinalEvaluation (InternID, MajorSkillPoint, SoftSkillPoint, AttitudePoint, Comment, IsDeleted) "
                        + "VALUES (?, ?, ?, ?, ?,0)";
                ps = connection.prepareStatement(insertSql);
                ps.setInt(1, finaltermPoint.getInternID());
                ps.setFloat(2, finaltermPoint.getMajorPoints());
                ps.setFloat(3, finaltermPoint.getSoftSkillPoints());
                ps.setFloat(4, finaltermPoint.getAttitudePoints());
                ps.setString(5, finaltermPoint.getComment());

                return ps.executeUpdate(); // Return the number of rows affected (should be 1 if successful)
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public List<FinaltermPoint> getListFinaltermPointBySemester() {

        Connection connection = null;
        List<FinaltermPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql = "select FinalEvaluation.FinalEval_ID, Student.FullName, FinalEvaluation.MajorSkillPoint, FinalEvaluation.SoftSkillPoint, FinalEvaluation.AttitudePoint, FinalEvaluation.Comment, FinalEvaluation.InternID\n"
                    + "from FinalEvaluation join Intern\n"
                    + "on FinalEvaluation.InternID = Intern.Intern_ID join Semester\n"
                    + "on Intern.Semester_ID = Semester.Semester_ID join Student\n"
                    + "on Student.StudentID = Intern.StudentID\n"
                    + "where FinalEvaluation.IsDeleted = 0 and Semester.Semester_ID = (select Semester_ID\n"
                    + "								from Semester\n"
                    + "								where CURRENT_TIMESTAMP between StartDate and EndDate)";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                FinaltermPoint finalTPoint = new FinaltermPoint(
                        rs.getInt("FinalEval_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID"));

                list.add(finalTPoint);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
