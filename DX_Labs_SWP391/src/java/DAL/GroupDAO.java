/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Group;

/**
 *
 * @author ThinkPro
 */
public class GroupDAO extends DBContext {

    protected Connection connection;

    public List<Group> getGroupsByMentorId(int mentorId) {
        connection = this.getConnection();
        String sql = "select * from [Group] where Mentor_ID = ?";
        List<Group> groups = new ArrayList();
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, mentorId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Group m = new Group();
                m.setGroup_id(rs.getInt("Group_ID"));
                m.setGroup_name(rs.getString("Group_Name"));
                m.setMentor_id(rs.getInt("Mentor_ID"));
                groups.add(m);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return groups;
    }
    
     public Group getGroupsByGroupId(int groupId) {
        connection = this.getConnection();
        String sql = "select * from [Group] where [Group].Group_ID = ?";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, groupId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Group m = new Group();
                m.setGroup_id(rs.getInt("Group_ID"));
                m.setGroup_name(rs.getString("Group_Name"));
                m.setMentor_id(rs.getInt("Mentor_ID"));
                return m;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
