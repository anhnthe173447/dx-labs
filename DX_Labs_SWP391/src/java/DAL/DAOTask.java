/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Task;
import models.TaskStatus;

/**
 *
 * @author Laptop K1
 */
public class DAOTask extends DBContext {

    protected Connection connection;

    public void deleteTask(String taskID) {
        connection = this.getConnection();
        PreparedStatement ps = null;
        String sql = "update task set isDeleted = 0 WHERE task_id = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Task> getListTask() {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql
                    = "select t.TaskName,t.Description,t.StartTaskTime,t.EndTaskTime,t.TaskStatus,m.Full_name, g.Group_Name,m.Mentor_ID,g.Group_ID,t.Task_ID from task t join Mentor m on t.Mentor_ID = m.Mentor_ID\n"
                    + " join [Group] g on t.group_id = g.Group_ID where IsDeleted = 1 ORDER BY \n"
                    + "g.Group_Name;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(rs.getString(10),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getString(7),
                        rs.getString(1),
                        rs.getString(2),
                        rs.getDate(3),
                        rs.getDate(4),
                        rs.getString(5),
                        rs.getString(6));
                list.add(taskList);
            }
        } catch (Exception e) {
        }
        return list;
    }

//    public static void main(String[] args) {
//        DAOTask taskManager = new DAOTask();
//        List<Task> tasks = taskManager.getListTask();
//
//        for (Task task : tasks) {
//            System.out.println(task);
//        }
//    }
    public boolean insertTask(String mentorID, String groupID, String taskName, String description, String start, String end) {
        connection = this.getConnection();
        PreparedStatement ps = null;

        String sql = "INSERT INTO task (Mentor_id, taskname, Description, StartTaskTime,EndTaskTime,TaskStatus, isDeleted, group_id)VALUES                       \n"
                + "(?,?,?,?,?,1,1,?);";
        try {
            ps = connection.prepareStatement(sql);

            ps.setString(1, mentorID);
            ps.setString(2, taskName);
            ps.setString(3, description);
            ps.setString(4, start);
            ps.setString(5, end);
            ps.setString(6, groupID);
            int affectedRows = ps.executeUpdate();
            return affectedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateTask(String taskID, String mentorID, int groupID, String taskName, String description, String start, String end) {
        connection = this.getConnection();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE Task \n"
                    + "SET  \n"
                    + "Mentor_ID = ?,\n"
                    + "[Group_ID] = ?,\n"
                    + "[taskname] = ?,\n"
                    + "[description] =?,\n"
                    + "starttasktime =?,\n"
                    + "endtasktime = ?\n"
                    + "WHERE task_id = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, mentorID);
            ps.setInt(2, groupID);
            ps.setString(3, taskName);
            ps.setString(4, description);
            ps.setString(5, start);
            ps.setString(6, end);
            ps.setString(7, taskID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception ex) {

        }
    }

    public List<Task> getTaskByTaskID(String taskID) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql = "SELECT t.task_id, t.Mentor_id, m.Full_name, t.taskname, t.[Description], t.StartTaskTime, t.EndTaskTime, t.TaskStatus, g.Group_Name, g.Group_ID\n"
                    + " FROM task t \n"
                    + " JOIN [group] g ON t.Group_ID = g.Group_ID \n"
                    + "JOIN Mentor m ON t.Mentor_id = m.Mentor_ID\n"
                    + "WHERE t.task_id = ? AND t.IsDeleted = 1";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getInt(10),
                        rs.getString(9),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDate(7),
                        rs.getString(8),
                        rs.getString(3));

                list.add(taskList);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Task> getTaskByTaskID1(String taskID) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql = "SELECT t.task_id, t.Mentor_id, m.Full_name, t.taskname, t.[Description], t.StartTaskTime, \n"
                    + "t.EndTaskTime, t.TaskStatus,g.Group_ID,g.Group_Name FROM task t \n"
                    + "JOIN [group] g ON t.Group_ID = g.Group_ID \n"
                    + "JOIN Mentor m ON t.Mentor_id = m.Mentor_ID\n"
                    + "WHERE t.task_id = ? AND t.isDeleted = 1\n"
                    + "GROUP BY t.task_id, t.Mentor_id, m.Full_name, t.taskname, t.[Description],\n"
                    + "t.StartTaskTime, t.EndTaskTime, t.TaskStatus,g.Group_ID,g.Group_Name";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(9),
                        rs.getString(10),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getDate(7),
                        rs.getString(8),
                        rs.getString(3));

                list.add(taskList);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Task> getAnswerTaskByIDForMentor(String taskID) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql = "SELECT t.Task_ID, t.Mentor_ID, t.Group_ID, g.Group_Name, t.TaskName, "
                    + "t.Description, t.StartTaskTime, t.EndTaskTime, t.TaskStatus, "
                    + "a.Answer_ID, a.AnswerContent, a.AnswerTime, a.Comment "
                    + "FROM Task t "
                    + "JOIN Answer a ON t.Task_ID = a.Task_ID "
                    + "JOIN [Group] g ON t.Group_ID = g.Group_ID "
                    + "WHERE t.Task_ID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task task = new Task(
                        rs.getString("Task_ID"),
                        rs.getString("Mentor_ID"),
                        rs.getInt("Group_ID"),
                        rs.getString("Group_Name"),
                        rs.getString("TaskName"),
                        rs.getString("Description"),
                        rs.getDate("StartTaskTime"),
                        rs.getDate("EndTaskTime"),
                        rs.getString("TaskStatus"),
                        rs.getInt("Answer_ID"),
                        rs.getString("AnswerContent"),
                        rs.getTimestamp("AnswerTime"),
                        rs.getString("Comment")
                );
                list.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void updateTaskStatus(String taskID) {
        connection = this.getConnection();

        String query = "UPDATE Task SET TaskStatus = 0  WHERE task_ID = ?";
        try (
                PreparedStatement ps = connection.prepareStatement(query)) {

            ps.setString(1, taskID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Task> searchTask(String query, int mentor_id) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql
                    = "select t.task_id,t.Mentor_id,m.Full_name ,g.Group_ID,g.Group_Name,t.taskname,t.[Description], t.StartTaskTime,t.EndTaskTime,t.TaskStatus from task t join \n"
                    + "[group] g on t.Group_ID = g.Group_ID\n"
                    + "join Mentor m on t.Mentor_id = m.Mentor_ID\n"
                    + "where isDeleted = 1 and m.Mentor_ID = ? and t.TaskName like ? ORDER BY g.Group_Name ;";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentor_id);
            ps.setString(2, "%" + query.trim() + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getString(3));
                list.add(taskList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
