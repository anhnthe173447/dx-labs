/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Semester;

/**
 *
 * @author admin
 */
public class SemesterDAO extends DBContext {

    public List<Semester> getAllSemester() {

        Connection connection = null;
        List<Semester> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql
                    = "SELECT * FROM semester";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {

                Semester semesterList = new Semester(rs.getInt("Semester_ID"),
                        rs.getString("SemesterName"),
                        rs.getDate("StartDate"),
                        rs.getDate("EndDate"));

                list.add(semesterList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

}
