/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Group;

/**
 *
 * @author Laptop K1
 */
public class GetGroupDAO extends DBContext {

    public List<Group> getGroupByUserID(int userid) {
        List<Group> list = new ArrayList<>();
        try (Connection connection = this.getConnection()) {
            String sql = "  SELECT g.Group_ID,g.Group_Name, g.Mentor_ID\n"
                    + "  FROM [Group] g \n"
                    + "  join Mentor m on g.Mentor_ID = m.Mentor_ID where m.UserID = ?";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setInt(1, userid);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Group group = new Group(rs.getInt(1), rs.getString(2), rs.getInt(3));

                        list.add(group);
                    }
                }
            }

        } catch (Exception e) {

        }
        return list;
    }

    public int getHighestGroupId() {
        String sql = "SELECT MAX(Group_ID) AS maxGroupId FROM [dbo].[Group]";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            if (rs.next()) {
                return rs.getInt("maxGroupId");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // Return 0 if no Group_ID is found (e.g., if the table is empty)
        return 0;
    }

    public boolean groupNameExists(String groupName, int mentorId) {
        String sql = "SELECT COUNT(*) FROM [Group] WHERE Group_Name = ? AND Mentor_ID = ?";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql)) {

            st.setString(1, groupName);
            st.setInt(2, mentorId);

            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    int count = rs.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In thông báo lỗi để dễ dàng kiểm tra khi xảy ra lỗi
        }
        return false;
    }

}
