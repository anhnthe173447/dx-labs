/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Accounts;

/**
 *
 * @author Laptop K1
 */
public class LoginDAO extends DBContext {
    
    protected Connection connection;
    
    public Accounts login(String username, String password) {
        connection = this.getConnection();
        if (connection == null) {
            System.out.println("Connection is null!");
            return null;
        }
        try {
            String sql = "select * "
                    + "from account "
                    + "where username "
                    + "collate latin1_general_bin = ? and password = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                return new Accounts(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
        
    }
}
