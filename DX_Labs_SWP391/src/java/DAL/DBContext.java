/**
 *
 * @author admin
 */
package DAL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBContext {

    protected Connection connection;

    public Connection getConnection() {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;databaseName=DXLab10";
            String user = "sa";

            String password = "123456";


            connection = DriverManager.getConnection(url, user, password);
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("Error " + e.getMessage() + " at DBContext");
            return null;
        }
    }

    public static void main(String[] args) {
        DBContext db = new DBContext();
        try {
            Connection connection = db.getConnection();
            if (connection != null) {
                System.out.println("ket noi thanh cong den co so du lieu");
            } else {
                System.out.println("khong ket noi duoc den co so du lieu");
            }
        } catch (Exception e) {
            System.out.println("ket noi that bai" + e.getMessage());
        }
    }

}
