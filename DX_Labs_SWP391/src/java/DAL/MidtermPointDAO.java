/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.FinaltermPoint;
import models.MidtermPoint;

/**
 *
 * @author admin
 */
public class MidtermPointDAO extends DBContext {

    protected Connection connection;

    public List<MidtermPoint> getListMidtermPoint(int userID) {

        Connection connection = null;
        List<MidtermPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql = "    SELECT Student.FullName, MidtermEvaluation.Midterm_ID, MidtermEvaluation.MajorSkillPoint, MidtermEvaluation.SoftSkillPoint, MidtermEvaluation.AttitudePoint,\n"
                    + "                    MidtermEvaluation.Comment, MidtermEvaluation.InternID \n"
                    + "                    FROM MidtermEvaluation JOIN Intern ON MidtermEvaluation.InternID = Intern.Intern_ID\n"
                    + "                    JOIN Student ON Intern.StudentID = Student.StudentID\n"
                    + "					Join [Group] on [Group].Group_ID = Intern.Group_ID\n"
                    + "					where [Group].Mentor_ID =(select Mentor_ID\n"
                    + "					from Mentor join Account on Mentor.UserID = Account.User_ID\n"
                    + "					where Account.User_ID = ?)";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            rs = ps.executeQuery();

            while (rs.next()) {
                MidtermPoint midtermPoint = new MidtermPoint(
                        rs.getInt("Midterm_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID"));

                list.add(midtermPoint);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    //insert
    public int getMaxMidtermID() {
        String sql = "SELECT MAX(Midterm_ID) FROM MidtermEvaluation";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean insertMidtermPoint(String internID, int midtermID, float majorPoints, float softSkillPoints, float attitudePoints, String comment) {
        String sql
                = "SET IDENTITY_INSERT MidtermEvaluation ON;"
                + "INSERT INTO MidtermEvaluation "
                + "(InternID, Midterm_ID, MajorSkillPoint, SoftSkillPoint, AttitudePoint, Comment) "
                + "VALUES (?, ?, ?, ?, ?, ?, 0);"
                + "SET IDENTITY_INSERT MidtermEvaluation OFF";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, internID);
            ps.setInt(2, midtermID);
            ps.setFloat(3, majorPoints);
            ps.setFloat(4, softSkillPoints);
            ps.setFloat(5, attitudePoints);
            ps.setString(6, comment);

            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    //detail
    public MidtermPoint getMidtermPointByInternID(int internID) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        MidtermPoint midtermPoint = null;

        try {
            connection = this.getConnection();
            String sql = "select MidtermEvaluation.Midterm_ID, Student.FullName ,MidtermEvaluation.MajorSkillPoint, MidtermEvaluation.SoftSkillPoint, MidtermEvaluation.AttitudePoint, MidtermEvaluation.Comment,\n"
                    + "MidtermEvaluation.InternID\n"
                    + "from MidtermEvaluation join Intern on MidtermEvaluation.InternID = Intern.Intern_ID\n"
                    + "join Student on Intern.StudentID = Student.StudentID\n"
                    + "where MidtermEvaluation.InternID = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, internID);
            rs = ps.executeQuery();

            if (rs.next()) {
                midtermPoint = new MidtermPoint(
                        rs.getInt("Midterm_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return midtermPoint;
    }

    //update
    public boolean updateMidtermPoint(MidtermPoint midtermPoint) {
        String sql = "UPDATE MidtermEvaluation\n"
                + "SET MajorSkillPoint = ?,\n"
                + "SoftSkillPoint = ?,\n"
                + "AttitudePoint = ?,\n"
                + "Comment = ?\n"
                + "WHERE InternID = ? AND IsDeleted = 0";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setFloat(1, midtermPoint.getMajorPoints());
            ps.setFloat(2, midtermPoint.getSoftSkillPoints());
            ps.setFloat(3, midtermPoint.getAttitudePoints());
            ps.setString(4, midtermPoint.getComment());
            ps.setInt(5, midtermPoint.getInternID());

            int rowsAffected = ps.executeUpdate();
            System.out.println("Số lượng bản ghi bị ảnh hưởng: " + rowsAffected);
            return rowsAffected > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    public boolean deleteMidtermPoint(MidtermPoint midtermP) {
        String sql
                = "  delete MidtermEvaluation\n"
                + "  where Midterm_ID = ?";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            int rowsAffected = ps.executeUpdate();
            System.out.println("Số lượng bản ghi bị ảnh hưởng: " + rowsAffected);
            return rowsAffected > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public int saveMidtermPoint(MidtermPoint midtermPoint) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();

            // Check if the record already exists
            String checkSql = "SELECT COUNT(*) AS count FROM MidtermEvaluation WHERE InternID = ?";
            ps = connection.prepareStatement(checkSql);
            ps.setInt(1, midtermPoint.getInternID());
            rs = ps.executeQuery();

            if (rs.next() && rs.getInt("count") > 0) {
                // Update the existing record
                String updateSql = "UPDATE MidtermEvaluation SET MajorSkillPoint = ?, SoftSkillPoint = ?, AttitudePoint = ?, "
                        + "Comment = ? "
                        + "WHERE InternID = ? ";
                ps = connection.prepareStatement(updateSql);
                ps.setFloat(1, midtermPoint.getMajorPoints());
                ps.setFloat(2, midtermPoint.getSoftSkillPoints());
                ps.setFloat(3, midtermPoint.getAttitudePoints());
                ps.setString(4, midtermPoint.getComment());
                ps.setInt(5, midtermPoint.getInternID());

                return ps.executeUpdate(); // Return the number of rows affected (should be 1 if successful)
            } else {
                // Insert new record if it doesn't exist
                String insertSql = "INSERT INTO MidtermEvaluation (InternID, MajorSkillPoint, SoftSkillPoint, AttitudePoint, Comment, IsDeleted) "
                        + "VALUES (?, ?, ?, ?, ?,0)";
                ps = connection.prepareStatement(insertSql);
                ps.setInt(1, midtermPoint.getInternID());
                ps.setFloat(2, midtermPoint.getMajorPoints());
                ps.setFloat(3, midtermPoint.getSoftSkillPoints());
                ps.setFloat(4, midtermPoint.getAttitudePoints());
                ps.setString(5, midtermPoint.getComment());

                return ps.executeUpdate(); // Return the number of rows affected (should be 1 if successful)
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0; // Return 0 if operation fails
    }

    public List<MidtermPoint> getListMidtermPointBySemester() {

        Connection connection = null;
        List<MidtermPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql = "select MidtermEvaluation.Midterm_ID, Student.FullName,\n"
                    + "MidtermEvaluation.MajorSkillPoint, MidtermEvaluation.SoftSkillPoint, MidtermEvaluation.AttitudePoint, MidtermEvaluation.Comment,\n"
                    + "MidtermEvaluation.InternID\n"
                    + "from MidtermEvaluation join Intern\n"
                    + "on MidtermEvaluation.InternID = Intern.Intern_ID join Semester\n"
                    + "on Intern.Semester_ID = Semester.Semester_ID join Student\n"
                    + "on Student.StudentID = Intern.StudentID\n"
                    + "where Semester.Semester_ID = (select Semester_ID\n"
                    + "								from Semester\n"
                    + "								where CURRENT_TIMESTAMP between StartDate and EndDate)";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                MidtermPoint midtermPoint = new MidtermPoint(
                        rs.getInt("Midterm_ID"),
                        rs.getString("FullName"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getString("Comment"),
                        rs.getInt("InternID"));

                list.add(midtermPoint);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

}
