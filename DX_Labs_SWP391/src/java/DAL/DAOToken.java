/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import models.Accounts;
import models.TokenForgotPassword;

/**
 *
 * @author admin
 */
public class DAOToken extends DBContext {

    //common token DAO
    public String getFormatDate(LocalDateTime myDateObj) {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);
        return formattedDate;
    }

    public boolean insertTokenForget(TokenForgotPassword tokenForget) {
        String sql = "INSERT INTO tokenForgetPassword (User_ID, Token, expiryTime, isUsed) "
                + "VALUES (?, ?, ?, ?)";
        try {
            // Kiểm tra xem kết nối có null hay không
            if (connection == null || connection.isClosed()) {
                System.err.println("Kết nối đến cơ sở dữ liệu không tồn tại hoặc đã bị đóng.");
                return false;
            }

            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, tokenForget.getUserID());
            ps.setString(2, tokenForget.getToken());
            ps.setTimestamp(3, Timestamp.valueOf(getFormatDate(tokenForget.getExpiryTime())));
            ps.setBoolean(4, tokenForget.isIsUsed());

            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public TokenForgotPassword getTokenPassword(String token) {
        String sql = "select *\n"
                + "from tokenForgetPassword\n"
                + "where Token = ?";
        try {
            connection = this.getConnection();
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, token);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new TokenForgotPassword(
                        rs.getInt("TokenID"),
                        rs.getInt("User_ID"),
                        rs.getString("Token"),
                        rs.getTimestamp("expiryTime").toLocalDateTime(),
                        rs.getBoolean("isUsed")
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateStatus(TokenForgotPassword token) {
        System.out.println("Updating token status: " + token);
        String sql = "UPDATE tokenForgetPassword SET isUsed = ? WHERE Token = ?";
        PreparedStatement st = null; // Khai báo st trước khối try
        try {
            connection = this.getConnection();
            st = connection.prepareStatement(sql);
            st.setBoolean(1, token.isIsUsed());
            st.setString(2, token.getToken());
            int rowsUpdated = st.executeUpdate();
            System.out.println("Rows updated: " + rowsUpdated);
        } catch (SQLException e) {
            e.printStackTrace();  // In lỗi chi tiết để dễ dàng debug hơn
        } finally {
            // Đóng PreparedStatement và Connection để tránh rò rỉ tài nguyên
            try {
                if (st != null) {
                    st.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    //common token DAO

    //FORGOT PASSWORD FOR INTERN
    public Accounts getAccountByEmail(String email) {
        String sql = "SELECT Account.User_ID, Account.Role_ID, Student.Email FROM Account \n"
                + "  join Intern on Account.User_ID = Intern.User_ID\n"
                + "  join Student on Intern.StudentID = Student.StudentID\n"
                + "  where Student.Email = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Accounts acc = new Accounts();
                acc.setID(rs.getInt("User_ID"));
                acc.setEmail(rs.getString("Email"));
                acc.setRole(rs.getInt("Role_ID"));
                return acc;
            } else {
                sql
                        = "  SELECT Account.User_ID, Account.Role_ID, Mentor.Email FROM Account\n"
                        + "  join Mentor on Mentor.UserID = Account.User_ID\n"
                        + "  where Mentor.Email = ?";
                ps = connection.prepareStatement(sql);
                ps.setString(1, email);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Accounts acc = new Accounts();
                    acc.setID(rs.getInt("User_ID"));
                    acc.setEmail(rs.getString("Email"));
                    acc.setRole(rs.getInt("Role_ID"));
                    return acc;
                } else {
                    sql
                            = "SELECT Account.User_ID, Account.Role_ID, HR.Email FROM Account \n"
                            + "	Join HR on HR.UserID = Account.User_ID\n"
                            + "	where HR.Email = ?";
                    ps = connection.prepareStatement(sql);
                    ps.setString(1, email);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        Accounts acc = new Accounts();
                        acc.setID(rs.getInt("User_ID"));
                        acc.setEmail(rs.getString("Email"));
                        acc.setRole(rs.getInt("Role_ID"));
                        return acc;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public Accounts getAccountByUserID(int userID) {
        String sql = "Select Account.User_ID, Student.Email\n"
                + "from Account join Intern on Account.User_ID = Intern.User_ID\n"
                + "join Student on Student.StudentID = Intern.StudentID\n"
                + "Where Account.IsActive = 1 AND Account.User_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Accounts acc = new Accounts();
                acc.setID(rs.getInt("User_ID"));
                acc.setEmail(rs.getString("Email"));
                return acc;
            } else {
                sql
                        = "select Account.User_ID, Mentor.Email\n"
                        + "from Account join Mentor on Account.User_ID = Mentor.UserID\n"
                        + "WHERE Account.IsActive = 1 AND Account.User_ID = ?";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, userID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Accounts acc = new Accounts();
                    acc.setID(rs.getInt("User_ID"));
                    acc.setEmail(rs.getString("Email"));
                    return acc;
                } else {
                    sql
                            = "SELECT Account.User_ID, Mentor.Email \n"
                            + "	Join HR on HR.UserID = Account.User_ID\n"
                            + "	where Account.IsActive = 1 AND Account.User_ID = ?";
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1, userID);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        Accounts acc = new Accounts();
                        acc.setID(rs.getInt("User_ID"));
                        acc.setEmail(rs.getString("Email"));
                        return acc;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public void updatePassword(int userID, String password) {
        String sql
                = "  update Account\n"
                + "  set Password = ?\n"
                + "  where User_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, password);
            st.setInt(2, userID);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Accounts getFullnameOfAccounts(int userID) {
        String sql
                = "SELECT Student.FullName FROM Account \n"
                + "JOIN Intern ON Account.User_ID = Intern.User_ID\n"
                + "JOIN Student ON Intern.StudentID = Student.StudentID\n"
                + "WHERE Account.User_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Accounts acc = new Accounts(sql);
                acc.setFullname(rs.getString("FullName"));
                return acc;
            } else {
                sql
                        = "  SELECT Mentor.Full_name FROM Account \n"
                        + "JOIN Mentor ON Mentor.UserID = Account.User_ID\n"
                        + "WHERE Account.User_ID = ?";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, userID);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Accounts acc = new Accounts(sql);
                    acc.setFullname(rs.getString("Full_name"));
                    return acc;
                } else {
                    sql
                            = "SELECT HR.FullName FROM Account \n"
                            + "JOIN HR ON HR.UserID = Account.User_ID\n"
                            + "WHERE Account.User_ID = ?";
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1, userID);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        Accounts acc = new Accounts(sql);
                        acc.setFullname(rs.getString("FullName"));
                        return acc;
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        DAOToken daoToken = new DAOToken();
        Accounts account = daoToken.getFullnameOfAccounts(1);
        if (account != null) {
            System.out.println("Full Name: " + account.getFullname());
        } else {
            System.out.println("Account not found or error occurred.");
        }
    }
}
