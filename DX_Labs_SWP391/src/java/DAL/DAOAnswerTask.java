/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import models.Answer;
import models.Mentor;

public class DAOAnswerTask extends DBContext {

    public boolean insertAnswerTask(String taskID, String groupID, String answerContent, Timestamp answerTime) {
        connection = this.getConnection();
        PreparedStatement ps = null;
        boolean result = false;
        try {
            String sql = "INSERT INTO Answer (AnswerContent, Task_ID, AnswerTime, Group_ID) VALUES (?, ?, ?, ?)";
            ps = connection.prepareStatement(sql);
            ps.setString(1, answerContent);
            ps.setString(2, taskID);
            ps.setTimestamp(3, answerTime);
            ps.setString(4, groupID);
            int status = ps.executeUpdate();
            result = status > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Answer> getAnswersForTask(int taskID, int groupID) {
        List<Answer> answers = new ArrayList<>();
        String sql = "SELECT * FROM Answer WHERE Task_ID = ? AND Group_ID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, taskID);
            ps.setInt(2, groupID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer answer = new Answer(rs.getInt(1),
                        rs.getBlob(2).toString(),
                        rs.getTimestamp(4),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return answers;
    }

    public Answer getAnswerByTask(String taskID) {
        connection = this.getConnection();
        String sql = "select * from answer where Task_ID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer answer = new Answer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getTimestamp(4),
                        rs.getInt(6)
                );
                return answer;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateAnswer(String taskID, String answerContent, Timestamp time, String groupid) {
        connection = this.getConnection();
        String sql = "UPDATE [DXLab10].[dbo].[Answer]\n"
                + "SET \n"
                + "[AnswerContent] = ?,"
                + "[AnswerTime] = ?,"
                + "[Group_ID] = ?\n"
                + "WHERE \n"
                + "[Task_ID] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps = connection.prepareStatement(sql);
            ps.setString(1, answerContent);
            ps.setTimestamp(2, time);
            ps.setString(3, groupid);
            ps.setString(4, taskID);
            ResultSet rs = ps.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean answerExists(String taskID, String groupID) {
        connection = this.getConnection();
        String sql = "SELECT COUNT(*) FROM [DXLab10].[dbo].[Answer] WHERE [Task_ID] = ? AND [Group_ID] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, taskID);
            ps.setString(2, groupID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updatePointAnswerTask(String taskID, String comment) {
        connection = this.getConnection();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE [DXLab10].[dbo].[Answer]\n"
                    + "SET [Comment] = ?\n"
                    + "WHERE Task_ID = ?;";
            ps = connection.prepareStatement(sql);
            ps.setString(1, comment);
            ps.setString(2, taskID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception ex) {

        }
    }

public List<Mentor> getAllAnswerForMentor(int userID) {
    List<Mentor> mentorList = new ArrayList<>();
    String sql = "SELECT a.User_ID, g.Group_ID, g.Group_Name, t.Task_ID, t.TaskName, t.Description, "
               + "t.EndTaskTime, t.TaskStatus, ans.AnswerContent, ans.AnswerTime, ans.Comment "
               + "FROM Mentor m "
               + "JOIN Account a ON m.UserID = a.User_ID "
               + "JOIN [Group] g ON m.Mentor_ID = g.Mentor_ID "
               + "JOIN [Task] t ON g.Group_ID = t.Group_ID "
               + "JOIN Answer ans ON g.Group_ID = ans.Group_ID AND t.Task_ID = ans.Task_ID "
               + "WHERE a.User_ID = ?";
    try {
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, userID);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Mentor mentor = new Mentor(
                rs.getString("User_ID"),
                rs.getString("Group_ID"),
                rs.getString("Group_Name"),
                rs.getString("Task_ID"),
                rs.getString("TaskName"),
                rs.getString("Description"),
                rs.getString("EndTaskTime"),
                rs.getInt("TaskStatus"),
                rs.getString("AnswerContent"),
                rs.getString("AnswerTime"),
                rs.getString("Comment")
            );
            mentorList.add(mentor);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return mentorList;
}

}
