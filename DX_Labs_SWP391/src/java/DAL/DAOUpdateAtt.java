/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import models.Attendance;

/**
 *
 * @author Laptop K1
 */
public class DAOUpdateAtt extends DBContext {

    public void updateAtt(List<Attendance> attendanceList) {
        connection = this.getConnection();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE Attendance\n"
                    + " SET attendance_statusID = ?,\n"
                    + " reason = ?\n"
                    + " WHERE id_intern = ?;";
            ps = connection.prepareStatement(sql);

            for (Attendance att : attendanceList) {
                ps.setInt(1, att.getAttendanceStatusid());
                ps.setString(2, att.getReason());
                ps.setString(3, att.getIdIntern());
                ps.addBatch();
            }

            ps.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        } 
    }
}
