/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.Interns;
import models.Mentor;
import models.Task;

/**
 *
 * @author Laptop K1
 */
public class GetInternByUserID extends DBContext {
    public Interns getInternByUserID(int userid) {
        connection = this.getConnection();
        Interns list = new Interns();
        try {
            String sql = "select i.Intern_ID,i.User_ID,i.StudentID,i.Group_ID,i.Semester_ID,i.InternStatus from Intern i join account a on i.[user_id] = a.[user_id] where i.user_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list = new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getBoolean(6));

            }
        } catch (Exception e) {
        }
        return list;
    }
    public Mentor getMentorByUserID(int userid) {

        connection = this.getConnection();
        Mentor list = new Mentor();
        try {
            String sql = "select * from Mentor  where UserID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list = new Mentor(rs.getInt(1),
                        rs.getString(9),
                        rs.getString(2),
                        rs.getString(4),
                        rs.getString(3),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(8),
                        rs.getString(7));

            }
        } catch (Exception e) {
        }
        return list;
    }
}
