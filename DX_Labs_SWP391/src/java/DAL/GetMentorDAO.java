/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;

import models.Mentor;

/**
 *
 * @author ADMIN
 */
public class GetMentorDAO extends DBContext {

    protected Connection connection;

    public List<Mentor> getMentor() {
        connection = this.getConnection();
        List<Mentor> list = new ArrayList<>();
        try {
            String sql = "select * from [Mentor]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Mentor listMentor = new Mentor(rs.getInt(1),
                        rs.getString(9),
                        rs.getString(2),
                        rs.getString(4),
                        rs.getString(3),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(8),
                        rs.getString(7)
                );
                list.add(listMentor);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //Intern 2
    public Mentor getMentorByID(String ID) {
        connection = this.getConnection();
        try {
            String sql = "select * from Mentor\n"
                    + "where Mentor_ID=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, ID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Mentor(rs.getInt(1),
                        rs.getString(9),
                        rs.getString(2),
                        rs.getString(4),
                        rs.getString(3),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(8),
                        rs.getString(7)
                );

            }
        } catch (Exception e) {
        }
        return null;
    }

    public Mentor getMentorByIDAccount(String ID) {
        connection = this.getConnection();
        try {
            String sql = "select * from Mentor\n"
                    + "where UserID=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, ID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Mentor(rs.getInt(1),
                        rs.getString(9),
                        rs.getString(2),
                        rs.getString(4),
                        rs.getString(3),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(8),
                        rs.getString(7)
                );

            }
        } catch (Exception e) {
        }
        return null;
    }

    public void UpdateMentor(String ID, String userId, String fullName, String email, String phone, String gender, String dob, String idCard, String Address) {
        connection = this.getConnection();
        String query = "UPDATE [dbo].[Mentor]\n"
                + "   SET [Mentor_ID] = ?\n"
                + "      ,[phone_number] = ?\n"
                + "      ,[Full_name] = ?\n"
                + "      ,[Email] = ?\n"
                + "      ,[Gender] = ?\n"
                + "      ,[DOB] = ?\n"
                + "      ,[Address] = ?\n"
                + "      ,[ID_Card] = ?\n"
                + "      ,[UserID] = ?\n"
                + " WHERE Mentor_ID=?";
        try {

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, ID);
            ps.setString(2, phone);
            ps.setString(3, fullName);
            ps.setString(4, email);
            ps.setString(5, gender);
            ps.setString(6, dob);
            ps.setString(7, Address);
            ps.setString(8, idCard);
            ps.setString(9, userId);
            ps.setString(10, ID);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Mentor> getAllMentors() {
        Connection connection = this.getConnection();
        List<Mentor> mentors = new ArrayList<>();
        String sql = "SELECT * FROM mentor";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Mentor mentor = new Mentor();
                mentor.setMentorId(rs.getInt("mentor_id"));
                mentor.setFullName(rs.getString("full_name"));
                mentor.setPhone(rs.getString("phone_number"));
                mentor.setEmail(rs.getString("email"));
                mentor.setEmail(rs.getString("email"));
                mentor.setGender(rs.getString("Gender"));
                mentor.setDob(rs.getString("dob"));
                mentor.setAddress(rs.getString("address"));
                mentor.setUserId(rs.getString("userId"));
                mentors.add(mentor);

            }
        } catch (SQLException e) {

        }
        return mentors;
    }

    public int getNewUserId() {
        String sql = "SELECT TOP 1 User_ID FROM Account ORDER BY user_Id DESC";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {
            if (rs.next()) {
                return rs.getInt("user_id") + 1;
            }
        } catch (SQLException e) {

        }
        return 0;
    }

    public int getHighestMentorId() {
        String sql = "SELECT TOP 1 mentor_Id FROM Mentor ORDER BY mentor_Id DESC";
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getInt("mentor_Id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean insertAccount(Accounts account) {
        String sql = "INSERT INTO Account (user_id, userName, password, image, isActive, role_id) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, account.getID()); // Sử dụng ID từ đối tượng account
            statement.setString(2, account.getUserName());
            statement.setString(3, account.getPass());
            statement.setString(4, account.getImg());
            statement.setString(5, account.getIsActive());
            statement.setInt(6, account.getRole());
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {

        }
        return false;
    }

    public boolean insertMentor(Mentor mentor) {
        String sql = "INSERT INTO Mentor (Mentor_ID, Full_name, phone_number, Email, Gender, DOB, Address, ID_Card, UserID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, mentor.getMentorId());
            statement.setString(2, mentor.getFullName());
            statement.setString(3, mentor.getPhone());
            statement.setString(4, mentor.getEmail());
            statement.setString(5, mentor.getGender());
            statement.setString(6, mentor.getDob());
            statement.setString(7, mentor.getAddress());
            statement.setString(8, mentor.getIdCard());
            statement.setString(9, mentor.getUserId());
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {

        }
        return false;
    }

    public boolean deleteAccount(int userId) {
        String sql = "DELETE FROM Account WHERE user_Id = ?";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, userId);
            int rowsAffected = st.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean emailExists(String email) {
        String sql = "SELECT COUNT(*) FROM mentor WHERE email = ?";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, email);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean phoneNumberExists(String phoneNumber) {
        String sql = "SELECT COUNT(*) FROM mentor WHERE phone_number = ?";
        try (Connection connection = getConnection(); PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, phoneNumber);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
