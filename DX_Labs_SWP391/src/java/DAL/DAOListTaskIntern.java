/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.Task;

/**
 *
 * @author Laptop K1
 */
public class DAOListTaskIntern extends DBContext {
    public List<Task> getListTaskIntern(int groupID) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql = "select t.task_id,t.Mentor_id,m.Full_name ,g.Group_ID,g.Group_Name,t.taskname,t.[Description], t.StartTaskTime,t.EndTaskTime,t.TaskStatus from task t join \n"
                    + " [group] g on t.Group_ID = g.Group_ID\n"
                    + "  join Mentor m on t.Mentor_id = m.Mentor_ID\n"
                    + "  where isDeleted = 1 and g.Group_ID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, groupID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getString(3));

                list.add(taskList);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public List<Task> getListTask(int mentorID) {
        connection = this.getConnection();
        List<Task> list = new ArrayList<>();
        try {
            String sql = "select t.task_id,t.Mentor_id,m.Full_name ,g.Group_ID,g.Group_Name,t.taskname,t.[Description], t.StartTaskTime,t.EndTaskTime,t.TaskStatus from task t join \n"
                    + "[group] g on t.Group_ID = g.Group_ID\n"
                    + "join Mentor m on t.Mentor_id = m.Mentor_ID\n"
                    + "where isDeleted = 1 and m.Mentor_ID = ? ORDER BY g.Group_Name;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentorID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task taskList = new Task(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getString(3));

                list.add(taskList);
            }
        } catch (Exception e) {
        }
        return list;
    }

}
