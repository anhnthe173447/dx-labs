/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.MidtermPoint;
import models.OverallPoint;
import models.ReportMidtermPoint;
import models.ReportOverallPoint;

/**
 *
 * @author ADMIN
 */
public class ReportDAO extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    //get list
    public List<ReportOverallPoint> getListOverallEvaluationP() {

        Connection connection = null;
        List<ReportOverallPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql = "SELECT \n"
                    + "    Student.StudentID, \n"
                    + "    Intern.Intern_ID, \n"
                    + "    Student.FullName, \n"
                    + "    Intern.Division, \n"
                    + "    Mentor.Full_name AS MentorName, \n"
                    + "    ReportOverall.Allowance, \n"
                    + "    ReportOverall.Comment, \n"
                    + "    FORMAT(OverallEvaluation.MajorSkillPoint, 'N2') AS MajorSkillPoint, \n"
                    + "    FORMAT(OverallEvaluation.SoftSkillPoint, 'N2') AS SoftSkillPoint, \n"
                    + "    FORMAT(OverallEvaluation.AttitudePoint, 'N2') AS AttitudePoint, \n"
                    + "    FORMAT(OverallEvaluation.FinalResult, 'N2') AS FinalResult\n"
                    + "FROM \n"
                    + "    ReportOverall\n"
                    + "JOIN \n"
                    + "    OverallEvaluation \n"
                    + "    ON ReportOverall.OverallEval_ID = OverallEvaluation.OverallEval_ID\n"
                    + "JOIN \n"
                    + "    Intern \n"
                    + "    ON OverallEvaluation.Intern_ID = Intern.Intern_ID\n"
                    + "JOIN \n"
                    + "    [Group] \n"
                    + "    ON [Group].Group_ID = Intern.Group_ID\n"
                    + "JOIN \n"
                    + "    Mentor \n"
                    + "    ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                    + "JOIN \n"
                    + "    Student \n"
                    + "    ON Intern.StudentID = Student.StudentID\n"
                    + "";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                ReportOverallPoint finalEvaluationList = new ReportOverallPoint(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                );

                list.add(finalEvaluationList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public ReportOverallPoint getReportOverallByInternById(String id) {
        connection = this.getConnection();
        String sql = "SELECT \n"
                + "    Student.StudentID,\n"
                + "    Intern.Intern_ID,\n"
                + "    Student.FullName,\n"
                + "    Intern.Division,\n"
                + "    Mentor.Full_name,\n"
                + "    ReportOverall.Allowance,\n"
                + "    ReportOverall.Comment,\n"
                + "    OverallEvaluation.MajorSkillPoint,\n"
                + "    OverallEvaluation.SoftSkillPoint,\n"
                + "    OverallEvaluation.AttitudePoint,\n"
                + "    OverallEvaluation.FinalResult\n"
                + "FROM \n"
                + "    ReportOverall\n"
                + "JOIN \n"
                + "    OverallEvaluation \n"
                + "    ON ReportOverall.OverallEval_ID = OverallEvaluation.OverallEval_ID\n"
                + "JOIN \n"
                + "    Intern \n"
                + "    ON OverallEvaluation.Intern_ID = Intern.Intern_ID\n"
                + "JOIN \n"
                + "    [Group] \n"
                + "    ON [Group].Group_ID = Intern.Group_ID\n"
                + "JOIN \n"
                + "    Mentor \n"
                + "    ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                + "JOIN \n"
                + "    Student \n"
                + "    ON Intern.StudentID = Student.StudentID\n"
                + "\n"
                + "WHERE \n"
                + "    Intern.Intern_ID = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                ReportOverallPoint report = new ReportOverallPoint(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11));
                return report;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public String getReportOverallIDByInternById(String id) {
        connection = this.getConnection();
        String sql = "SELECT \n"
                + "    ReportOverall.OverallEval_ID\n"
                + "FROM \n"
                + "    ReportOverall\n"
                + "JOIN \n"
                + "    OverallEvaluation \n"
                + "    ON ReportOverall.OverallEval_ID = OverallEvaluation.OverallEval_ID\n"
                + "JOIN \n"
                + "    Intern \n"
                + "    ON OverallEvaluation.Intern_ID = Intern.Intern_ID\n"
                + "JOIN \n"
                + "    [Group] \n"
                + "    ON [Group].Group_ID = Intern.Group_ID\n"
                + "JOIN \n"
                + "    Mentor \n"
                + "    ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                + "JOIN \n"
                + "    Student \n"
                + "    ON Intern.StudentID = Student.StudentID\n"
                + "WHERE \n"
                + "    Intern.Intern_ID = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String ReportOverall = rs.getString(1);
                return ReportOverall;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateReportOverall(String id, String allowance, String comments) {
        String query = "UPDATE [dbo].[ReportOverall]\n"
                + "   SET \n"
                + "      [Comment] = ?,\n"
                + "      [Allowance] = ?\n"
                + " WHERE [OverallEval_ID]= ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, comments);
            ps.setString(2, allowance);
            ps.setString(3, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void deleteReportOverall(String id) {
        String query = "DELETE FROM [dbo].[ReportOverall]\n"
                + "      WHERE OverallEval_ID =?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<ReportMidtermPoint> getListReportMidtern() {

        Connection connection = null;
        List<ReportMidtermPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql = "SELECT Student.StudentID,\n"
                    + "       Intern.Intern_ID,\n"
                    + "       Student.FullName,\n"
                    + "       Intern.Division,\n"
                    + "       Mentor.Full_name,\n"
                    + "       ReportMidterm.[Evaluate],\n"
                    + "	   ReportMidterm.Comment\n"
                    + "FROM ReportMidterm\n"
                    + "JOIN MidtermEvaluation ON MidtermEvaluation.Midterm_ID = ReportMidterm.Midterm_ID\n"
                    + "JOIN Intern ON MidtermEvaluation.InternID = Intern.Intern_ID\n"
                    + "JOIN [Group] ON [Group].Group_ID = Intern.Group_ID\n"
                    + "JOIN Mentor ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                    + "JOIN Student ON Intern.StudentID = Student.StudentID";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                ReportMidtermPoint finalEvaluationList = new ReportMidtermPoint(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                );

                list.add(finalEvaluationList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public ReportMidtermPoint getReportMidtermByInternById(String id) {
        connection = this.getConnection();
        String sql = "SELECT \n"
                + "    Student.StudentID,\n"
                + "    Intern.Intern_ID,\n"
                + "    Student.FullName,\n"
                + "    Intern.Division,\n"
                + "    Mentor.Full_name AS MentorName,\n"
                + "    ReportMidterm.Evaluate,\n"
                + "    ReportMidterm.Comment\n"
                + "FROM \n"
                + "    ReportMidterm\n"
                + "JOIN \n"
                + "    MidtermEvaluation \n"
                + "    ON MidtermEvaluation.Midterm_ID = ReportMidterm.Midterm_ID\n"
                + "JOIN \n"
                + "    Intern \n"
                + "    ON MidtermEvaluation.InternID = Intern.Intern_ID\n"
                + "JOIN \n"
                + "    [Group] \n"
                + "    ON [Group].Group_ID = Intern.Group_ID\n"
                + "JOIN \n"
                + "    Mentor \n"
                + "    ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                + "JOIN \n"
                + "    Student \n"
                + "    ON Intern.StudentID = Student.StudentID\n"
                + "WHERE \n"
                + "    Intern.Intern_ID = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                ReportMidtermPoint report = new ReportMidtermPoint(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                return report;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public String getReportMidtermIDByInternById(String id) {
        connection = this.getConnection();
        String sql = "SELECT \n"
                + "    ReportMidterm.Midterm_ID\n"
                + "FROM \n"
                + "    ReportMidterm\n"
                + "JOIN \n"
                + "    MidtermEvaluation \n"
                + "    ON MidtermEvaluation.Midterm_ID = ReportMidterm.Midterm_ID\n"
                + "JOIN \n"
                + "    Intern \n"
                + "    ON MidtermEvaluation.InternID = Intern.Intern_ID\n"
                + "JOIN \n"
                + "    [Group] \n"
                + "    ON [Group].Group_ID = Intern.Group_ID\n"
                + "JOIN \n"
                + "    Mentor \n"
                + "    ON [Group].Mentor_ID = Mentor.Mentor_ID\n"
                + "JOIN \n"
                + "    Student \n"
                + "    ON Intern.StudentID = Student.StudentID\n"
                + "WHERE \n"
                + "    Intern.Intern_ID = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String ReportMidtermID = rs.getString(1);
                return ReportMidtermID;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateReportEvalateMidtern(String id, String Evaluate) {
        String query = "UPDATE [dbo].[ReportMidterm]\n"
                + "   SET \n"
                + "      [Evaluate] = ?\n"
                + " WHERE [Midterm_ID]= ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, Evaluate);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateReportCommentMidtern(String id, String comments) {
        String query = "UPDATE [dbo].[ReportMidterm]\n"
                + "   SET \n"
                + "      [Comment] = ?\n"
                + " WHERE \n"
                + "      [Midterm_ID] = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, comments);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void deleteReportMidterm(String id) {
        String query = "DELETE FROM [dbo].[ReportMidterm]\n"
                + "      WHERE Midterm_ID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void AddReportMidterm(int Midterm_ID, String Comment, String Evaluate) {
        String query = "INSERT INTO [dbo].[ReportMidterm]\n"
                + "           ([Midterm_ID]\n"
                + "           ,[Comment]\n"
                + "           ,[Evaluate])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, Midterm_ID);
            ps.setString(2, Comment);
            ps.setString(3, Evaluate);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public boolean checkDuplicateReport(int idIntern) {
        ReportDAO daoR = new ReportDAO();
        String id = idIntern + "";
        List<ReportMidtermPoint> reportMidtermList = daoR.getListReportMidtern();
        for (ReportMidtermPoint reportMidtermPoint : reportMidtermList) {
            if (id.equals(reportMidtermPoint.getInternID())) {
                return false;
            }
        }
        return true;
    }

    public void AddReportOverall(int Overall_ID, String Comment, String Allowance) {
        String query = "INSERT INTO [dbo].[ReportOverall]\n"
                + "           ([OverallEval_ID]\n"
                + "           ,[Comment]\n"
                + "           ,[Allowance])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, Overall_ID);
            ps.setString(2, Comment);
            ps.setString(3, Allowance);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public boolean checkDuplicateReportOverall(int idIntern) {
        ReportDAO daoR = new ReportDAO();
        String id = idIntern + "";
        List<ReportOverallPoint> reportMidtermList = daoR.getListOverallEvaluationP();
        for (ReportOverallPoint reportOverallPoint : reportMidtermList) {
            if (id.equals(reportOverallPoint.getInternID())) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        OverallPointDAO f = new OverallPointDAO();
        ReportDAO daoR = new ReportDAO();
        List<OverallPoint> ListOverallPoint = f.getListOverallPoint(1);
        List<ReportOverallPoint> reportOverallList = new ArrayList<>();
        ReportOverallPoint report= new ReportOverallPoint("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0");

        for (OverallPoint overallPoint : ListOverallPoint) {
            ReportOverallPoint rp = daoR.getReportOverallByInternById(overallPoint.getInternID() + "");
            if (rp != null) {
                reportOverallList.add(rp);
            }
            if(rp==null){
                 reportOverallList.add(report);
            }
        }
        for (ReportOverallPoint reportOverallPoint : reportOverallList) {
            System.out.println(reportOverallPoint.getInternID());
        }
    }

}
