/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Mentor;

/**
 *
 * @author ThinkPro
 */
public class MentorDAO extends DBContext {

    protected Connection connection;

    public Mentor getMentorByUserId(int id) {
        connection = this.getConnection();
        String sql = "select * from Mentor where UserID = ?";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Mentor m = new Mentor();
                m.setMentorId(rs.getInt("Mentor_ID"));
                m.setFullName(rs.getString("Full_name"));
                m.setPhone(rs.getString("phone_number"));
                m.setEmail(rs.getString("Email"));
                m.setGender(rs.getBoolean("Gender") + "");
                m.setDob(rs.getDate("DOB") + "");
                m.setAddress(rs.getString("Address"));
                m.setIdCard(rs.getString("ID_Card"));
                m.setUserId(rs.getInt("UserID") + "");
                return m;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public Mentor getMentorByUserID(int userid) {

            connection = this.getConnection();
            Mentor list = new Mentor();
            try {
                String sql = "select * from Mentor  where UserID = ?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, userid);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    list = new Mentor(rs.getInt(1),
                            rs.getString(9),
                            rs.getString(2),
                            rs.getString(4),
                            rs.getString(3),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(8),
                            rs.getString(7));

                }
            } catch (Exception e) {
            }
            return list;
        }
    
     public Mentor getMentorByInternID(String internId) {

            connection = this.getConnection();
            Mentor list = new Mentor();
            try {
                String sql = "select Mentor.*\n"
                        + "from Intern \n"
                        + "join [Group] on Intern.Group_ID=[Group].Group_ID\n"
                        + "join Mentor on [Group].Mentor_ID=Mentor.Mentor_ID\n"
                        + "where Intern.Intern_ID=?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, internId);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    list = new Mentor(rs.getInt(1),
                            rs.getString(9),
                            rs.getString(2),
                            rs.getString(4),
                            rs.getString(3),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(8),
                            rs.getString(7));

                }
            } catch (Exception e) {
            }
            return list;
        }

}
