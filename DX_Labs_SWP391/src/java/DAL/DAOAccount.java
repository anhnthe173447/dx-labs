/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;

/**
 *
 * @author admin
 */
public class DAOAccount extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Accounts> getAllAccount() {
        String query = "select * from Account\n"
                + "where ([username] IS NOT NULL OR [password] IS NOT NULL) ";
        List<Accounts> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(4),
                        rs.getString(3),
                        rs.getString(5),
                        rs.getInt(6)
                )
                );
            }

        } catch (Exception e) {
        }
        return list;

    }

    public Accounts getAccountByID(String id) {
        String query = "Select * From Account\n"
                + "Where User_ID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {

                return new Accounts(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6)
                );
            }

        } catch (Exception e) {
        }
        return null;

    }

    public void editAccount(String id, String user, String pass, String role) {
        Accounts a = getAccountByID(id);
        String img = a.getImg();
        String query = "UPDATE [dbo].[Account]\n"
                + "   SET [User_ID] = ?\n"
                + "      ,[Role_ID] = ?\n"
                + "      ,[Password] = ?\n"
                + "      ,[UserName] = ?\n"
                + "      ,[IsActive] = 1\n"
                + "      ,[image] = ?\n"
                + "WHERE User_ID=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.setString(2, role);
            ps.setString(3, pass);
            ps.setString(4, user);
            ps.setString(5, img);
            ps.setString(6, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void DeleteAccount(String id) {
        String query = "DELETE FROM [dbo].[Account]\n"
                + "      WHERE User_ID =?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void AddAccount(String user, String pass, String role) {
        String query = "INSERT INTO [dbo].[Account]\n"
                + "           ([User_ID]\n"
                + "           ,[UserName]\n"
                + "           ,[Password]\n"
                + "           ,[image]\n"
                + "           ,[IsActive]\n"
                + "           ,[Role_ID])\n"
                + "     VALUES\n"
                + "           ((SELECT ISNULL(MAX(User_ID), 0) + 1 FROM [dbo].[Account])\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,'images/avatar/9-anh-dai-dien-trang-inkythuatso-03-15-27-03.jpg'\n"
                + "           ,1\n"
                + "           ,?)";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(3, role);
            ps.setString(2, pass);
            ps.setString(1, user);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public String getUserID() {
        String query = "SELECT MAX(User_ID) AS MaxUserID FROM Account;";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getString("MaxUserID");
            }

        } catch (Exception e) {
        }
        return null;

    }

    public void UpdateAccountIntern(String IDIntern, String UserID) {
        String query = "UPDATE [dbo].[Intern]\n"
                + "   SET\n"
                + "      [User_ID] = ?\n"
                + "\n"
                + " WHERE Intern_ID=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, UserID);
            ps.setString(2, IDIntern);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public boolean isDuplicateUsername(String username) {
        String query = "Select Count(*) from Account\n"
                + "Where UserName=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next() && rs.getInt(1) > 0) {
                return true; // Username đã tồn tại
            }
        } catch (Exception e) {

        }
        return false; // Username không tồn tại
    }

    //intern 2
    public void UpdateAccount(String id, String user, String pass, String role, String img) {
        String query = "UPDATE [dbo].[Account]\n"
                + "   SET [User_ID] = ?\n"
                + "      ,[Role_ID] = ?\n"
                + "      ,[Password] = ?\n"
                + "      ,[UserName] = ?\n"
                + "      ,[image] = ?\n"
                + "WHERE User_ID=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.setString(2, role);
            ps.setString(3, pass);
            ps.setString(4, user);
            ps.setString(5, img);
            ps.setString(6, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void UpdatePass(String id, String pass) {
        String query = "UPDATE [dbo].[Account]\n"
                + "   SET \n"
                + "      [Password] =?\n"
                + " WHERE User_ID=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, pass);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        DAOAccount dao = new DAOAccount();
        List<Accounts> list = dao.getAllAccount();
        Accounts acc = dao.getAccountByID("1");
        System.out.println(acc.getID());

        for (Accounts a : list) {
            System.out.println(a.getUserName());
        }
    }

    public Accounts checkAccountRole(int roleID) {
        Accounts account = new Accounts();
        String query = "select * from Account"
                + "  where Role_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, roleID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Accounts acc = new Accounts();

                acc.setID(rs.getInt("User_ID"));
                acc.setUserName(rs.getString("UserName"));
                acc.setPass(rs.getString("Password"));
                acc.setImg(rs.getString("Image"));
                acc.setIsActive(rs.getString("IsActive"));
                acc.setRole(rs.getInt("Role_ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return account;
    }

    public Accounts getImageOfAccountByInternID(int internID) {
        String query = "select Account.image\n"
                + "from Account\n"
                + "join Intern on Account.User_ID = Intern.User_ID\n"
                + "join MidtermEvaluation on Intern.Intern_ID = MidtermEvaluation.InternID\n"
                + "where Intern.Intern_ID = ?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, internID);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Accounts(rs.getString("image")
                );
            }

        } catch (Exception e) {
        }
        return null;

    }
}
