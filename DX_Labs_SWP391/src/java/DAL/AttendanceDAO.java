/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;

import java.util.Date;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import models.InternAttendance;
import models.Student;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import models.Group;

/**
 *
 * @author FPT
 */
public class AttendanceDAO extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public boolean checkInAttendance(String internID, Date checkInTime) {
        String query = "INSERT INTO AttendanceTime (CheckInTime, CheckingStatus, Intern_ID) VALUES (?, ?, ?)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(checkInTime));
            ps.setString(2, "CheckedIn");
            ps.setString(3, internID);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean checkOutAttendance(String internID, Date checkOutTime) {
        String query = "UPDATE AttendanceTime SET CheckOutTime = ?, CheckingStatus = ? WHERE Intern_ID = ? AND CAST(CheckInTime AS DATE) = CAST(GETDATE() AS DATE)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(checkOutTime));
            ps.setString(2, "CheckedOut");
            ps.setString(3, internID);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String getAttendanceStatus(String internID) {
        String status = "";
        String query = "SELECT CheckingStatus FROM AttendanceTime WHERE Intern_ID = ? AND CONVERT(date, CheckInTime) = CONVERT(date, GETDATE())";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, internID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                status = rs.getString("CheckingStatus");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public boolean updateAttendance(InternAttendance[] internAttendance) {
        String checkIfExistsQuery = "SELECT COUNT(*) FROM AttendanceTime WHERE Intern_ID = ? AND CAST(CheckInTime AS DATE) = CAST(GETDATE() AS DATE)";
        String insertQuery = "INSERT INTO AttendanceTime (CheckInTime, CheckOutTime, CheckingStatus, Intern_ID) VALUES (?, ?, ?, ?)";
        String updateCheckInQuery = "UPDATE AttendanceTime SET CheckInTime = ?, CheckingStatus = ? WHERE Intern_ID = ? AND CAST(CheckInTime AS DATE) = CAST(GETDATE() AS DATE)";
        String updateCheckOutQuery = "UPDATE AttendanceTime SET CheckOutTime = ?, CheckingStatus = ? WHERE Intern_ID = ? AND CAST(CheckInTime AS DATE) = CAST(GETDATE() AS DATE)";

        Connection conn = null;
        PreparedStatement checkIfExistsPs = null;
        PreparedStatement insertPs = null;
        PreparedStatement updateCheckInPs = null;
        PreparedStatement updateCheckOutPs = null;

        try {
            conn = new DBContext().getConnection();
            conn.setAutoCommit(false);
            checkIfExistsPs = conn.prepareStatement(checkIfExistsQuery);
            insertPs = conn.prepareStatement(insertQuery);
            updateCheckInPs = conn.prepareStatement(updateCheckInQuery);
            updateCheckOutPs = conn.prepareStatement(updateCheckOutQuery);

            for (InternAttendance ia : internAttendance) {
                // Check if record exists
                checkIfExistsPs.setString(1, ia.getInternID());
                boolean exists = false;
                try (ResultSet rs = checkIfExistsPs.executeQuery()) {
                    if (rs.next()) {
                        exists = (rs.getInt(1) > 0);
                    }
                }

                if (!exists) {
                    insertPs.setTimestamp(1, ia.getStatus().equals("Check In") ? new Timestamp(new Date().getTime()) : null);
                    insertPs.setTimestamp(2, ia.getStatus().equals("Check Out") ? new Timestamp(new Date().getTime()) : null);
                    insertPs.setString(3, ia.getStatus().equals("Check In") ? "CheckedIn" : "CheckedOut");
                    insertPs.setString(4, ia.getInternID());
                    insertPs.executeUpdate();
                } else {
                    if (ia.getStatus().equals("Check In")) {
                        // Update CheckIn if record exists and action is Check In
                        updateCheckInPs.setTimestamp(1, new Timestamp(new Date().getTime()));
                        updateCheckInPs.setString(2, "CheckedIn");
                        updateCheckInPs.setString(3, ia.getInternID());
                        updateCheckInPs.executeUpdate();
                    } else if (ia.getStatus().equals("Check Out")) {
                        // Update CheckOut if record exists and action is Check Out
                        updateCheckOutPs.setTimestamp(1, new Timestamp(new Date().getTime()));
                        updateCheckOutPs.setString(2, "CheckedOut");
                        updateCheckOutPs.setString(3, ia.getInternID());
                        updateCheckOutPs.executeUpdate();
                    }
                }
            }

            conn.commit(); // Commit transaction
            return true;
        } catch (SQLException ex) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException rollbackEx) {
                rollbackEx.printStackTrace();
            }
            ex.printStackTrace();
            return false;
        } finally {
            try {
                if (checkIfExistsPs != null) {
                    checkIfExistsPs.close();
                }
                if (insertPs != null) {
                    insertPs.close();
                }
                if (updateCheckInPs != null) {
                    updateCheckInPs.close();
                }
                if (updateCheckOutPs != null) {
                    updateCheckOutPs.close();
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                }
            } catch (SQLException closeEx) {
                closeEx.printStackTrace();
            }
        }
    }

    protected Connection connection;

    public List<Student> getStudentsByGroupId(int groupId) {
        List<Student> students = new ArrayList<>();
        String query = "SELECT I.Intern_ID, I.StudentID, st.FullName, st.Email, A.[CheckInTime], A.[CheckOutTime], \n"
                + "CASE WHEN A.CheckingStatus IS NULL THEN 'CheckedIn' ELSE A.CheckingStatus END AS CheckingStatus \n"
                + "FROM Student st \n"
                + "JOIN Intern I ON st.StudentID = I.StudentID \n"
                + "LEFT JOIN AttendanceTime A ON I.Intern_ID = A.Intern_ID AND CAST(A.CheckInTime AS DATE) = CAST(GETDATE() AS DATE) \n"
                + "WHERE I.Group_ID = ?";

        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, groupId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Student student = new Student();

                student.setInternID(rs.getString("Intern_ID"));
                student.setRoll_number(rs.getString("StudentID"));
                student.setFullname(rs.getString("FullName"));
                student.setEmail(rs.getString("Email"));

                Timestamp sqlCheckin = rs.getTimestamp("CheckInTime");
                Timestamp sqlCheckout = rs.getTimestamp("CheckOutTime");

                LocalDateTime checkin = sqlCheckin != null ? sqlCheckin.toLocalDateTime() : null;
                LocalDateTime checkout = sqlCheckout != null ? sqlCheckout.toLocalDateTime() : null;

                if (checkin == null && checkout != null) {
                    student.setCheckInTime(checkout);
                } else if (checkin != null && checkout != null) {
                    student.setCheckInTime(checkin);
                    student.setCheckOutTime(checkout);
                } else {
                    student.setCheckInTime(checkin);
                }

                student.setCheckingStatus(rs.getString("CheckingStatus"));
                students.add(student);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return students;
    }

    public Group getGroupByAccountID(int userID) {
        Group gr = new Group();
        String query
                = "select [Group].Group_ID, [Group].Group_Name, [Group].Mentor_ID \n"
                + "from Account join Intern on Account.User_ID = Intern.User_ID\n"
                + "join [Group] on Intern.Group_ID = [Group].Group_ID\n"
                + "Where Account.User_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Group group = new Group();

                group.setGroup_id(rs.getInt("Group_ID"));
                group.setGroup_name(rs.getString("Group_Name"));
                group.setMentor_id(rs.getInt("Group_Name"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gr;
    }

    public Group getGroupMentorByAccountID(int userID) {
        Group gr = new Group();
        String query
                = "select [Group].Group_ID, [Group].Group_Name, [Group].Mentor_ID \n"
                + "from Account join Mentor on Account.User_ID = Mentor.UserID\n"
                + "join [Group] on Mentor.Mentor_ID = [Group].Mentor_ID \n"
                + "where Account.User_ID = ?";
        try {
            connection = this.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Group group = new Group();

                group.setGroup_id(rs.getInt("Group_ID"));
                group.setGroup_name(rs.getString("Group_Name"));
                group.setMentor_id(rs.getInt("Mentor_ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gr;
    }

    public List<Student> getAttendanceForDate(Date date) {
        List<Student> students = new ArrayList<>();
        String query = "SELECT I.Intern_ID, I.StudentID, st.FullName, st.Email, A.[CheckInTime], A.[CheckOutTime], \n"
                + "CASE WHEN A.CheckingStatus IS NULL THEN 'CheckedIn' ELSE A.CheckingStatus END AS CheckingStatus \n"
                + "FROM Student st \n"
                + "JOIN Intern I ON st.StudentID = I.StudentID \n"
                + "LEFT JOIN AttendanceTime A ON I.Intern_ID = A.Intern_ID AND CAST(A.CheckInTime AS DATE) = ? \n"
                + "WHERE I.Group_ID = (SELECT Group_ID FROM Intern WHERE Intern_ID = A.Intern_ID)";

        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(query)) {

            // Format the date to match the SQL query
            String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            ps.setString(1, formattedDate);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Student student = new Student();

                    student.setInternID(rs.getString("Intern_ID"));
                    student.setRoll_number(rs.getString("StudentID"));
                    student.setFullname(rs.getString("FullName"));
                    student.setEmail(rs.getString("Email"));

                    Timestamp sqlCheckin = rs.getTimestamp("CheckInTime");
                    Timestamp sqlCheckout = rs.getTimestamp("CheckOutTime");

                    LocalDateTime checkin = sqlCheckin != null ? sqlCheckin.toLocalDateTime() : null;
                    LocalDateTime checkout = sqlCheckout != null ? sqlCheckout.toLocalDateTime() : null;

                    if (checkin == null && checkout != null) {
                        student.setCheckInTime(checkout);
                    } else if (checkin != null && checkout != null) {
                        student.setCheckInTime(checkin);
                        student.setCheckOutTime(checkout);
                    } else {
                        student.setCheckInTime(checkin);
                    }

                    student.setCheckingStatus(rs.getString("CheckingStatus"));
                    students.add(student);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return students;
    }

}
