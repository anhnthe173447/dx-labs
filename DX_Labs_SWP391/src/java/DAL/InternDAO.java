package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Group;

import models.Interns;
import models.Mentor;
import models.Student;

/**
 *
 * @author 84989
 */
public class InternDAO extends DBContext {

    public void insertStudent(String rollNumber, String fullName, String phoneNumber, String email,
            String major, String company, String jobTitle, String linkCV, int statusCV) {
        connection = this.getConnection();

        String sql = "INSERT INTO Student ([StudentID], FullName, Phone_number, Email, Major, Company, Job_tile, Link_CV, [CV_Status]) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, rollNumber);
            st.setString(2, fullName);
            st.setString(3, phoneNumber);
            st.setString(4, email);
            st.setString(5, major);
            st.setString(6, company);
            st.setString(7, jobTitle);
            st.setString(8, linkCV);
            st.setInt(9, statusCV);
            st.executeUpdate();
            insertIntern(rollNumber);
        } catch (SQLException e) {

            System.out.println(e);

        }
    }

    public boolean doesEmailtExist(String email) {
        connection = this.getConnection();
        String sql = "Select 1 from Student where email=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean doesPhoneNumbertExist(String phoneNumber) {
        connection = this.getConnection();
        String sql = "Select 1 from Student where phone_number=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, phoneNumber);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public void insertIntern(String rollNumber) throws SQLException {
        connection = this.getConnection();
        String sql = "INSERT INTO Intern (Intern_ID, User_ID, StudentID, Semester_ID) "
                + "SELECT COALESCE(MAX(CAST(Intern_ID AS INT)), 0) + 1, ?, ?, 2 FROM Intern";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, 0); 
            st.setString(2, rollNumber);

            st.executeUpdate();
        }
    }

    public boolean doesStudentExist(String studentId) {
        connection = this.getConnection();
        String sql = "Select 1 from Student where [StudentID]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, studentId);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean doesInternExist(String studentId) {
        connection = this.getConnection();
        String sql = "Select 1 from Intern where StudentID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, studentId);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public List<Student> getAllStudent() {
        connection = this.getConnection();
        List<Student> students = new ArrayList<>();
        String sql = "select * from Student";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setRoll_number(rs.getString(1));
                student.setFullname(rs.getString(2));
                student.setAddress(rs.getString(3));
                student.setPhone_number(rs.getString(4));
                student.setEmail(rs.getString(5));
                student.setId_card(rs.getString(6));
                student.setDob(rs.getDate(7));
                student.setMajor(rs.getString(8));
                student.setCompany(rs.getString(9));
                student.setJob_tile(rs.getString(10));
                student.setLink_cv(rs.getString(11));
                student.setStatus_cv(rs.getInt(12));
                students.add(student);
            }

        } catch (SQLException e) {
        }
        return students;
    }

    public List<Interns> getAllInterns() {
        Connection connection = this.getConnection();
        List<Interns> interns = new ArrayList<>();
        String sql = "SELECT * FROM Intern";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Interns intern = new Interns();
                intern.setIdIntern(rs.getString("Intern_ID"));
                intern.setUserID(rs.getInt("User_ID"));
                intern.setIdStudent(rs.getString("StudentID"));
                intern.setGroupID(rs.getInt("Group_ID"));
                intern.setSemesterID(rs.getInt("Semester_ID"));
                intern.setDivision(rs.getString("Division"));
                intern.setIsLeader(rs.getInt("IsLeader"));
                intern.setStatus(rs.getBoolean("InternStatus"));
                interns.add(intern);
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving interns: " + e.getMessage());
        }
        return interns;
    }

    public void updateCVStatus(String id, String newStatus) {
        connection = this.getConnection();
        String query = "UPDATE [dbo].[Student]\n"
                + "   SET \n"
                + "      [CV_Status] = ?\n"
                + " WHERE [StudentID]=?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, newStatus);
            st.setString(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public List<Group> getAllGroup() {
        connection = this.getConnection();
        List<Group> groups = new ArrayList<>();
        String sql = "SELECT * FROM [group]";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Group group = new Group();
                group.setGroup_id(rs.getInt("group_id"));
                group.setGroup_name(rs.getString("group_name"));
                group.setMentor_id(rs.getInt("mentor_id"));
                groups.add(group);
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving groups: " + e.getMessage());
        }
        return groups;
    }

    public void insert(Group g) {
        connection = this.getConnection();
        String sql = "INSERT INTO [Group] (group_id, group_name, mentor_id) VALUES (?, ?, ?)";

        try (PreparedStatement st = connection.prepareStatement(sql)) {

            st.setInt(1, g.getGroup_id());
            st.setString(2, g.getGroup_name());
            st.setInt(3, g.getMentor_id());

            st.executeUpdate();
        } catch (SQLException e) {

            System.out.println("Error inserting group: " + e.getMessage());
        }
    }

    public Group getGroupById(int group_id) {
        connection = this.getConnection();
        String sql = "SELECT * FROM [group] WHERE group_id=?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, group_id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Group group = new Group();
                group.setGroup_id(rs.getInt("group_id"));
                group.setGroup_name(rs.getString("group_name"));
                group.setMentor_id(rs.getInt("mentor_id"));

                return group;
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving group by ID: " + e.getMessage());
        }
        return null;
    }

    public Group getGroupByName(String group_name) {
        connection = this.getConnection();
        String sql = "SELECT * FROM [group] WHERE group_name=?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, group_name);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Group group = new Group();
                group.setGroup_id(rs.getInt("group_id"));
                group.setGroup_name(rs.getString("group_name"));
                group.setMentor_id(rs.getInt("mentor_id"));

                return group;
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving group by name: " + e.getMessage());
        }
        return null;
    }

    public void updateGroup(Group g) {
        connection = this.getConnection();
        String sql = "UPDATE [Group] SET group_name = ?, mentor_id = ? WHERE group_id = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, g.getGroup_name());
            st.setInt(2, g.getMentor_id());
            st.setInt(3, g.getGroup_id());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error updating group: " + e.getMessage());
        }
    }

    public void deleteGroup(int group_id) {
        connection = this.getConnection();
        String sql = "DELETE FROM [Group] WHERE group_id = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, group_id);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error deleting group: " + e.getMessage());
        }
    }

    public List<Interns> getInternByIdGroup(int groupId) {
        List<Interns> interns = new ArrayList<>();
        connection = this.getConnection();
        String sql = "SELECT * FROM intern WHERE group_id = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, groupId);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Interns intern = new Interns(rs.getString("intern_id"),
                        rs.getInt("User_ID"),
                        rs.getString("StudentID"),
                        rs.getInt("Group_ID"),
                        rs.getInt("Semester_ID"),
                        rs.getString("Division"),
                        rs.getInt("IsLeader"),
                        rs.getBoolean("InternStatus")
                );

                interns.add(intern);
            }
        } catch (SQLException e) {
        }
        return interns;
    }

    public Interns getInternById(String id) {
        connection = this.getConnection();
        String sql = "SELECT * FROM intern WHERE intern_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Interns intern = new Interns(
                        rs.getString("intern_id"),
                        rs.getInt("user_ID"),
                        rs.getString("studentId"),
                        rs.getInt("group_ID"),
                        rs.getInt("semester_ID"),
                        rs.getString("division"),
                        rs.getInt("isLeader"),
                        rs.getBoolean("Internstatus"));
                return intern;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Interns> getInternNoGroup() {
        Connection connection = this.getConnection();
        List<Interns> interns = new ArrayList<>();
        String sql = "SELECT * FROM Intern where group_id is null";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Interns intern = new Interns();
                intern.setIdIntern(rs.getString("Intern_ID"));
                intern.setUserID(rs.getInt("User_ID"));
                intern.setIdStudent(rs.getString("StudentID"));
                intern.setGroupID(rs.getInt("Group_ID"));
                intern.setSemesterID(rs.getInt("Semester_ID"));
                intern.setDivision(rs.getString("Division"));
                intern.setIsLeader(rs.getInt("IsLeader"));
                intern.setStatus(rs.getBoolean("InternStatus"));
                interns.add(intern);
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving interns: " + e.getMessage());
        }
        return interns;
    }

    public void addInternToGroup(String intern_id, int group_id) {
        connection = this.getConnection();
        String sql = "UPDATE intern SET group_id = ? WHERE intern_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, group_id);
            st.setString(2, intern_id);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteInternToGroup(String intern_id) {
        connection = this.getConnection();
        String sql = "UPDATE intern SET group_id = null WHERE intern_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, intern_id);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public boolean isGroupIdExists(int groupId) {
        connection = this.getConnection();
        String query = "SELECT * FROM [group] WHERE group_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, groupId);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean isGroupIdAndMentorIdExists(int groupId, int mentorId) {
        connection = this.getConnection();
        String query = "SELECT * FROM [group] WHERE group_id = ? AND mentor_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, groupId);
            ps.setInt(2, mentorId);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean isGroupIdExistsForOtherMentors(int groupId, int mentorId) {
        connection = this.getConnection();
        String query = "SELECT * FROM [group] WHERE group_id = ? AND mentor_id != ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, groupId);
            ps.setInt(2, mentorId);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean isGroupNameExistsForMentor(String groupName, int mentorId) {
        connection = this.getConnection();
        String query = "SELECT * FROM [group] WHERE group_name = ? AND mentor_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, groupName);
            ps.setInt(2, mentorId);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public List<Group> getGroupsByMentorId(int mentorId) {
        List<Group> groups = new ArrayList<>();
        connection = this.getConnection();
        String query = "SELECT * FROM [group] WHERE mentor_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, mentorId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Group group = new Group(rs.getInt("group_id"), rs.getString("group_name"), rs.getInt("mentor_id"));
                    groups.add(group);
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return groups;
    }

    public Mentor getMentorByUserId(String id) {
        Mentor mentor = null;
        connection = this.getConnection();
        String query = "SELECT * FROM mentor WHERE userId =?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                mentor = new Mentor(
                        rs.getInt("mentor_Id"),
                        rs.getString("userId"),
                        rs.getString("full_Name"),
                        rs.getString("email"),
                        rs.getString("phone_number"),
                        rs.getString("gender"),
                        rs.getString("dob"),
                        rs.getString("id_Card"),
                        rs.getString("address")
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return mentor;
    }

    public Group getGroupBySearchId(int groupId) {

        String query = "SELECT * FROM [group] WHERE group_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, groupId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Group group = new Group(rs.getInt("group_id"), rs.getString("group_name"), rs.getInt("mentor_id"));
                    return group;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<Group> getGroupBySearchName(String groupName, int mentorId) {
        List<Group> groups = new ArrayList<>();
        String query = "SELECT * FROM [group] WHERE group_name LIKE ? AND mentor_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, "%" + groupName + "%");
            ps.setInt(2, mentorId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Group group = new Group(rs.getInt("group_id"), rs.getString("group_name"), rs.getInt("mentor_id"));
                    groups.add(group);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groups;
    }

    public List<Interns> searchInternsNoGroupByStudentID(String studentId) {
        connection = this.getConnection();
        List<Interns> interns = new ArrayList<>();
        String sql = "SELECT * FROM Intern WHERE LOWER(StudentID) LIKE LOWER(?) AND Group_ID IS NULL";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, "%" + studentId.toLowerCase() + "%");
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Interns intern = new Interns();
                    intern.setIdIntern(rs.getString("Intern_ID"));
                    intern.setUserID(rs.getInt("User_ID"));
                    intern.setIdStudent(rs.getString("StudentID"));
                    intern.setGroupID(rs.getInt("Group_ID"));
                    intern.setSemesterID(rs.getInt("Semester_ID"));
                    intern.setDivision(rs.getString("Division"));
                    intern.setIsLeader(rs.getInt("IsLeader"));
                    intern.setStatus(rs.getBoolean("InternStatus"));
                    interns.add(intern);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error retrieving interns: " + e.getMessage());
        }
        return interns;
    }

    public boolean hasLeader(int groupId) {
        connection = this.getConnection();
        String sql = "select count(*) from intern where group_id = ? and isLeader = 1";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, groupId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean setLeader(int internId) {
        connection = this.getConnection();
        String sql = "update intern set isLeader = 1 where intern_id =?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, internId);
            int rowsUpdate = st.executeUpdate();
            return rowsUpdate > 0;
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public boolean removeLeader(int internId) {
        connection = this.getConnection();
        String sql = "update intern set isLeader = 0 where intern_id =?";
        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, internId);
            int rowsUpdate = st.executeUpdate();
            return rowsUpdate > 0;
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    public static void main(String[] args) {
        InternDAO idao = new InternDAO();
        List<Interns> i = idao.getInternByIdGroup(1);
        for (Interns interns : i) {
            System.out.println("Id: " + interns.getIdStudent());
        }
    }

    public Mentor getMentorByMentorID(int id) {
        Mentor mentor = null;
        connection = this.getConnection();
        String query = "SELECT * FROM mentor WHERE mentor_id =?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                mentor = new Mentor(
                        rs.getInt("mentor_Id"),
                        rs.getString("userId"),
                        rs.getString("full_Name"),
                        rs.getString("email"),
                        rs.getString("phone_number"),
                        rs.getString("gender"),
                        rs.getString("dob"),
                        rs.getString("id_Card"),
                        rs.getString("address")
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return mentor;
    }

}
