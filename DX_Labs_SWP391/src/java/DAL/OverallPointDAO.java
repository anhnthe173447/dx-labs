/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.OverallPoint;

/**
 *
 * @author admin
 */
public class OverallPointDAO extends DBContext {

    protected Connection connection;

    //get list
    public List<OverallPoint> getListOverallPoint(int userID) {

        Connection connection = null;
        List<OverallPoint> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = this.getConnection();
            String sql
                    = " SELECT OverallEvaluation.OverallEval_ID,\n"
                    + " OverallEvaluation.MajorSkillPoint, OverallEvaluation.SoftSkillPoint, OverallEvaluation.AttitudePoint, OverallEvaluation.FinalResult,\n"
                    + " OverallEvaluation.Intern_ID, \n"
                    + " Student.FullName\n"
                    + " FROM OverallEvaluation \n"
                    + " JOIN Intern ON OverallEvaluation.Intern_ID = Intern.Intern_ID\n"
                    + " JOIN Student ON Intern.StudentID = Student.StudentID\n"
                    + "					join  [Group] on [Group].Group_ID = Intern.Group_ID\n"
                    + "					where [Group].Mentor_ID =(select Mentor_ID\n"
                    + "					from Mentor join Account on Mentor.UserID = Account.User_ID\n"
                    + "					where Account.User_ID = ?)";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            rs = ps.executeQuery();
            while (rs.next()) {
                OverallPoint finalEvaluationList = new OverallPoint(
                        rs.getInt("OverallEval_ID"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getFloat("FinalResult"),
                        rs.getInt("Intern_ID"),
                        rs.getString("FullName")
                );

                list.add(finalEvaluationList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    //get list end
    //detail
    public OverallPoint getOverallPointByInternID(int internID) {

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        OverallPoint overallP = null;

        try {
            connection = this.getConnection();
            String sql
                    = "SELECT OverallEvaluation.OverallEval_ID, OverallEvaluation.Midterm_ID, OverallEvaluation.FinalEval_ID,\n"
                    + "Student.FullName, \n"
                    + "OverallEvaluation.MajorSkillPoint, OverallEvaluation.SoftSkillPoint, OverallEvaluation.AttitudePoint,"
                    + " OverallEvaluation.FinalResult,\n"
                    + " OverallEvaluation.Semester_ID, "
                    + " OverallEvaluation.Intern_ID, "
                    + " OverallEvaluation.isDelete,\n"
                    + "Semester.SemesterName\n"
                    + "FROM OverallEvaluation \n"
                    + "JOIN Intern on OverallEvaluation.Intern_ID = Intern.Intern_ID\n"
                    + "JOIN Student on Intern.StudentID = Student.StudentID\n"
                    + "JOIN Semester ON OverallEvaluation.Semester_ID = Semester.Semester_ID\n"
                    + "WHERE OverallEvaluation.isDelete = 0 and OverallEvaluation.Intern_ID = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, internID);
            rs = ps.executeQuery();

            if (rs.next()) {
                overallP = new OverallPoint(
                        rs.getInt("OverallEval_ID"),
                        rs.getFloat("MajorSkillPoint"),
                        rs.getFloat("SoftSkillPoint"),
                        rs.getFloat("AttitudePoint"),
                        rs.getFloat("FinalResult"),
                        rs.getInt("Intern_ID"),
                        rs.getString("FullName")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return overallP;
    }
    //detail end

//    public boolean checkInternExistsInStages(String internID) {
//        String sql = "SELECT COUNT(*) \n"
//                + "FROM Beginning_stage \n"
//                + "JOIN Ending_stage  ON Beginning_stage.Id_intern = Ending_stage.Id_intern \n"
//                + "WHERE Beginning_stage.Id_intern = ?";
//        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
//            ps.setString(1, internID);
//            try (ResultSet rs = ps.executeQuery()) {
//                if (rs.next()) {
//                    return rs.getInt(1) > 0;
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    public int getCurrentSemesterID() {
//        String sql = "SELECT Semester_id FROM Semester WHERE GETDATE() BETWEEN Start_date AND End_date";
//        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
//            if (rs.next()) {
//                return rs.getInt("Semester_id");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
//    public boolean insertOverallPoint(String idIntern, int mentorId, String comment, float finalResult) {
//        // Debugging: in ra idIntern
//        System.out.println("insertOverallPoint - Intern ID: " + idIntern);
//
//        if (!checkInternExistsInStages(idIntern)) {
//            return false; // Intern không tồn tại trong cả BeginningStage và EndingStage
//        }
//
//        int idSemester = getCurrentSemesterID();
//        if (idSemester == 0) {
//            return false; // Không tìm thấy semester hiện tại
//        }
//
//        String sql = "INSERT INTO InternshipEvaluations \n"
//                + "	(Id_intern, mentor_Id, comment, ID_beginning_stage, ID_Ending_stage, Final_Result, ID_Semester) \n"
//                + "   SELECT Beginning_stage.Id_intern, ?, ?,Beginning_stage.ID_beginning_stage, Ending_stage.ID_ending_stage, ?, ?\n"
//                + "   FROM Beginning_stage JOIN Ending_stage ON Beginning_stage.Id_intern = Ending_stage.Id_intern \n"
//                + "   WHERE Beginning_stage.Id_intern = ?";
//        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
//            ps.setInt(1, mentorId);
//            ps.setString(2, comment);
//            ps.setFloat(3, finalResult);
//            ps.setInt(4, idSemester);
//            ps.setString(5, idIntern);
//
//            // Debugging: in ra câu lệnh SQL
//            System.out.println("Executing SQL: " + ps.toString());
//
//            return ps.executeUpdate() > 0;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
    public boolean updateOverallPoint(OverallPoint overallP) {
        String sql = "UPDATE OverallEvaluation "
                + "SET "
                + "MajorSkillPoint = ?, "
                + "SoftSkillPoint = ?, "
                + "AttitudePoint = ?, "
                + "FinalResult = ? "
                + "WHERE Intern_ID = ?";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setFloat(1, overallP.getMajorSkillPoint());
            ps.setFloat(2, overallP.getSoftSkillPoint());
            ps.setFloat(3, overallP.getAttitudePoint());
            ps.setFloat(4, overallP.getFinalResult());
            ps.setInt(5, overallP.getInternID());

            int rowsAffected = ps.executeUpdate();
            System.out.println("Số lượng bản ghi bị ảnh hưởng: " + rowsAffected);
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void save(OverallPoint overallPoint, String operation) {
        if ("insert".equalsIgnoreCase(operation)) {
            insertOverallPoint(overallPoint);
        } else if ("update".equalsIgnoreCase(operation)) {
            updateOverallPoint(overallPoint);
        } else {
            throw new IllegalArgumentException("Unsupported operation: " + operation);
        }
    }

    private void insertOverallPoint(OverallPoint overallPoint) {
        String sql = "INSERT INTO OverallEvaluation "
                + "( MajorSkillPoint, SoftSkillPoint, AttitudePoint, FinalResult,"
                + "  Intern_ID, isDelete) "
                + "VALUES (?, ?, ?, ?, ?, 0)";
        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setFloat(1, overallPoint.getMajorSkillPoint());
            ps.setFloat(2, overallPoint.getSoftSkillPoint());
            ps.setFloat(3, overallPoint.getAttitudePoint());
            ps.setFloat(4, overallPoint.getFinalResult());
            ps.setInt(5, overallPoint.getInternID());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    private void updateOverallPoint1(OverallPoint overallPoint) {
//        String sql = "UPDATE OverallEvaluation "
//                + "SET  MajorSkillPoint = ?, SoftSkillPoint = ?, "
//                + "AttitudePoint = ?, FinalResult = ?, Semester_ID = ?, isDelete = ? "
//                + "WHERE Intern_ID = ?";
//        try (Connection connection = this.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
//
//            ps.setFloat(1, overallPoint.getMajorSkillPoint());
//            ps.setFloat(2, overallPoint.getSoftSkillPoint());
//            ps.setFloat(3, overallPoint.getAttitudePoint());
//            ps.setFloat(4, overallPoint.getFinalResult());
//            ps.setInt(5, overallPoint.getSemesterID());
//            ps.setBoolean(6, overallPoint.isIsDelete());
//            ps.setInt(7, overallPoint.getInternID());
//
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
    public OverallPoint getByInternId(int internId) {
        String sql = "select * from OverallEvaluation where Intern_ID = ?";
        try {
            connection = this.getConnection();

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, internId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                OverallPoint o = new OverallPoint();
                o.setOverallPointID(rs.getInt("OverallEval_ID"));
                o.setMajorSkillPoint(!(rs.getFloat("MajorSkillPoint") + "").isEmpty() ? rs.getFloat("MajorSkillPoint") : 0.0f);
                o.setAttitudePoint(!(rs.getFloat("AttitudePoint") + "").isEmpty() ? rs.getFloat("AttitudePoint") : 0.0f);
                o.setSoftSkillPoint(!(rs.getFloat("SoftSkillPoint") + "").isEmpty() ? rs.getFloat("SoftSkillPoint") : 0.0f);
                o.setFinalResult(!(rs.getFloat("FinalResult") + "").isEmpty() ? rs.getFloat("FinalResult") : 0.0f);
                o.setInternID(rs.getInt("Intern_ID"));
                return o;
            }
        } catch (Exception e) {
        }
        return null;
    }

}
