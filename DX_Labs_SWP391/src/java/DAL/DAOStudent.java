/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import models.Students;

/**
 *
 * @author ADMIN
 */
public class DAOStudent {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public Students getStudentByID(String id) {
        String query = "select * from Student\n"
                + "where [StudentID]=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {

                return new Students(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12)
                );
            }

        } catch (Exception e) {
        }
        return null;

    }

    //intern2
    public void editStudent(String id, String fullname, String address, String phone, String email, String IDcard, String DOB,String major,String Company,String jobtile) {

        String query = "UPDATE [dbo].[Student]\n"
                + "   SET \n"
                + "       [FullName] = ?\n"
                + "      ,[Address] = ?\n"
                + "      ,[Phone_number] = ?\n"
                + "      ,[Email] = ?\n"
                + "      ,[ID_Card] = ?\n"
                + "      ,[DOB] = ?\n"
                + "      ,[Major] = ?\n"
                + "      ,[Company] = ?\n"
                + "      ,[Job_tile] = ?\n"
                + " WHERE [StudentID]=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, fullname);
            ps.setString(2, address);
            ps.setString(3, phone);
            ps.setString(4, email);
            ps.setString(5, IDcard);
            ps.setString(6, DOB);
            ps.setString(7, major);
            ps.setString(8, Company);
            ps.setString(9, jobtile);
            ps.setString(10, id);          
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        DAOStudent dao = new DAOStudent();
        Students st = dao.getStudentByID("S001");
        System.out.println(st.getFullName());
    }
}
