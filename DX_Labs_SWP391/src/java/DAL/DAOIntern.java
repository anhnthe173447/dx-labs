/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import models.Accounts;
import models.Group;
import models.Intern_Student;
import models.Interns;
import models.Mentor;
import models.MidtermPoint;
import models.OverallPoint;
import models.Students;

/**
 *
 * @author ADMIN
 */
public class DAOIntern extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Interns> getAllIntern() {

        String query = "SELECT * \n"
                + "FROM Intern\n"
                + "ORDER BY CAST(Intern_ID AS INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;

    }

    public void updateInternStatus(String id, String newStatus) {
        String query = "UPDATE [dbo].[Intern]\n"
                + "   SET \n"
                + "      [InternStatus] = ?\n"
                + " WHERE Intern_ID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, newStatus);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Interns getInternByID(String id) {
        String query = "select * from Intern\n"
                + "where Intern_ID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)
                );
            }

        } catch (Exception e) {
        }
        return null;

    }

    public static boolean isDateOver18(String dateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);

        try {
            Date birthDate = new Date(dateFormat.parse(dateStr).getTime());
            Calendar currentDateCal = Calendar.getInstance();
            Calendar minAgeCal = (Calendar) currentDateCal.clone();
            minAgeCal.add(Calendar.YEAR, -18);
            return birthDate.before(new Date(minAgeCal.getTimeInMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void editIntern(String id, String fullname, String address, String phone, String email, String IDcard, String DOB) {
        Interns intern = getInternByID(id);
        String idStudent = intern.getIdStudent();
        String query = "UPDATE [dbo].[Student]\n"
                + "   SET \n"
                + "      [FullName] = ?\n"
                + "      ,[Address] = ?\n"
                + "      ,[Phone_number] = ?\n"
                + "      ,[Email] = ?\n"
                + "      ,[ID_Card] = ?\n"
                + "      ,[DOB] = ?\n"
                + " WHERE StudentID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, fullname);
            ps.setString(2, address);
            ps.setString(3, phone);
            ps.setString(4, email);
            ps.setString(5, IDcard);
            ps.setString(6, DOB);
            ps.setString(7, idStudent);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void DeleteIntern(String id) {
        String query = "DELETE FROM [dbo].[Intern]\n"
                + "      WHERE Intern_ID=?";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //Intern 2
    public List<Interns> SearchInternByName(String name) {
        String query = "Select Intern_ID,User_ID,i.StudentID,Group_ID,Semester_id,Division,IsLeader,InternStatus from Intern i join Student s\n"
                + "on i.StudentID= s.StudentID\n"
                + "WHERE s.FullName LIKE ?\n"
                + "Order by Cast(Intern_ID As INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + name + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Interns> SearchInternByNameByMentor(String name, int mentor) {
        String query = "SELECT \n"
                + "    Intern_ID,\n"
                + "    User_ID,\n"
                + "    i.StudentID,\n"
                + "    g.Group_ID,\n"
                + "    Semester_id,\n"
                + "    Division,\n"
                + "    IsLeader,\n"
                + "    InternStatus\n"
                + "FROM \n"
                + "    Intern i\n"
                + "JOIN Student s ON i.StudentID = s.StudentID\n"
                + "Join\n"
                + "	[Group] g on i.Group_ID = g.Group_ID\n"
                + "join Mentor m on m.Mentor_ID=g.Mentor_ID\n"
                + "WHERE \n"
                + "    s.FullName LIKE ? And m.Mentor_ID = ?\n"
                + "ORDER BY \n"
                + "    CAST(Intern_ID AS INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + name + "%");
            ps.setInt(2, mentor);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Interns> searchInternByInternId(String id) {
        String query = "Select Intern_ID,User_ID,i.StudentID,Group_ID,Semester_id,Division,IsLeader,InternStatus from Intern i join Student s\n"
                + "on i.StudentID= s.StudentID\n"
                + "WHERE Intern_ID LIKE ?\n"
                + "Order by Cast(Intern_ID As INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + id + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Interns> searchInternByInternIdByMentor(String id, int mentorID) {
        String query = "SELECT \n"
                + "    Intern_ID,\n"
                + "    User_ID,\n"
                + "    i.StudentID,\n"
                + "    g.Group_ID,\n"
                + "    Semester_id,\n"
                + "    Division,\n"
                + "    IsLeader,\n"
                + "    InternStatus\n"
                + "FROM \n"
                + "    Intern i\n"
                + "JOIN Student s ON i.StudentID = s.StudentID\n"
                + "Join\n"
                + "	[Group] g on i.Group_ID = g.Group_ID\n"
                + "join Mentor m on m.Mentor_ID=g.Mentor_ID\n"
                + "WHERE \n"
                + "    Intern_ID LIKE ? And m.Mentor_ID = ?\n"
                + "ORDER BY \n"
                + "    CAST(Intern_ID AS INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + id + "%");
            ps.setInt(2, mentorID);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Interns> SearchInternByStudentID(String ID) {
        String query = "Select Intern_ID,User_ID,i.StudentID,Group_ID,Semester_id,Division,IsLeader,InternStatus from Intern i join Student s\n"
                + "on i.StudentID= s.StudentID\n"
                + "WHERE s.StudentID LIKE ?\n"
                + "Order by Cast(Intern_ID As INT) ASC;";
        List<Interns> list = new ArrayList<>();
        DAOAccount daoA = new DAOAccount();
        DAOStudent daoS = new DAOStudent();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + ID + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Interns> SearchInternByStudentIDByMentor(String ID, int mentor) {
        String query = "SELECT \n"
                + "    Intern_ID,\n"
                + "    User_ID,\n"
                + "    i.StudentID,\n"
                + "    g.Group_ID,\n"
                + "    Semester_id,\n"
                + "    Division,\n"
                + "    IsLeader,\n"
                + "    InternStatus\n"
                + "FROM \n"
                + "    Intern i\n"
                + "JOIN Student s ON i.StudentID = s.StudentID\n"
                + "Join\n"
                + "	[Group] g on i.Group_ID = g.Group_ID\n"
                + "join Mentor m on m.Mentor_ID=g.Mentor_ID\n"
                + "WHERE \n"
                + "    s.StudentID LIKE ? And m.Mentor_ID = ?\n"
                + "ORDER BY \n"
                + "    CAST(Intern_ID AS INT) ASC;";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + ID + "%");
            ps.setInt(2, mentor);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Interns getInternByAccount(String id) {
        String query = "select * from Intern\n"
                + "where [User_ID]=?";
        DAOAccount daoA = new DAOAccount();
        DAOStudent daoS = new DAOStudent();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {

                return new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)
                );
            }

        } catch (Exception e) {
        }
        return null;

    }

    public void UpdateAccountIntern(String id) {
        String query = "UPDATE [dbo].[Intern]\n"
                + "   SET \n"
                + "      [User_ID] = (SELECT MAX([User_ID]) FROM [dbo].Account)\n"
                + "\n"
                + " WHERE Intern_ID=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Intern_Student> getInternsByGroupIds(List<Group> groups) {
        List<Intern_Student> interns = new ArrayList<>();
        if (groups == null || groups.isEmpty()) {
            return interns; // return empty list if no groups are provided
        }

        StringBuilder queryBuilder = new StringBuilder("SELECT i.Intern_ID, i.User_ID, i.Group_ID, i.Semester_ID, i.StudentID, i.Division, i.IsLeader, i.InternStatus, ")
                .append("s.FullName, s.Address, s.Phone_number, g.Group_Name, s.Email, s.ID_Card, s.Major, s.Job_tile, s.Company, s.Link_CV, s.DOB, s.CV_Status, ")
                .append("m.MajorSkillPoint as MajorSkillPoint_Midterm , m.SoftSkillPoint as SoftSkillPoint_Midterm, m.AttitudePoint as AttitudePoint_Midterm, m.Comment  as Comment_Midterm,"
                        + " e.MajorSkillPoint as MajorSkillPoint_Final , e.SoftSkillPoint as SoftSkillPoint_Final, e.AttitudePoint as AttitudePoint_Final, e.Comment  as Comment_Final  ")
                .append("FROM Intern i ")
                .append("JOIN [Group] g ON g.Group_ID = i.Group_ID ")
                .append("JOIN Student s ON i.StudentID = s.StudentID ")
                .append("LEFT JOIN MidtermEvaluation m ON m.InternID = i.Intern_ID ") // Left join to MidtermEvaluation
                .append("LEFT JOIN FinalEvaluation e ON e.InternID = i.Intern_ID ")
                .append("WHERE i.Group_ID IN (");

        for (int i = 0; i < groups.size(); i++) {
            queryBuilder.append("?");
            if (i < groups.size() - 1) {
                queryBuilder.append(",");
            }
        }
        queryBuilder.append(")");

        String query = queryBuilder.toString();

        try {
            conn = new DBContext().getConnection(); // open connection to SQL
            ps = conn.prepareStatement(query);

            for (int i = 0; i < groups.size(); i++) {
                ps.setInt(i + 1, groups.get(i).getGroup_id());
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                Intern_Student internStudent = new Intern_Student();
                internStudent.setIntern_ID(rs.getInt("Intern_ID"));
                internStudent.setUser_ID(rs.getInt("User_ID"));
                internStudent.setGroup_ID(rs.getInt("Group_ID"));
                internStudent.setSemester_ID(rs.getInt("Semester_ID"));
                internStudent.setStudentID(rs.getString("StudentID"));
                internStudent.setDivision(rs.getString("Division"));
                internStudent.setIsLeader(rs.getBoolean("IsLeader"));
                internStudent.setInternStatus(rs.getBoolean("InternStatus"));
                internStudent.setFullName(rs.getString("FullName"));
                internStudent.setAddress(rs.getString("Address"));
                internStudent.setPhone_number(rs.getString("Phone_number"));
                internStudent.setEmail(rs.getString("Email"));
                internStudent.setGroup_Name(rs.getString("Group_Name"));
                internStudent.setID_Card(rs.getString("ID_Card"));
                internStudent.setMajor(rs.getString("Major"));
                internStudent.setJob_tile(rs.getString("Job_tile"));
                internStudent.setCompany(rs.getString("Company"));
                internStudent.setLink_CV(rs.getString("Link_CV"));
                internStudent.setDOB(rs.getDate("DOB"));
                internStudent.setCV_Status(rs.getBoolean("CV_Status"));

                // Set MidtermEvaluation fields
                float majorSkillPoint = rs.getFloat("MajorSkillPoint_Midterm");
                if (!rs.wasNull()) {
                    internStudent.setMajorSkillPoint_Midterm(majorSkillPoint);
                }

                float softSkillPoint = rs.getFloat("SoftSkillPoint_Midterm");
                if (!rs.wasNull()) {
                    internStudent.setSoftSkillPoint_Midterm(softSkillPoint);
                }

                float attitudePoint = rs.getFloat("AttitudePoint_Midterm");
                if (!rs.wasNull()) {
                    internStudent.setAttitudePoint_Midterm(attitudePoint);
                }

                String comment = rs.getString("Comment_Midterm");
                if (comment != null) {
                    internStudent.setComment_Midterm(comment);
                }

                // Set MidtermEvaluation fields
                float majorSkillPointf = rs.getFloat("MajorSkillPoint_Final");
                if (!rs.wasNull()) {
                    internStudent.setMajorSkillPoint_Final(majorSkillPointf);
                }

                float softSkillPointf = rs.getFloat("SoftSkillPoint_Final");
                if (!rs.wasNull()) {
                    internStudent.setSoftSkillPoint_Final(softSkillPointf);
                }

                float attitudePointf = rs.getFloat("AttitudePoint_Final");
                if (!rs.wasNull()) {
                    internStudent.setAttitudePoint_Final(attitudePointf);
                }

                String commentf = rs.getString("Comment_Final");
                if (commentf != null) {
                    internStudent.setComment_Final(commentf);
                }

                interns.add(internStudent);
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        return interns;
    }

    public List<Intern_Student> getOverallPointOfInternsByGroupIds(List<Group> groups) {
        List<Intern_Student> interns = new ArrayList<>();
        if (groups == null || groups.isEmpty()) {
            return interns; // return empty list if no groups are provided
        }

        StringBuilder queryBuilder = new StringBuilder("SELECT i.Intern_ID, i.User_ID, i.Group_ID, i.Semester_ID, i.StudentID, i.Division, i.IsLeader, i.InternStatus, ")
                .append("s.FullName, s.Address, s.Phone_number, g.Group_Name, s.Email, s.ID_Card, s.Major, s.Job_tile, s.Company, s.Link_CV, s.DOB, s.CV_Status, ")
                .append(" o.MajorSkillPoint as MajorSkillPoint_Overall , "
                        + "o.SoftSkillPoint as SoftSkillPoint_Overall, "
                        + "o.AttitudePoint as AttitudePoint_Overall, o.FinalResult  ")
                .append("FROM Intern i ")
                .append("JOIN [Group] g ON g.Group_ID = i.Group_ID ")
                .append("JOIN Student s ON i.StudentID = s.StudentID ")
                .append(" JOIN OverallEvaluation o ON o.Intern_ID = i.Intern_ID ") // Left join to MidtermEvaluation
                .append("WHERE i.Group_ID IN (");

        for (int i = 0; i < groups.size(); i++) {
            queryBuilder.append("?");
            if (i < groups.size() - 1) {
                queryBuilder.append(",");
            }
        }
        queryBuilder.append(")");

        String query = queryBuilder.toString();
        try {
            conn = new DBContext().getConnection(); // open connection to SQL
            ps = conn.prepareStatement(query);

            for (int i = 0; i < groups.size(); i++) {
                ps.setInt(i + 1, groups.get(i).getGroup_id());
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                Intern_Student internStudent = new Intern_Student();
                internStudent.setIntern_ID(rs.getInt("Intern_ID"));
                internStudent.setUser_ID(rs.getInt("User_ID"));
                internStudent.setGroup_ID(rs.getInt("Group_ID"));
                internStudent.setSemester_ID(rs.getInt("Semester_ID"));
                internStudent.setStudentID(rs.getString("StudentID"));
                internStudent.setDivision(rs.getString("Division"));
                internStudent.setIsLeader(rs.getBoolean("IsLeader"));
                internStudent.setInternStatus(rs.getBoolean("InternStatus"));
                internStudent.setFullName(rs.getString("FullName"));
                internStudent.setAddress(rs.getString("Address"));
                internStudent.setPhone_number(rs.getString("Phone_number"));
                internStudent.setEmail(rs.getString("Email"));
                internStudent.setGroup_Name(rs.getString("Group_Name"));
                internStudent.setID_Card(rs.getString("ID_Card"));
                internStudent.setMajor(rs.getString("Major"));
                internStudent.setJob_tile(rs.getString("Job_tile"));
                internStudent.setCompany(rs.getString("Company"));
                internStudent.setFinalResult_Overall(rs.getFloat("FinalResult"));
                internStudent.setLink_CV(rs.getString("Link_CV"));
                internStudent.setDOB(rs.getDate("DOB"));
                internStudent.setCV_Status(rs.getBoolean("CV_Status"));

                // Set MidtermEvaluation fields
                float majorSkillPoint = rs.getFloat("MajorSkillPoint_Overall");
                if (!rs.wasNull()) {
                    internStudent.setMajorSkillPoint_Overall(majorSkillPoint);
                }

                float softSkillPoint = rs.getFloat("SoftSkillPoint_Overall");
                if (!rs.wasNull()) {
                    internStudent.setSoftSkillPoint_Overall(softSkillPoint);
                }

                float attitudePoint = rs.getFloat("AttitudePoint_Overall");
                if (!rs.wasNull()) {
                    internStudent.setAttitudePoint_Overall(attitudePoint);
                }
                interns.add(internStudent);
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        return interns;
    }

    public List<Intern_Student> getInternsByGroupIdsAndCurrentSemester(List<Group> groups) {
        List<Intern_Student> interns = new ArrayList<>();
        if (groups == null || groups.isEmpty()) {
            return interns; // return empty list if no groups are provided
        }

        StringBuilder queryBuilder = new StringBuilder("SELECT * FROM Intern WHERE Group_ID IN (");

        for (int i = 0; i < groups.size(); i++) {
            queryBuilder.append("?");
            if (i < groups.size() - 1) {
                queryBuilder.append(",");
            }
        }

        queryBuilder.append(") AND Semester_ID = (SELECT Semester_ID FROM Semester WHERE CURRENT_TIMESTAMP BETWEEN StartDate AND EndDate)");

        String query = queryBuilder.toString();

        try {
            conn = new DBContext().getConnection(); // open connection to SQL
            ps = conn.prepareStatement(query);

            for (int i = 0; i < groups.size(); i++) {
                ps.setInt(i + 1, groups.get(i).getGroup_id());
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                Intern_Student intern = new Intern_Student();
                intern.setIntern_ID(rs.getInt("Intern_ID"));
                intern.setUser_ID(rs.getInt("User_ID"));
                intern.setGroup_ID(rs.getInt("Group_ID"));
                intern.setSemester_ID(rs.getInt("Semester_ID"));
                intern.setStudentID(rs.getString("StudentID"));
                intern.setDivision(rs.getString("Division"));
                intern.setIsLeader(rs.getBoolean("IsLeader"));
                intern.setInternStatus(rs.getBoolean("InternStatus"));
                interns.add(intern);
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        return interns;
    }

    public class GetInternByUserID extends DBContext {

        public Interns getInternByUserID(int userid) {
            connection = this.getConnection();
            Interns list = new Interns();
            try {
                String sql = "select i.Intern_ID,i.User_ID,i.StudentID,i.Group_ID,i.Semester_ID,i.InternStatus from Intern i join account a on i.[user_id] = a.[user_id] where i.user_id = ?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, userid);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    list = new Interns(rs.getString(1),
                            rs.getInt(2),
                            rs.getString(3),
                            rs.getInt(4),
                            rs.getInt(5),
                            rs.getBoolean(6));

                }
            } catch (Exception e) {
            }
            return list;
        }

    }

    public List<Interns> getInternByMentor(int idMentor) {
        String query = "select Intern.*\n"
                + "From Intern join [Group]\n"
                + "on Intern.Group_ID= [Group].Group_ID\n"
                + "where Mentor_ID=?";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, idMentor);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));

            }

        } catch (Exception e) {
        }
        return list;

    }

    public List<Interns> getInternBySemester(int idMentor, String semester) {
        String query = "select Intern.*\n"
                + "From Intern join [Group]\n"
                + "on Intern.Group_ID= [Group].Group_ID\n"
                + "where[Group].Mentor_ID =? and Intern.Semester_id =?";
        List<Interns> list = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, idMentor);
            ps.setString(2, semester);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Interns(rs.getString(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getBoolean(8)));

            }

        } catch (Exception e) {
        }
        return list;

    }

    public static void main(String[] args) {
        DAOIntern dao = new DAOIntern();
        List<Interns> intern = dao.getInternByMentor(1);
        Interns in = dao.getInternByAccount("2");
        dao.updateInternStatus("2", "1");
        System.out.println(in.getIdIntern());
        for (Interns list : intern) {
            System.out.println(list.getIdIntern());
        }

    }

}
