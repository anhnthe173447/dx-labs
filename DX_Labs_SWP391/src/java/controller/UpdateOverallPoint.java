/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.OverallPointDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.OverallPoint;

/**
 *
 * @author admin
 */
public class UpdateOverallPoint extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");

        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        
        String internID = request.getParameter("id");
        int idIntern = 0;

        if (internID != null && !internID.isEmpty()) {
            idIntern = Integer.parseInt(internID);
        }

        OverallPointDAO odao = new OverallPointDAO();
        OverallPoint overallPoint = odao.getOverallPointByInternID(idIntern);

        request.setAttribute("OverallPoint", overallPoint);
        request.getRequestDispatcher("UpdateOverallPoint.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");

        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String internIDStr = request.getParameter("idIntern");
        int idIntern = 0;
        if (internIDStr != null && !internIDStr.isEmpty()) {
            try {
                idIntern = Integer.parseInt(internIDStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        float majorSkillPoint = 0;
        String majorPointStr = request.getParameter("majorpoint");
        if (majorPointStr != null && !majorPointStr.isEmpty()) {
            try {
                majorSkillPoint = Float.parseFloat(majorPointStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        float softSkillPoint = 0;
        String softSkillPointStr = request.getParameter("softSkillPoint");
        if (softSkillPointStr != null && !softSkillPointStr.isEmpty()) {
            try {
                softSkillPoint = Float.parseFloat(softSkillPointStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        float attitudePoint = 0;
        String attitudePointStr = request.getParameter("attitudePoint");
        if (attitudePointStr != null && !attitudePointStr.isEmpty()) {
            try {
                attitudePoint = Float.parseFloat(attitudePointStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        float finalResult = 0;
        String finalResultStr = request.getParameter("finalResult");
        if (finalResultStr != null && !finalResultStr.isEmpty()) {
            try {
                finalResult = Float.parseFloat(finalResultStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        OverallPoint overallPoint = new OverallPoint();
        overallPoint.setInternID(idIntern);
        overallPoint.setMajorSkillPoint(majorSkillPoint);
        overallPoint.setSoftSkillPoint(softSkillPoint);
        overallPoint.setAttitudePoint(attitudePoint);
        overallPoint.setFinalResult(finalResult);

        OverallPointDAO odao = new OverallPointDAO();
        boolean updated = odao.updateOverallPoint(overallPoint);
        if (updated) {
            request.setAttribute("message", "Overall Point updated successfully!");
        } else {
            request.setAttribute("message", "No Overall Point found for intern ID: " + idIntern);
        }

        response.sendRedirect("DetailOverallPoint_Mentor?id=" + idIntern);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
