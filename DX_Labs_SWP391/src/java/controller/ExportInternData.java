package controller;

import DAL.ReportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import models.ReportOverallPoint;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ADMIN
 */
public class ExportInternData extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExportInternData</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExportInternData at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ReportDAO daoOV = new ReportDAO();
        List<ReportOverallPoint> listOV = daoOV.getListOverallEvaluationP();
        boolean isExport = exportToExcel(listOV, response);

        if (!isExport) {
            response.getWriter().write("Failed to export data");
        }
    }

    public boolean exportToExcel(List<ReportOverallPoint> list, HttpServletResponse response) {
        Workbook workbook = null;
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            String uploadPath = getServletContext().getRealPath("") + File.separator + "Excel" + File.separator + "report.xlsx";
            fileInputStream = new FileInputStream(uploadPath);
            workbook = new XSSFWorkbook(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0); // Assuming the data is in the first sheet

            int rowNum = 13;
            int id = 1;

            for (ReportOverallPoint point : list) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(id);
                row.createCell(1).setCellValue(point.getStudentID());
                row.createCell(2).setCellValue(point.getInternID());
                row.createCell(3).setCellValue(point.getInternName());
                row.createCell(4).setCellValue(point.getDivision());
                row.createCell(5).setCellValue(point.getMentorName());
                row.createCell(6).setCellValue(point.getAllowance());
                row.createCell(7).setCellValue(point.getComment());
                row.createCell(8).setCellValue(point.getMajorSkillPoint());
                row.createCell(9).setCellValue(point.getSoftSkillPoint());
                row.createCell(10).setCellValue(point.getAttitudePoint());
                row.createCell(11).setCellValue(point.getFinalResult());

            }

            fileInputStream.close(); // Close the input stream before writing the output
            fileOutputStream = new FileOutputStream(uploadPath);
            workbook.write(fileOutputStream);

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=\"ExportedData.xlsx\"");
            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
