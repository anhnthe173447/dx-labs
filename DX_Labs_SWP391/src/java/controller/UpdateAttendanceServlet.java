/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.AttendanceDAO;
import DAL.DAOAccount;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpSession;
import java.time.ZoneId;
import java.util.List;
import models.Accounts;
import models.Group;
import models.InternAttendance;
import models.Interns;
import models.Student;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Date;

/**
 *
 * @author FPT
 */
@WebServlet(name = "UpdateAttendanceServlet", urlPatterns = {"/attendance"})
public class UpdateAttendanceServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet requests
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateAttendanceServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateAttendanceServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    private static final long serialVersionUID = 1L;
    private AttendanceDAO attendanceDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        attendanceDAO = new AttendanceDAO();
    }
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        List<Student> students = attendanceDAO.getStudentsByGroupId(1);
        // Convert LocalDateTime to Date
        for (Student student : students) {
            if (student.getCheckInTime() != null) {
                student.setCheckInDate(Date.from(student.getCheckInTime().atZone(ZoneId.systemDefault()).toInstant()));
            }
            if (student.getCheckOutTime() != null) {
                student.setCheckOutDate(Date.from(student.getCheckOutTime().atZone(ZoneId.systemDefault()).toInstant()));
            }
        }
        request.setAttribute("students", students);
        request.getRequestDispatcher("attendanceStatus.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        // lay mot list danh sach
        ObjectMapper mapper = new ObjectMapper();
        InternAttendance[] interns = mapper.readValue(request.getReader(), InternAttendance[].class);

        System.out.println(interns.length);
        boolean success = attendanceDAO.updateAttendance(interns);

        if (success) {
            response.getWriter().write("Cập nhật thành công");
        } else {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Đã xảy ra lỗi khi cập nhật");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
