/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.GetGroupDAO;
import DAL.InternDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Accounts;
import models.Group;
import models.Interns;
import models.Mentor;
import models.Student;

/**
 *
 * @author admin
 */
@WebServlet(name = "DisplayInternForMentor", urlPatterns = {"/displayInternForMentor"})
public class DisplayInternForMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayInternForMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayInternForMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        GetGroupDAO groupDAO = new GetGroupDAO();
        int maxGroupId = groupDAO.getHighestGroupId();
        int newGroupId = maxGroupId + 1;
        if (account == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        InternDAO internDAO = new InternDAO();

        String mentor_id_raw = request.getParameter("mentor_id");

        int mentorId = Integer.parseInt(mentor_id_raw);
        List<Group> groups = internDAO.getGroupsByMentorId(mentorId);
        List<Interns> interns = internDAO.getAllInterns();
        List<Student> students = internDAO.getAllStudent();
        Mentor mentor = internDAO.getMentorByMentorID(mentorId);
        request.setAttribute("mentor", mentor);

        request.setAttribute("interns", interns);
        request.setAttribute("groups", groups);
        request.setAttribute("students", students);
        request.getRequestDispatcher("displayInternGroupForMentor.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
