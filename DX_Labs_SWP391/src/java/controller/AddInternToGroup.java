/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.InternDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Accounts;
import models.Group;
import models.Interns;
import models.Student;

/**
 *
 * @author 84989
 */
public class AddInternToGroup extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddInternToGroup</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddInternToGroup at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String intern_id = request.getParameter("intern_id");
        String groupId = request.getParameter("groupId");
        InternDAO internDAO = new InternDAO();
        Interns intern = internDAO.getInternById(intern_id);
        List<Group> group = internDAO.getAllGroup();
        List<Student> student = internDAO.getAllStudent();

        request.setAttribute("groupId", groupId);
        request.setAttribute("group", group);
        request.setAttribute("student", student);
        request.setAttribute("intern", intern);
        request.getRequestDispatcher("AddInternToGroup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String intern_id = request.getParameter("intern_id");
        int group_id = Integer.parseInt(request.getParameter("group_id"));

        InternDAO internDAO = new InternDAO();
        internDAO.addInternToGroup(intern_id, group_id);

        response.sendRedirect("GroupDetail?group_id=" + group_id);
    }
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
