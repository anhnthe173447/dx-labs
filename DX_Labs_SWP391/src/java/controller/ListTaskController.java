package controller;

import DAL.DAOListTaskIntern;
import DAL.DAOTask;
import DAL.GetInternByUserID;
import models.Accounts;
import models.Mentor;
import models.Task;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ListTaskController handles listing and searching tasks for mentors.
 */
public class ListTaskController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method to list tasks.
     *
     * @param request the servlet request
     * @param response the servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        DAOListTaskIntern dao = new DAOListTaskIntern();
        GetInternByUserID dao1 = new GetInternByUserID();
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        if (account == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        Mentor mentor = dao1.getMentorByUserID(account.getID());
        if (mentor == null) {
            response.sendRedirect("error.jsp");
            return;
        }
        int mentorId = mentor.getMentorId(); // or the appropriate method to get the mentorId
        List<Task> listTask = dao.getListTask(mentorId);
        request.setAttribute("listTask", listTask);
        request.setAttribute("mentorID", mentorId); // Set mentorID as an attribute
        request.getRequestDispatcher("task.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        DAOListTaskIntern dao = new DAOListTaskIntern();
        DAOTask taskDAO = new DAOTask();
        String searchValue = request.getParameter("query");
        GetInternByUserID dao1 = new GetInternByUserID();
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        if (account == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        Mentor mentor = dao1.getMentorByUserID(account.getID());
        if (mentor == null) {
            response.sendRedirect("error.jsp");
            return;
        }
        int mentorId = mentor.getMentorId(); // or the appropriate method to get the mentorId
        List<Task> tasks;
        if (searchValue == null || searchValue.trim().isEmpty()) {
            // If no search value, get all tasks for the mentor
            tasks = dao.getListTask(mentorId);
           
        } else {         
            tasks = taskDAO.searchTask(searchValue, mentorId);
            if (tasks.isEmpty()) {
                request.setAttribute("message", "No tasks found.");
            }
        }
        request.setAttribute("listTask", tasks);
        request.setAttribute("searchValue", searchValue);
        request.getRequestDispatcher("task.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Servlet for listing and searching tasks for mentors.";
    }
}