/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.FinalTermPointDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.FinaltermPoint;

/**
 *
 * @author admin
 */
public class UpdateFinalPoint extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");

        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        
        String internID = request.getParameter("id");
        //EP KIEU
        int idIntern = Integer.parseInt(internID);

        FinalTermPointDAO fdao = new FinalTermPointDAO();
        FinaltermPoint finalTermPoint = fdao.getFinalPointByInternID(idIntern);

        request.setAttribute("FinaltermPoint", finalTermPoint);
        request.getRequestDispatcher("UpdateFinalPoint.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");

        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String internID = request.getParameter("internID");
        //EP KIEU
        int idIntern = Integer.parseInt(internID);
        float majorPoints = Float.parseFloat(request.getParameter("majorPoints"));
        float softSkillPoints = Float.parseFloat(request.getParameter("softSkillPoints"));
        float attitudePoints = Float.parseFloat(request.getParameter("attitudePoints"));
        String comment = request.getParameter("comment");

        FinaltermPoint finaltermPoint = new FinaltermPoint();
        finaltermPoint.setInternID(idIntern);
        finaltermPoint.setMajorPoints(majorPoints);
        finaltermPoint.setSoftSkillPoints(softSkillPoints);
        finaltermPoint.setAttitudePoints(attitudePoints);
        finaltermPoint.setComment(comment);

        FinalTermPointDAO fdao = new FinalTermPointDAO();
        boolean updated = fdao.updateFinalPoint(finaltermPoint);
        if (updated) {
            request.setAttribute("message", "Ending stage updated successfully!");
        } else {
            request.setAttribute("message", "No beginning stage found for intern ID: " + internID);
        }

        response.sendRedirect("DetailFinalTermPoint_Mentor?id=" + internID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
