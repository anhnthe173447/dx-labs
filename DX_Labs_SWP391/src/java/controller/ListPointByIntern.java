/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOIntern;
import DAL.FinalTermPointDAO;
import DAL.MidtermPointDAO;
import DAL.OverallPointDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import javax.mail.Session;
import models.Accounts;
import models.FinaltermPoint;
import models.Interns;
import models.MidtermPoint;
import models.OverallPoint;

/**
 *
 * @author ADMIN
 */
public class ListPointByIntern extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListPointByIntern</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListPointByIntern at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        int getList = Integer.parseInt(id);
        HttpSession session = request.getSession();

        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        DAOIntern dao = new DAOIntern();
        MidtermPointDAO daoM = new MidtermPointDAO();
        FinalTermPointDAO daoF = new FinalTermPointDAO();
        OverallPointDAO daoO = new OverallPointDAO();
        Interns intern = dao.getInternByAccount(account.getID() + "");
        int idIntern = Integer.parseInt(intern.getIdIntern());
        float average = 0;
        String status = null;

        if (getList == 1) {
            MidtermPoint point = daoM.getMidtermPointByInternID(idIntern);
            if (point == null) {
                request.setAttribute("mess", "There are no midterm grades yet");
            } else {
                float sum = point.getMajorPoints() + point.getAttitudePoints() + point.getSoftSkillPoints();
                float averageO = sum / 3;

                if (0 <= averageO && averageO <= 5) {
                    status = "Poor";
                } else if (5 <= averageO && averageO <= 8) {
                    status = "Average";
                } else if (8 <= averageO && averageO <= 10) {
                    status = "Ecxelent";
                }
                request.setAttribute("id", getList);
                request.setAttribute("status", status);
                request.setAttribute("average", averageO);
                request.setAttribute("point", point);
            }

        } else if (getList == 2) {
            FinaltermPoint point = daoF.getFinalPointByInternID(idIntern);
            if (point == null) {
                request.setAttribute("mess", "There are no final grades yet");
            } else {
                float sum = point.getMajorPoints() + point.getAttitudePoints() + point.getSoftSkillPoints();
                float averageO = sum / 3;
                if (0 <= averageO && averageO <= 5) {
                    status = "Poor";
                } else if (5 <= averageO && averageO <= 8) {
                    status = "Average";
                } else if (8 <= averageO && averageO <= 10) {
                    status = "Ecxelent";
                }
                request.setAttribute("id", getList);
                request.setAttribute("status", status);
                request.setAttribute("average", averageO);
                request.setAttribute("point", point);
            }

        } else if (getList == 3) {
            OverallPoint point = daoO.getOverallPointByInternID(idIntern);
            FinaltermPoint point2 = daoF.getFinalPointByInternID(idIntern);
            if (point == null) {
                request.setAttribute("mess", "There are no overall grades yet");
            } else {
                average = point.getFinalResult();
                float sum = point.getMajorSkillPoint() + point.getAttitudePoint() + point.getSoftSkillPoint();
                float averageO = sum / 3;
                if (0 <= averageO && averageO <= 5) {
                    status = "Poor";
                } else if (5 <= averageO && averageO <= 8) {
                    status = "Average";
                } else if (8 <= averageO && averageO <= 10) {
                    status = "Ecxelent";
                }
                request.setAttribute("point2", point2);
                request.setAttribute("id", getList);
                request.setAttribute("status", status);
                request.setAttribute("average", averageO);
                request.setAttribute("point", point);
            }

        }
        request.getRequestDispatcher("ReportGrade.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
