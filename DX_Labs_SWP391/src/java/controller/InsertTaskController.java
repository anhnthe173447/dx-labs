/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOListTaskIntern;
import DAL.DAOTask;
import DAL.GetGroupDAO;
import DAL.GetInternByUserID;
import DAL.GetMentorDAO;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.List;
import models.Accounts;
import models.Group;
import models.Mentor;
import models.Task;
import models.TaskStatus;

/**
 *
 * @author Laptop K1
 */
public class InsertTaskController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertTaskController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertTaskController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        GetInternByUserID dao1 = new GetInternByUserID();
        GetGroupDAO dao = new GetGroupDAO();
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts a = (Accounts) session.getAttribute("acc");
        Mentor m = dao1.getMentorByUserID(a.getID());

        List<Group> list = dao.getGroupByUserID(a.getID());
        request.setAttribute("listg", list);
        request.setAttribute("listM", m);

        request.getRequestDispatcher("createTask.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOTask dao = new DAOTask();
        String mentorID = request.getParameter("mentorID");
        String groupID = request.getParameter("groupID");
        String taskname = request.getParameter("taskname");
        String description = request.getParameter("description");
        String start = request.getParameter("start");
        String end = request.getParameter("end");

        LocalDate inputStartDate = LocalDate.parse(start);
        LocalDate inputEndDate = LocalDate.parse(end);
        LocalDate minSemesterStartDate = LocalDate.of(2024, 5, 1);
        LocalDate maxSemesterEndDate = LocalDate.of(2024, 8, 31);

        String mess = null;

        if (inputStartDate.isBefore(minSemesterStartDate) || inputStartDate.isAfter(maxSemesterEndDate)) {
            mess = "The semester starts on May 1, 2024 and ends on August 31, 2024";
        } else if (inputEndDate.isAfter(maxSemesterEndDate)) {
            mess = "The end date must not be later than August 31, 2024";
        } else if (inputEndDate.isBefore(inputStartDate)) {
            mess = "The end date must be greater than or equal to the start date";
        }

        if (mess != null) {
            request.setAttribute("mess", mess);
            doGet(request, response);  // Call doGet to set attributes and forward to JSP
        } else {
            dao.insertTask(mentorID, groupID, taskname, description, start, end);
            response.sendRedirect("tasks");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

// </editor-fold>
}
