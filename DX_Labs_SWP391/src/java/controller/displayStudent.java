/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.InternDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Accounts;
import models.Student;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "displayStudent", urlPatterns = {"/displayStudents"})
public class displayStudent extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayAllInternsAdded</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayAllInternsAdded at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        InternDAO internDAO = new InternDAO();

        String mess = null;
        List<Student> students = internDAO.getAllStudent();
        request.setAttribute("students", students);
        request.getRequestDispatcher("StudentManager.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        InternDAO internDAO = new InternDAO();
        List<Student> students = internDAO.getAllStudent();
        for (Student student : students) {
            String StatusCV = "status_" + student.getRoll_number();
            String newStatus = request.getParameter(StatusCV);
            if (newStatus != null && !newStatus.isEmpty()) {
                internDAO.updateCVStatus(student.getRoll_number(), newStatus);
                int checkStatus = Integer.parseInt(newStatus);
                if (checkStatus == 1 && internDAO.doesInternExist(student.getRoll_number()) == false) {
                    try {
                        internDAO.insertIntern(student.getRoll_number());
                    } catch (SQLException ex) {

                    }
                }
            }
        }
        request.setAttribute("students", students);
        response.sendRedirect("displayStudents");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
