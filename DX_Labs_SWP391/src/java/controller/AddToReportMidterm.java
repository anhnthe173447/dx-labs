/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.MidtermPointDAO;
import DAL.ReportDAO;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.MidtermPoint;

/**
 *
 * @author ADMIN
 */
public class AddToReportMidterm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String id = request.getParameter("id");
        int idIntern = Integer.parseInt(id);
        MidtermPointDAO b = new MidtermPointDAO();
        ReportDAO daoR = new ReportDAO();
        List<MidtermPoint> MidtermPList = b.getListMidtermPoint(u.getID());
        for (MidtermPoint midtermPoint : MidtermPList) {
            if (id.equalsIgnoreCase(midtermPoint.getInternID()+"") ) {
                float AveragePoint = midtermPoint.attitudePoints + midtermPoint.majorPoints + midtermPoint.softSkillPoints;
                if (0 <= AveragePoint && AveragePoint <= 6) {
                    daoR.AddReportMidterm(midtermPoint.getMidtermID() , midtermPoint.getComment(), "Poor");
                    request.setAttribute("mess", "Add to report midterm successfully");
                } else if (7 <= AveragePoint && AveragePoint <= 12) {
                    daoR.AddReportMidterm(midtermPoint.getMidtermID(), midtermPoint.getComment(), "Average");
                    request.setAttribute("mess", "Add to report midterm successfully");
                } else if (13 <= AveragePoint && AveragePoint <= 18) {
                    daoR.AddReportMidterm(midtermPoint.getMidtermID() , midtermPoint.getComment(), "Good");
                    request.setAttribute("mess", "Add to report midterm successfully");
                } else if (19 <= AveragePoint && AveragePoint <= 24) {
                    daoR.AddReportMidterm(midtermPoint.getMidtermID(), midtermPoint.getComment(), "Very Good");
                    request.setAttribute("mess", "Add to report midterm successfully");
                } else if (25 <= AveragePoint && AveragePoint <= 30) {
                    daoR.AddReportMidterm(midtermPoint.getMidtermID(), midtermPoint.getComment(), "Excellent");
                    request.setAttribute("mess", "Add to report midterm successfully");
                } else {
                    request.setAttribute("mess", "Invalid point range");
                }

                break;
            }
        }
        request.getRequestDispatcher("ListMidtermPoint").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
