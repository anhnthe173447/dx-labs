/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOIntern;
import DAL.GroupDAO;
import DAL.MentorDAO;
import DAL.SemesterDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;
import models.Group;
import models.Intern_Student;
import models.Mentor;
import models.Semester;

/**
 *
 * @author ThinkPro
 */
public class OverallEvaluationController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OverallEvaluation</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OverallEvaluation at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        int sesmesterId = 2;
        String ses_raw = request.getParameter("semester_ID");
        if (ses_raw != null && !ses_raw.isEmpty()) {
            sesmesterId = Integer.parseInt(ses_raw);
        }

        int groupID = -1;
        String groupID_raw = request.getParameter("group_ID");
        if (groupID_raw != null && !groupID_raw.isEmpty()) {
            groupID = Integer.parseInt(groupID_raw);
        }

        SemesterDAO sd = new SemesterDAO();
        List<Semester> ses = sd.getAllSemester();

        request.setAttribute("ses", ses);

        DAOIntern di = new DAOIntern();
        MentorDAO md = new MentorDAO();
        GroupDAO gd = new GroupDAO();

        Mentor mentor = md.getMentorByUserId(u.getID());
        List<Group> groups = gd.getGroupsByMentorId(mentor.getMentorId());
        request.setAttribute("group", groups);

        List<Intern_Student> interns = di.getOverallPointOfInternsByGroupIds(groups);

        List<Intern_Student> filteredInterns = new ArrayList<>();
        for (Intern_Student intern : interns) {
            boolean matchesSemester = (sesmesterId == -1 || intern.getSemester_ID() == sesmesterId);
            boolean matchesGroup = (groupID == -1 || intern.getGroup_ID() == groupID);
            if (matchesSemester && matchesGroup) {
                filteredInterns.add(intern);
            }
        }
        request.setAttribute("interns", filteredInterns);
        request.setAttribute("sesId", sesmesterId);
        request.setAttribute("group_ID", groupID);
        request.getRequestDispatcher("overall-evaluate.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
