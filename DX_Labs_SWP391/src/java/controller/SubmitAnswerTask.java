/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOAnswerTask;
import DAL.DAOTask;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import models.Accounts;
import models.Answer;
import models.Upload;

/**
 *
 * @author Laptop K1
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class SubmitAnswerTask extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubmitAnswerTask</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubmitAnswerTask at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }        String taskID = request.getParameter("taskID");
        String groupID = request.getParameter("groupID");
        String endDateStr = request.getParameter("end");
        session.setAttribute("taskID", taskID);

        Timestamp answerTime = new Timestamp(System.currentTimeMillis());
        Timestamp endDate;
        try {
            endDate = Timestamp.valueOf(endDateStr + " 00:00:00");
        } catch (IllegalArgumentException e) {
            session.setAttribute("submitMessage", "Invalid end date format.");
            response.sendRedirect("answerTask?taskID=" + taskID);
            return;
        }

        if (answerTime.after(endDate)) {
            session.setAttribute("submitMessage", "Submission deadline has passed.");
            response.sendRedirect("answerTask?taskID=" + taskID);
            return;
        }

        try {
            Part filePart = request.getPart("file");

            if (filePart == null || filePart.getSize() == 0) {
                session.setAttribute("submitMessage", "Please select a file to upload.");
                response.sendRedirect("answerTask?taskID=" + taskID);
                return;
            }

            String fileName = filePart.getSubmittedFileName();
            String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (!isValidFileType(fileType)) {
                session.setAttribute("submitMessage", "Invalid file type. Please upload zip, rar, doc, docx, xls, xlsx files.");
                response.sendRedirect("answerTask?taskID=" + taskID);
                return;
            }

            long fileSize = filePart.getSize();
            long maxSize = 1024 * 1024 * 10;

            if (fileSize > maxSize) {
                session.setAttribute("submitMessage", "File size exceeds the limit of 10MB.");
                response.sendRedirect("answerTask?taskID=" + taskID);
                return;
            }

            String filePath = "./uploads/answer/";
            String uploadPath = getServletContext().getRealPath(filePath);
            Upload upload = new Upload();
            String namePathSaveDB = upload.uploadFile(filePart, uploadPath);

            DAOAnswerTask dao = new DAOAnswerTask();
            boolean isInsertedOrUpdated;
            DAOTask daoTask = new DAOTask();

            if (dao.answerExists(taskID, groupID)) {
                isInsertedOrUpdated = dao.updateAnswer(taskID, namePathSaveDB, answerTime, groupID);
                daoTask.updateTaskStatus(taskID);
            } else {
                isInsertedOrUpdated = dao.insertAnswerTask(taskID, groupID, namePathSaveDB, answerTime);
                daoTask.updateTaskStatus(taskID);
            }

            if (isInsertedOrUpdated) {
                session.setAttribute("submitMessage", "Submission successful.");
            } else {
                session.setAttribute("submitMessage", "Update successful.");
            }

            response.sendRedirect("answerTask?taskID=" + taskID);
        } catch (IllegalStateException e) {
            // Handle exception
        }

    }

    private boolean isValidFileType(String fileType) {
        String[] allowedFileTypes = {"zip", "rar", "doc", "docx", "xls", "xlsx"};
        for (String type : allowedFileTypes) {
            if (type.equalsIgnoreCase(fileType)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
