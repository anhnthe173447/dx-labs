/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOAccount;
import DAL.DAOIntern;
import DAL.DAOStudent;
import DAL.MentorDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;
import models.Interns;
import models.Mentor;
import models.Students;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ManagerIntern", urlPatterns = {"/managerIntern"})
public class ManagerIntern extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        DAOIntern dao = new DAOIntern();
        DAOStudent daoS = new DAOStudent();
        DAOAccount daoA = new DAOAccount();
        MentorDAO daoM= new MentorDAO();
        List<Interns> list = dao.getAllIntern();
        List<Students> listS = new ArrayList<>();
        List<Accounts> listA = new ArrayList<>();
        List<Mentor> listM= new ArrayList<>();
        for (Interns interns : list) {
            String idStudent = interns.getIdStudent();
            String idACC = interns.getUserID() + "";
            Students s = daoS.getStudentByID(idStudent);
            listS.add(s);
            Accounts a = daoA.getAccountByID(idACC);
            listA.add(a);
            Mentor m= daoM.getMentorByInternID(interns.getIdIntern());
            listM.add(m);
        }
        request.setAttribute("listA", listA);
        request.setAttribute("listS", listS);
        request.setAttribute("listI", list);
        request.setAttribute("listM", listM);
        request.getRequestDispatcher("InternManager.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOIntern dao = new DAOIntern();
        List<Interns> interns = dao.getAllIntern();

        for (Interns intern : interns) {
            String statusParam = "status_" + intern.getIdIntern();
            String newStatus = request.getParameter(statusParam);
            if (newStatus != null && !newStatus.isEmpty()) {
                // Assuming you have a method to update the intern's status
                dao.updateInternStatus(intern.getIdIntern(), newStatus);
            }
        }
        request.setAttribute("listI", interns);
        response.sendRedirect("managerIntern");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
