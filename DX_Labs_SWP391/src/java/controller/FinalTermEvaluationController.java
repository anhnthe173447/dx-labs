/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOIntern;
import DAL.GroupDAO;
import DAL.MentorDAO;
import DAL.FinalTermPointDAO;
import DAL.OverallPointDAO;
import DAL.SemesterDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import models.Accounts;
import models.FinaltermPoint;
import models.Group;
import models.Intern_Student;
import models.Mentor;
import models.OverallPoint;
import models.Semester;

/**
 *
 * @author ThinkPro
 */
public class FinalTermEvaluationController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FinalTermEvaluation</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FinalTermEvaluation at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        int sesmesterId = 2;
        String ses_raw = request.getParameter("semester_ID");
        if (ses_raw != null && !ses_raw.isEmpty()) {
            sesmesterId = Integer.parseInt(ses_raw);
        }

        int groupID = -1;
        String groupID_raw = request.getParameter("group_ID");
        if (groupID_raw != null && !groupID_raw.isEmpty()) {
            groupID = Integer.parseInt(groupID_raw);
        }

        DAOIntern di = new DAOIntern();
        MentorDAO md = new MentorDAO();
        GroupDAO gd = new GroupDAO();
        SemesterDAO sd = new SemesterDAO();
        
        // Show option
        List<Semester> ses = sd.getAllSemester();
        request.setAttribute("ses", ses);

        // Show option
        Mentor mentor = md.getMentorByUserId(u.getID());
        List<Group> groups = gd.getGroupsByMentorId(mentor.getMentorId());
        request.setAttribute("group", groups);

        // Get all interns by groups
        List<Intern_Student> interns = di.getInternsByGroupIds(groups);

        // Filter interns by semester and group
        List<Intern_Student> filteredInterns = new ArrayList<>();
        for (Intern_Student intern : interns) {
            boolean matchesSemester = (sesmesterId == -1 || intern.getSemester_ID() == sesmesterId);
            boolean matchesGroup = (groupID == -1 || intern.getGroup_ID() == groupID);
            if (matchesSemester && matchesGroup) {
                filteredInterns.add(intern);
            }
        }
        
        request.setAttribute("interns", filteredInterns);
        request.setAttribute("group_ID", groupID);
        request.setAttribute("sesId", sesmesterId);
        request.getRequestDispatcher("finalterm-evaluate.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        FinalTermPointDAO finaltermPointDAO = new FinalTermPointDAO();

        // Get parameters from the form
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            if (paramName.startsWith("majorSkillPoint_")) {
                // Extract intern ID from paramName
                int internId = Integer.parseInt(paramName.substring("majorSkillPoint_".length()));

                // Get other parameters
                float majorSkillPoint = Float.parseFloat(request.getParameter(paramName));
                float softSkillPoint = Float.parseFloat(request.getParameter("softSkillPoint_" + internId));
                float attitudePoint = Float.parseFloat(request.getParameter("attitudePoint_" + internId));
                String comment = request.getParameter("comment_" + internId);

                // Create MidtermPoint object
                FinaltermPoint midtermPoint = new FinaltermPoint();
                midtermPoint.setInternID(internId);
                midtermPoint.setMajorPoints(majorSkillPoint);
                midtermPoint.setSoftSkillPoints(softSkillPoint);
                midtermPoint.setAttitudePoints(attitudePoint);
                midtermPoint.setComment(comment);

                // Save to database
                int result = finaltermPointDAO.saveFinaltermPoint(midtermPoint);
                if (result > 0) {
                    DAOIntern di = new DAOIntern();
                    MentorDAO md = new MentorDAO();
                    GroupDAO gd = new GroupDAO();

                    session.setAttribute("notification", "Save Midterm Evaluation successfully");
                    session.setAttribute("typeNoti", "alert-success");

                    //Cập nhật điểm bảng OverallPoint = (midterm+final)/2.0 (dành cho majorSkillPoint, attitudePoint, SoftSkillPoint) 
                    // check nếu majorSkillPoint_Final, attitudePoint_Final , SoftSkillPoint_Point có giá trị thì truyền giá trị còn không có thì mặc định = 0
                    Mentor mentor = md.getMentorByUserId(u.getID());
                    List<Group> groups = gd.getGroupsByMentorId(mentor.getMentorId());

                    List<Intern_Student> interns = di.getInternsByGroupIds(groups);
                    Intern_Student i = new Intern_Student();
                    for (Intern_Student is : interns) {
                        if (is.getIntern_ID() == internId) {
                            i = is;
                            break;
                        }
                    }

                    float majorSkillPoint_Midterm = (!(i.getMajorSkillPoint_Midterm() + "").isEmpty()) ? i.getMajorSkillPoint_Midterm() : 0;
                    float attitudePoint_Midterm = (!(i.getAttitudePoint_Midterm() + "").isEmpty()) ? i.getAttitudePoint_Midterm() : 0;
                    float softSkillPoint_Midterm = (!(i.getSoftSkillPoint_Midterm() + "").isEmpty()) ? i.getSoftSkillPoint_Midterm() : 0;

                    float majorSkillPoint_Overall = (majorSkillPoint + majorSkillPoint_Midterm) / 2.0f;
                    float attitudePoint_Overall = (attitudePoint + attitudePoint_Midterm) / 2.0f;
                    float softSkillPoint_Overall = (softSkillPoint + softSkillPoint_Midterm) / 2.0f;
                    float finalResult = (majorSkillPoint_Overall + attitudePoint_Overall + softSkillPoint_Overall) / 3.0f;

                    OverallPointDAO od = new OverallPointDAO();
                    OverallPoint overallPoint = od.getByInternId(internId);
                    if (overallPoint != null) {
                        overallPoint.setMajorSkillPoint(majorSkillPoint_Overall);
                        overallPoint.setSoftSkillPoint(softSkillPoint_Overall);
                        overallPoint.setAttitudePoint(attitudePoint_Overall);
                        overallPoint.setFinalResult(finalResult);
                        od.save(overallPoint, "update");
                    } else {
                        overallPoint = new OverallPoint();
                        overallPoint.setMajorSkillPoint(majorSkillPoint_Overall);
                        overallPoint.setSoftSkillPoint(softSkillPoint_Overall);
                        overallPoint.setAttitudePoint(attitudePoint_Overall);
                        overallPoint.setFinalResult(finalResult);
                        od.save(overallPoint, "insert");
                    }

                } else {
                    session.setAttribute("notification", "Saved failed. internal error");
                    session.setAttribute("typeNoti", "alert-danger");
                }

            }
        }
        response.sendRedirect(request.getContextPath() + "/finalterm-evaluate");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
