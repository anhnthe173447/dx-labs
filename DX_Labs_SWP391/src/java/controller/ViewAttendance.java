/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.AttendanceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import models.Accounts;
import models.Student;

/**
 *
 * @author FPT
 */
public class ViewAttendance extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet viewAttendance</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet viewAttendance at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    private static final long serialVersionUID = 1L;
    private AttendanceDAO attendanceDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        attendanceDAO = new AttendanceDAO();
    }
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String dateParam = request.getParameter("date");
        java.util.Date selectedDate = null;

        if (dateParam != null && !dateParam.isEmpty()) {
            try {
                // Use java.util.Date for parsing
                selectedDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateParam);
            } catch (ParseException e) {
                e.printStackTrace(); // Log the error
                // Optionally handle the parse exception, e.g., set a default date or return an error message
            }
        } else {
            selectedDate = new java.util.Date(); // Default to today's date if no date is selected
        }

        // Convert java.util.Date to java.sql.Date
        java.sql.Date sqlDate = new java.sql.Date(selectedDate.getTime());

        // Create an instance of AttendanceDAO
        AttendanceDAO dao = new AttendanceDAO();
        List<Student> students = dao.getAttendanceForDate(sqlDate);

        // Convert LocalDateTime to Date
        for (Student student : students) {
            if (student.getCheckInTime() != null) {
                student.setCheckInDate(java.util.Date.from(student.getCheckInTime().atZone(ZoneId.systemDefault()).toInstant()));
            }
            if (student.getCheckOutTime() != null) {
                student.setCheckOutDate(java.util.Date.from(student.getCheckOutTime().atZone(ZoneId.systemDefault()).toInstant()));
            }
        }
        request.setAttribute("students", students);
        // Set attributes and forward to JSP
        request.setAttribute("students", students);
        request.setAttribute("selectedDate", new SimpleDateFormat("yyyy-MM-dd").format(selectedDate));
        request.getRequestDispatcher("attendanceStatus.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
