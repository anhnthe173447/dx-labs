package controller;

import DAL.GetGroupDAO;
import DAL.InternDAO;
import jakarta.servlet.ServletException;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.Group;
import models.Mentor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DisplayGroup extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        GetGroupDAO groupDAO = new GetGroupDAO();
        int maxGroupId = groupDAO.getHighestGroupId();
        int newGroupId = maxGroupId + 1;
        if (account == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        String userId = String.valueOf(account.getID());
        InternDAO internDAO = new InternDAO();
        Mentor mentor = null;

        try {
            mentor = internDAO.getMentorByUserId(userId);

            if (mentor == null) {
                request.setAttribute("mess", "No mentor information found.");
            } else {
                int mentorId = mentor.getMentorId();
                List<Group> groups = internDAO.getGroupsByMentorId(mentorId);

                request.setAttribute("mentorName", mentor.getFullName());
                request.setAttribute("mentorId", mentor.getMentorId());
                if (groups == null || groups.isEmpty()) {
                    request.setAttribute("mess", "No groups found.");
                } else {
                    request.setAttribute("groups", groups);
                }
            }
        } catch (Exception e) {

            request.setAttribute("mess", "Error fetching groups or mentor information.");
        }
        request.setAttribute("newGroupId", newGroupId);
        request.getRequestDispatcher("group.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String searchValue = request.getParameter("searchValue");

        HttpSession session = request.getSession();
        Accounts account = (Accounts) session.getAttribute("acc");

        if (account == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        String userId = String.valueOf(account.getID());
        InternDAO internDAO = new InternDAO();
        Mentor mentor = null;

        try {
            mentor = internDAO.getMentorByUserId(userId);

            if (mentor == null) {
                request.setAttribute("mess", "No mentor information found.");
            } else {
                int mentorId = mentor.getMentorId();
                List<Group> groups = new ArrayList<>();

                if (searchValue != null && !searchValue.trim().isEmpty()) {
                    searchValue = searchValue.trim().replaceAll("\\s+", " ");
                    groups = internDAO.getGroupBySearchName(searchValue, mentorId);
                } else {
                    request.setAttribute("message", "Please input a search value.");
                    groups = internDAO.getGroupsByMentorId(mentorId);
                }

                request.setAttribute("searchValue", searchValue);
                request.setAttribute("mentorName", mentor.getFullName());
                request.setAttribute("mentorId", mentor.getMentorId());

                if (groups == null || groups.isEmpty()) {
                    request.setAttribute("mess", "No groups found.");
                } else {
                    request.setAttribute("groups", groups);
                }
            }
        } catch (Exception e) {

            request.setAttribute("mess", "An error occurred while processing your request.");
        }

        request.getRequestDispatcher("group.jsp").forward(request, response);
    }
}
