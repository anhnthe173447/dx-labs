package controller;

import DAL.InternDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@MultipartConfig
public class uploadExcel extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("importExcel.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Part filePart = request.getPart("file");

        if (!filePart.getSubmittedFileName().endsWith(".xls") && !filePart.getSubmittedFileName().endsWith(".xlsx")) {
            session.setAttribute("errorMessage", "File is not an Excel file.");
            response.sendRedirect("uploadExcel");
            return;
        }
        try (InputStream inputStream = filePart.getInputStream()) {
            XSSFWorkbook wb = new XSSFWorkbook(inputStream);
            Sheet sheet = wb.getSheetAt(0);
            // Check if the template matches
            Row headerRow = sheet.getRow(0);
            if (headerRow == null || !isHeaderValid(headerRow)) {
                session.setAttribute("errorMessage", "Excel file does not match the required template.");
                response.sendRedirect("uploadExcel");
                return;
            }

            InternDAO internDAO = new InternDAO();
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next();
            DataFormatter dataFormatter = new DataFormatter();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                String rollNumber = dataFormatter.formatCellValue(row.getCell(0));
                String email = dataFormatter.formatCellValue(row.getCell(1));
                String fullName = dataFormatter.formatCellValue(row.getCell(2));
                String phoneNumber = dataFormatter.formatCellValue(row.getCell(3));
                String major = dataFormatter.formatCellValue(row.getCell(4));
                String company = dataFormatter.formatCellValue(row.getCell(5));
                String jobTitle = dataFormatter.formatCellValue(row.getCell(6));
                String linkCV = dataFormatter.formatCellValue(row.getCell(7));
                int statusCV = 1;
                if (rollNumber == null || rollNumber.isEmpty()) {
                    continue; 
                }

                // Check if student already exists
                if (internDAO.doesStudentExist(rollNumber)) {
                    session.setAttribute("errorMessage", "Student ID " + rollNumber + " already exists in intern list.");
                    response.sendRedirect("uploadExcel");
                    return;
                }
                // Check if email already exists
                if (internDAO.doesEmailtExist(email)) {
                    session.setAttribute("errorMessage", "Email " + email + " already exists in intern list.");
                    response.sendRedirect("uploadExcel");
                    return;
                }
                // Check if phone number already exists
                if (internDAO.doesPhoneNumbertExist(phoneNumber)) {
                    session.setAttribute("errorMessage", "Phone Number " + phoneNumber + " already exists in intern list.");
                    response.sendRedirect("uploadExcel");
                    return;
                }

                internDAO.insertStudent(rollNumber, fullName, phoneNumber, email, major, company, jobTitle, linkCV, statusCV);
            }

            session.setAttribute("successMessage", "Excel file imported successfully.");
            response.sendRedirect("displayStudentAdd");

        } catch (IOException e) {
            session.setAttribute("errorMessage", "Error processing Excel file: " + e.getMessage());
            response.sendRedirect("uploadExcel");
        }
    }

    private boolean isHeaderValid(Row headerRow) {
        if (headerRow.getLastCellNum() != 9) {
            return false;
        }
        try {
            return headerRow.getCell(0).getStringCellValue().equals("Roll Number")
                    && headerRow.getCell(1).getStringCellValue().equals("Email")
                    && headerRow.getCell(2).getStringCellValue().equals("Full Name")
                    && headerRow.getCell(3).getStringCellValue().equals("Phone Number")
                    && headerRow.getCell(4).getStringCellValue().equals("Major")
                    && headerRow.getCell(5).getStringCellValue().equals("Company")
                    && headerRow.getCell(6).getStringCellValue().equals("Job Title")
                    && headerRow.getCell(7).getStringCellValue().equals("Link cv")
                    && headerRow.getCell(8).getStringCellValue().equals("CV Status");
        } catch (IllegalStateException e) {
            return false;
        }
    }
}
