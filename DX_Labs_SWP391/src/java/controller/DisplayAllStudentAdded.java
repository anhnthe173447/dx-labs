    package controller;

import DAL.InternDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.util.List;
import models.Accounts;

import models.Interns;
import models.Student;

@WebServlet(name = "DisplayAllStudentAdded", urlPatterns = {"/displayStudentAdd"})
public class DisplayAllStudentAdded extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayAllInternsAdded</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayAllInternsAdded at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        InternDAO internDAO = new InternDAO();

        String mess = null;
        List<Interns> interns = internDAO.getAllInterns();
        List<Student> students = internDAO.getAllStudent();

        if (interns.isEmpty() || students.isEmpty()) {
            mess = "No interns and students found.";
            request.setAttribute(mess, mess);
        } else {

            request.setAttribute("students", students);
            request.setAttribute("interns", interns);

        }
        request.getRequestDispatcher("displayAllStudentAdded.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
