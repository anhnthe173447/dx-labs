/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.GetMentorDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.Mentor;

/**
 *
 * @author 84989
 */
public class InsertMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        GetMentorDAO mentorDAO = new GetMentorDAO();
        String fullName = request.getParameter("fullName");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String genderParam = request.getParameter("gender");

        String fullNameRegex = "^[a-zA-Z\\s]+$";
        String emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        String phoneRegex = "(84|0[3|5|7|8|9])+([0-9]{9})\\b";

        int gender = "Male".equals(genderParam) ? 1 : 0;

        int highestMentorId = mentorDAO.getHighestMentorId();
        int mentorId = highestMentorId + 1;

        int newUserIdInt = mentorDAO.getNewUserId();
        String userName = "Mentor" + mentorId;
        String pass = "1";
        String isActive = "1";
        int role = 1;
        Accounts account = new Accounts(newUserIdInt, userName, pass, "", isActive, role);

        boolean accountSuccess = mentorDAO.insertAccount(account);
        if (accountSuccess) {

            if (fullName == null || phone == null || email == null
                    || !fullName.matches(fullNameRegex)
                    || !phone.matches(phoneRegex)
                    || !email.matches(emailRegex)) {

                mentorDAO.deleteAccount(newUserIdInt);
                request.setAttribute("errorMessage", "Invalid input. Please check your entries and try again.");
                request.getRequestDispatcher("AddMentor.jsp").forward(request, response);
                return;
            }
            boolean emailExists = mentorDAO.emailExists(email);
            boolean phoneExists = mentorDAO.phoneNumberExists(phone);

            if (emailExists && phoneExists) {
                mentorDAO.deleteAccount(newUserIdInt);
                request.setAttribute("errorMessage", "Email and phone number already exist. Please use different values.");
                request.getRequestDispatcher("AddMentor.jsp").forward(request, response);
                return;
            } else if (emailExists) {
                mentorDAO.deleteAccount(newUserIdInt);
                request.setAttribute("errorMessage", "Email already exists. Please use a different email.");
                request.getRequestDispatcher("AddMentor.jsp").forward(request, response);
                return;
            } else if (phoneExists) {
                mentorDAO.deleteAccount(newUserIdInt);
                request.setAttribute("errorMessage", "Phone number already exists. Please use a different phone number.");
                request.getRequestDispatcher("AddMentor.jsp").forward(request, response);
                return;
            }

            String userId = String.valueOf(newUserIdInt);

            Mentor mentor = new Mentor(mentorId, userId, fullName, email, phone, String.valueOf(gender), "", "", userId);
            
            boolean mentorSuccess = mentorDAO.insertMentor(mentor);

            if (mentorSuccess) {
                response.sendRedirect("displayMentors");
            } else {

                mentorDAO.deleteAccount(newUserIdInt);
                request.setAttribute("errorMessage", "Failed to insert Mentor. Please check the console for more details.");
                request.getRequestDispatcher("AddMentor.jsp").forward(request, response);
            }
        } else {

            request.setAttribute("errorMessage", "Failed to insert Account.");
            request.getRequestDispatcher("/AddMentor.jsp").forward(request, response);
        }
    }
}