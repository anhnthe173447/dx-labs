/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOAccount;
import DAL.DAOIntern;
import DAL.DAOStudent;
import DAL.GetMentorDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Path;
import models.Accounts;
import models.Interns;
import models.Mentor;

/**
 *
 * @author ADMIN
 *
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class ProfileMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfileMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfileMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Accounts account = (Accounts) session.getAttribute("acc");
        int id = account.getID();
        String idAccount = Integer.toString(id);
        GetMentorDAO dao = new GetMentorDAO();
        Mentor mentor = dao.getMentorByIDAccount(idAccount);
        request.setAttribute("account", account);
        request.setAttribute("mentor", mentor);
        request.getRequestDispatcher("ProfileMentor.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        DAOIntern dao = new DAOIntern();
        DAOStudent daoS = new DAOStudent();
        DAOAccount daoA = new DAOAccount();
        GetMentorDAO daoM = new GetMentorDAO();

        String fullname = request.getParameter("fullnameM");
        String IDMentor = request.getParameter("IDMentor");
        String address = request.getParameter("AddressM");
        String phoneNumber = request.getParameter("phoneM");
        String email = request.getParameter("emailM");
        String idCard = request.getParameter("IDCardM");
        String dob = request.getParameter("DOBM");
        String gender = request.getParameter("genderM");
        String userName = request.getParameter("UserNameM");

        boolean checkDOB = dao.isDateOver18(dob);
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        Accounts account = (Accounts) session.getAttribute("acc");
        String idAccount = Integer.toString(account.getID());
        String role = Integer.toString(account.getRole());

        String fullNameRegex = "^[a-zA-Z\\s]+$";
        String emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        String phoneRegex = "(84|0[3|5|7|8|9])+([0-9]{8})\\b";
        String idCardRegex = "^\\d{12}$";

        boolean isFullNameValid = fullname.matches(fullNameRegex);
        boolean isEmailValid = email.matches(emailRegex);
        boolean isPhoneValid = phoneNumber.matches(phoneRegex);
        boolean isIdCardValid = idCard.matches(idCardRegex);

        if (!isFullNameValid || fullname.isBlank()) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "Full name cannot be blank and cannot contain numbers or special characters");
        } else if (!isPhoneValid || phoneNumber.isBlank()) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "Please enter a phone number with more than 9 digits and start with 0 or 84");
        } else if (!isEmailValid || email.isBlank()) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "Please enter the correct email format");
        } else if (!isIdCardValid || idCard.isBlank()) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "ID Card must be 12 digits");
        } else if (!checkDOB) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "Date of birth must be 18 years old");
        } else if (userName.isBlank()) {
            Accounts acc = daoA.getAccountByID(idAccount);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            request.setAttribute("account", acc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("error", "User name cannot be blank");
        } else {
            Part filePart = request.getPart("profileImage");
            String fileName = Path.of(filePart.getSubmittedFileName()).getFileName().toString();
            String uploadPath = getServletContext().getRealPath("") + File.separator + "upload" + File.separator + fileName;
            File uploadDir = new File(uploadPath);
            filePart.write(uploadPath);
            String imagePath = "./upload/" + fileName;
            daoA.UpdateAccount(idAccount, userName, account.getPass(), role, imagePath);
            daoM.UpdateMentor(IDMentor, account.getID() + "", fullname, email, phoneNumber, gender, dob, idCard, address);
            Mentor mentor = daoM.getMentorByIDAccount(idAccount);
            Accounts newAcc = daoA.getAccountByID(idAccount);
            session.setAttribute("account", newAcc);
            session.setAttribute("acc", newAcc);
            request.setAttribute("mentor", mentor);
            request.setAttribute("success", "Update has been done successfully!");
        }
        request.getRequestDispatcher("ProfileMentor.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
