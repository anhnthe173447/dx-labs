/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOAccount;
import DAL.DAOIntern;
import DAL.DAOStudent;
import DAL.GetMentorDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;
import models.Interns;
import models.Mentor;
import models.Students;

/**
 *
 * @author ADMIN
 */
public class ManagerInternByMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManagerInternByMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManagerInternByMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts accountMentor = (Accounts) session.getAttribute("acc");
        if (accountMentor == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        DAOIntern dao = new DAOIntern();
        DAOStudent daoS = new DAOStudent();
        DAOAccount daoA = new DAOAccount();
        GetMentorDAO daoM = new GetMentorDAO();
        Mentor mentor = daoM.getMentorByID(accountMentor.getID() + "");
        List<Interns> list = dao.getInternByMentor(mentor.getMentorId());
        List<Students> listS = new ArrayList<>();
        List<Accounts> listA = new ArrayList<>();
        for (Interns interns : list) {
            String idStudent = interns.getIdStudent();
            String idACC = interns.getUserID() + "";
            Students s = daoS.getStudentByID(idStudent);
            listS.add(s);
            Accounts a = daoA.getAccountByID(idACC);
            listA.add(a);
        }
        request.setAttribute("listA", listA);
        request.setAttribute("listS", listS);
        request.setAttribute("listI", list);
        request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOIntern dao = new DAOIntern();
        DAOStudent daoS = new DAOStudent();
        DAOAccount daoA = new DAOAccount();
        GetMentorDAO daoM = new GetMentorDAO();
        HttpSession session = request.getSession();
        Accounts accountMentor = (Accounts) session.getAttribute("acc");
        Mentor mentor = daoM.getMentorByID(accountMentor.getID() + "");
         String semester = request.getParameter("statusFilter");
        if (semester == null) {
            semester = "0";

        }
        List<Interns> listAll = dao.getAllIntern();
        List<Interns> list = dao.getInternBySemester(mentor.getMentorId(), semester);
        List<Students> listS = new ArrayList<>();
        List<Accounts> listA = new ArrayList<>();
        for (Interns interns : list) {
            String idStudent = interns.getIdStudent();
            String idACC = interns.getUserID() + "";
            Students s = daoS.getStudentByID(idStudent);
            listS.add(s);
            Accounts a = daoA.getAccountByID(idACC);
            listA.add(a);
        }

        for (Interns intern : listAll) {
            String statusParam = "status_" + intern.getIdIntern();
            String newStatus = request.getParameter(statusParam);
            if (newStatus != null) {
                dao.updateInternStatus(intern.getIdIntern(), newStatus);
            }
        }
        if (semester.equals("0")) {
            request.setAttribute("semester", semester);
            response.sendRedirect("ManagerInternByMentor");

        } else {
            request.setAttribute("semester", semester);
            request.setAttribute("listI", list);
            request.setAttribute("listA", listA);
            request.setAttribute("listS", listS);
            // Redirect or forward to an appropriate page after processing
            request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
