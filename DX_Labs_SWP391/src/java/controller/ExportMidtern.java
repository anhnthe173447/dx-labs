/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.ReportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import models.Accounts;
import models.ReportMidtermPoint;
import models.ReportOverallPoint;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ADMIN
 */
public class ExportMidtern extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExportMidtern</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExportMidtern at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        ReportDAO daoR = new ReportDAO();
        List<ReportMidtermPoint> listOV = daoR.getListReportMidtern();
        boolean isExport = exportToExcel(listOV, response);

        if (!isExport) {
            response.getWriter().write("Failed to export data");
        }
    }

    public boolean exportToExcel(List<ReportMidtermPoint> list, HttpServletResponse response) {
        Workbook workbook = null;
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            String uploadPath = getServletContext().getRealPath("") + File.separator + "Excel" + File.separator + "report_Midterm.xlsx";
            fileInputStream = new FileInputStream(uploadPath);
            workbook = new XSSFWorkbook(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0); // Assuming the data is in the first sheet

            int rowNum = 7; // Starting row for data
            int id = 1;

            for (ReportMidtermPoint point : list) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(id++);
                row.createCell(1).setCellValue(point.getStudentID());
                row.createCell(2).setCellValue(point.getInternID());
                row.createCell(3).setCellValue(point.getInternName());
                row.createCell(4).setCellValue(point.getDivision());
                row.createCell(5).setCellValue(point.getMentorName());
                // Radio button values
                row.createCell(6).setCellValue(point.getEvaluate().equals("Excellent") ? "Excellent" : "");
                row.createCell(7).setCellValue(point.getEvaluate().equals("Very good") ? "Very good" : "");
                row.createCell(8).setCellValue(point.getEvaluate().equals("Good") ? "Good" : "");
                row.createCell(9).setCellValue(point.getEvaluate().equals("Average") ? "Average" : "");
                row.createCell(10).setCellValue(point.getEvaluate().equals("Poor") ? "Poor" : "");
                row.createCell(11).setCellValue(point.getComment());
            }

            fileInputStream.close(); // Close the input stream before writing the output
            fileOutputStream = new FileOutputStream(uploadPath);
            workbook.write(fileOutputStream);

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=\"ReportMidterm.xlsx\"");
            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
