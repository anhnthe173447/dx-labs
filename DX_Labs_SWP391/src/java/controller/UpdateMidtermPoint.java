/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.MidtermPointDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.MidtermPoint;

/**
 *
 * @author admin
 */
public class UpdateMidtermPoint extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");

        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        String internID = request.getParameter("id");
        //ép kiểu
        int idIntern = Integer.parseInt(internID);

        MidtermPointDAO mdao = new MidtermPointDAO();
        MidtermPoint midtermPoint = mdao.getMidtermPointByInternID(idIntern);

        request.setAttribute("MidtermPoint", midtermPoint);
        request.getRequestDispatcher("UpdateMidtermPoint.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String internID = request.getParameter("internID");
        //ép kiểu
        int idIntern = Integer.parseInt(internID);
        
        float majorPoints = Float.parseFloat(request.getParameter("majorPoints"));
        float softSkillPoints = Float.parseFloat(request.getParameter("softSkillPoints"));
        float attitudePoints = Float.parseFloat(request.getParameter("attitudePoints"));
        String comment = request.getParameter("comment");

        MidtermPoint midtermPoint = new MidtermPoint();
        midtermPoint.setInternID(idIntern);
        midtermPoint.setMajorPoints(majorPoints);
        midtermPoint.setSoftSkillPoints(softSkillPoints);
        midtermPoint.setAttitudePoints(attitudePoints);
        midtermPoint.setComment(comment);

        MidtermPointDAO mdao = new MidtermPointDAO();
        boolean updated = mdao.updateMidtermPoint(midtermPoint);
        if (updated) {
            request.setAttribute("message", "Beginning stage updated successfully!");
        } else {
            request.setAttribute("message", "No beginning stage found for intern ID: " + internID);
        }

        response.sendRedirect("DetailMidtermPoint_Mentor?id=" + internID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
