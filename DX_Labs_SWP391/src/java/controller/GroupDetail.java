package controller;

import DAL.InternDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Accounts;
import models.Interns;
import models.Student;

/**
 *
 * @author 84989
 */
public class GroupDetail extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String group_id_raw = request.getParameter("group_id");
        InternDAO internDAO = new InternDAO();

        int groupId = Integer.parseInt(group_id_raw);
        boolean hasLeader = internDAO.hasLeader(groupId);
        List<Interns> interns = internDAO.getInternByIdGroup(groupId);
        List<Student> students = internDAO.getAllStudent();

        request.setAttribute("groupId", groupId);
        request.setAttribute("interns", interns);
        request.setAttribute("students", students);
        request.setAttribute("hasLeader", hasLeader);

        request.getRequestDispatcher("GroupDetails.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String action = request.getParameter("action");
        String internIdRaw = request.getParameter("internId");
        String groupIdRaw = request.getParameter("groupId");
        int internId = Integer.parseInt(internIdRaw);
        int groupId = Integer.parseInt(groupIdRaw);

        InternDAO internDAO = new InternDAO();
        boolean hasLeader = internDAO.hasLeader(groupId);
        boolean success = false;
        String message = "";
        //check case set leader
        if ("setLeader".equals(action)) {
            if (!hasLeader) {
                success = internDAO.setLeader(internId);
                if (success) {
                    message = "Intern set as leader successfully.";
                    hasLeader = true; 
                } else {
                    message = "Failed to set intern as leader.";
                }
            } else {
                message = "Group already has a leader.";
            }
            
        }
        //check case remove leader
        else if ("removeLeader".equals(action)) {
            success = internDAO.removeLeader(internId);
            if (success) {
                message = "Leader  removed successfully.";
                hasLeader = false; 
            } else {
                message = "Failed to remove leader role.";
            }
        }

        List<Interns> interns = internDAO.getInternByIdGroup(groupId);
        List<Student> students = internDAO.getAllStudent();
        request.setAttribute("groupId", groupId);
        request.setAttribute("interns", interns);
        request.setAttribute("students", students);
        request.setAttribute("hasLeader", hasLeader);
        request.setAttribute("message", message);

        request.getRequestDispatcher("GroupDetails.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage group details";
    }
}