/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOAccount;
import DAL.DAOIntern;
import DAL.DAOStudent;
import DAL.MentorDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;
import models.Interns;
import models.Mentor;
import models.Students;

/**
 *
 * @author ADMIN
 */
public class SearchInternByMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchInternByMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchInternByMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String search = request.getParameter("search");
        String searchID = request.getParameter("searchID");
        DAOIntern dao = new DAOIntern();
        DAOStudent daoS = new DAOStudent();
        DAOAccount daoA = new DAOAccount();
        MentorDAO daoM= new MentorDAO();
        HttpSession session = request.getSession();
        Accounts accountMentor = (Accounts) session.getAttribute("acc");

        if (search.isBlank()) {
            List<Interns> list = dao.getAllIntern();
            List<Students> listS = new ArrayList<>();
            List<Accounts> listA = new ArrayList<>();
            for (Interns interns : list) {
                String idStudent = interns.getIdStudent();
                String idACC = interns.getUserID() + "";
                Students s = daoS.getStudentByID(idStudent);
                listS.add(s);
                Accounts a = daoA.getAccountByID(idACC);
                listA.add(a);
            }
            request.setAttribute("error", "Please do not leave the search field blank");
            request.setAttribute("listA", listA);
            request.setAttribute("listS", listS);
            request.setAttribute("listI", list);
            request.setAttribute("search", search);
            request.setAttribute("searchID", searchID);
            request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
        }

        if (searchID.equals("1")) {
            List<Interns> listI = dao.SearchInternByNameByMentor(search,accountMentor.getID());
            List<Students> listS = new ArrayList<>();
            List<Accounts> listA = new ArrayList<>();
            for (Interns interns : listI) {
                String idStudent = interns.getIdStudent();
                String idACC = interns.getUserID() + "";
                Students s = daoS.getStudentByID(idStudent);
                listS.add(s);
                Accounts a = daoA.getAccountByID(idACC);
                listA.add(a);
            }
            if (listI.isEmpty()) {
                request.setAttribute("error", "Not found name in intern list");
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            } else {
                request.setAttribute("listI", listI);
                request.setAttribute("listA", listA);
                request.setAttribute("listS", listS);
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            }

        } else if (searchID.equals("3")) {
            List<Interns> listI = dao.searchInternByInternIdByMentor(search,accountMentor.getID());
            List<Students> listS = new ArrayList<>();
            List<Accounts> listA = new ArrayList<>();
            for (Interns interns : listI) {
                String idStudent = interns.getIdStudent();
                String idACC = interns.getUserID() + "";
                Students s = daoS.getStudentByID(idStudent);
                listS.add(s);
                Accounts a = daoA.getAccountByID(idACC);
                listA.add(a);
            }
            if (listI.isEmpty()) {
                request.setAttribute("error", "Not found Intern ID in intern list");
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            } else {
                request.setAttribute("listI", listI);
                request.setAttribute("listA", listA);
                request.setAttribute("listS", listS);
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            }
        } else if (searchID.equals("2")) {
            List<Interns> listI = dao.SearchInternByStudentIDByMentor(search,accountMentor.getID());
            List<Students> listS = new ArrayList<>();
            List<Accounts> listA = new ArrayList<>();
            for (Interns interns : listI) {
                String idStudent = interns.getIdStudent();
                String idACC = interns.getUserID() + "";
                Students s = daoS.getStudentByID(idStudent);
                listS.add(s);
                Accounts a = daoA.getAccountByID(idACC);
                listA.add(a);
            }
            if (listI.isEmpty()) {
                request.setAttribute("error", "Not found Student ID in intern list");
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            } else {
                request.setAttribute("listI", listI);
                request.setAttribute("listA", listA);
                request.setAttribute("listS", listS);
                request.setAttribute("searchID", searchID);
                request.setAttribute("search", search);
                request.getRequestDispatcher("ManagerInternByMentor.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
