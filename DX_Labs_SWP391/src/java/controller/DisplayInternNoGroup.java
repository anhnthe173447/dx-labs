package controller;

import DAL.InternDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import models.Accounts;
import models.Interns;
import models.Student;

public class DisplayInternNoGroup extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String group_id_raw = request.getParameter("groupId");
        InternDAO internDAO = new InternDAO();
        int groupId = Integer.parseInt(group_id_raw);
        List<Interns> interns = internDAO.getInternNoGroup();
        List<Student> students = internDAO.getAllStudent();
        request.setAttribute("groupId", groupId);
        request.setAttribute("interns", interns);
        request.setAttribute("students", students);
        request.getRequestDispatcher("DisplayToInternNoGroup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String group_id_raw = request.getParameter("groupId");
        String studentId = request.getParameter("studentId");

        InternDAO internDAO = new InternDAO();
        int groupId = Integer.parseInt(group_id_raw);

        List<Interns> interns = new ArrayList<>();
        String mess = null;

        if (studentId != null && !studentId.trim().isEmpty()) {
            studentId = studentId.trim().replaceAll("\\s+", " ");
            interns = internDAO.searchInternsNoGroupByStudentID(studentId);
            if (interns == null || interns.isEmpty()) {
                mess = "No results found for Student ID: " + studentId;
            }
            request.setAttribute("studentId", studentId);

        } else {
            mess = "Please input Student ID.";
            interns = internDAO.getInternNoGroup();
        }

        List<Student> students = internDAO.getAllStudent();
        request.setAttribute("groupId", groupId);
        request.setAttribute("interns", interns);
        request.setAttribute("students", students);
        if (mess != null) {
            request.setAttribute("mess", mess);
        }

        request.getRequestDispatcher("DisplayToInternNoGroup.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
