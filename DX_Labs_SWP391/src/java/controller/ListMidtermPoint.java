package controller;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import DAL.MidtermPointDAO;
import DAL.ReportDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import java.util.logging.Logger;
import models.Accounts;
import models.MidtermPoint;
import models.ReportMidtermPoint;

/**
 *
 * @author admin
 */
public class ListMidtermPoint extends HttpServlet {

    private static final Logger logger = Logger.getLogger(ListMidtermPoint.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListMidtermPoint</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListBeginningPoint at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        MidtermPointDAO b = new MidtermPointDAO();
        ReportDAO daoR = new ReportDAO();
        List< MidtermPoint> MidtermPList = b.getListMidtermPoint(u.getID());
        List<ReportMidtermPoint> reportMidtermList = new ArrayList<>();
        ReportMidtermPoint report = new ReportMidtermPoint("0", "0", "0", "0", "0", "0", "0");

        // Log dữ liệu để kiểm tra
        for (MidtermPoint mp : MidtermPList) {
            ReportMidtermPoint rp = daoR.getReportMidtermByInternById(mp.getInternID() + "");
            if (!daoR.checkDuplicateReport(mp.getInternID())) {
                reportMidtermList.add(rp);
            } else {
                reportMidtermList.add(report);
            }
//            logger.info(mp.toString());
        }
        request.setAttribute("report", reportMidtermList);
        request.setAttribute("MidtermPList", MidtermPList);
        request.getRequestDispatcher("ListMidtermP_Mentor.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
