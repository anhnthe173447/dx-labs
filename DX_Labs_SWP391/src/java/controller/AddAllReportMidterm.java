/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.MidtermPointDAO;
import DAL.ReportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Accounts;
import models.MidtermPoint;
import models.ReportMidtermPoint;

/**
 *
 * @author ADMIN
 */
public class AddAllReportMidterm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        MidtermPointDAO b = new MidtermPointDAO();
        ReportDAO daoR = new ReportDAO();
        List<MidtermPoint> MidtermPList = b.getListMidtermPoint(u.getID());
        List<ReportMidtermPoint> reportMidtermList = daoR.getListReportMidtern();

        for (MidtermPoint midtermPoint : MidtermPList) {
            if (daoR.checkDuplicateReport(midtermPoint.getInternID())) {
                float AveragePoint = midtermPoint.getAttitudePoints() + midtermPoint.getMajorPoints() + midtermPoint.getSoftSkillPoints();
                String rating = null;
                if (0 <= AveragePoint && AveragePoint <= 6) {
                    rating = "Poor";
                } else if (7 <= AveragePoint && AveragePoint <= 12) {
                    rating = "Average";
                } else if (13 <= AveragePoint && AveragePoint <= 18) {
                    rating = "Good";
                } else if (19 <= AveragePoint && AveragePoint <= 24) {
                    rating = "VeryGood";
                } else if (25 <= AveragePoint && AveragePoint <= 30) {
                    rating = "Excellent";
                }
                daoR.AddReportMidterm(midtermPoint.getMidtermID(), midtermPoint.getComment(), rating);
            }
        }
        request.setAttribute("mess", "Add to report midterm successfully");
        request.getRequestDispatcher("ListMidtermPoint").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
