/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import DAL.DAOToken;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.TokenForgotPassword;
import ulti.Email;

/**
 *
 * @author admin
 */
@WebServlet(name = "resetPassword", urlPatterns = {"/resetPassword"})
public class resetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet resetPassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet resetPassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String token = request.getParameter("token");
        Email service = new Email();
        DAOToken daoT = new DAOToken();
        HttpSession session = request.getSession();
        if (token != null) {

            TokenForgotPassword tokensFP = daoT.getTokenPassword(token);
            if (tokensFP == null) {
                request.setAttribute("mess", "token invalid");
                request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            }
            if (tokensFP.isIsUsed()) {
                request.setAttribute("mess", "token is used");
                request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            }
            if (service.isExpireTime(tokensFP.getExpiryTime())) {
                request.setAttribute("mess", "Time of this token have ended");
                request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            }
            Accounts account = daoT.getAccountByUserID(tokensFP.getUserID());
            request.setAttribute("email", account.getEmail());
            session.setAttribute("token", tokensFP.getToken());
            request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
            return; // Add return here
        }
        request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOToken DAOToken = new DAOToken();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String cfpassword = request.getParameter("confirm_password");
        if (!password.equals(cfpassword)) {
            request.setAttribute("mess", "confirm password must same password");
            request.setAttribute("email", email);
            request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
            return;
        } 
//        else if (password.equals(cfpassword) || password.length() <= 6 && cfpassword.length() <= 6) {
//            request.setAttribute("mess", "New password must be more than 6 characters");
//            request.setAttribute("email", email);
//            request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
//            return;
//        }
        HttpSession session = request.getSession();
        String tokenStr = (String) session.getAttribute("token");
        TokenForgotPassword tokenForgotPassword = DAOToken.getTokenPassword(tokenStr);
        Email service = new Email();
        if (tokenForgotPassword == null) {
            request.setAttribute("mess", "token invalid");
            request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            return;
        }
        if (tokenForgotPassword.isIsUsed()) {
            request.setAttribute("mess", "token is used");
            request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            return;
        }
        if (service.isExpireTime(tokenForgotPassword.getExpiryTime())) {
            request.setAttribute("mess", "token is expiry time");
            request.getRequestDispatcher("requestPassword.jsp").forward(request, response);
            return;
        }

        //update is used of token
        tokenForgotPassword.setToken(tokenStr);
        tokenForgotPassword.setIsUsed(true);

        DAOToken.updatePassword(tokenForgotPassword.getUserID(), password);
        DAOToken.updateStatus(tokenForgotPassword);

        //save user in session and redirect to home
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
