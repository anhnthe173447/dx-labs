package controller;

import DAL.GetGroupDAO;
import DAL.InternDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.Group;

public class AddGroup extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("AddGroup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        InternDAO internDAO = new InternDAO();
        GetGroupDAO getGroupDAO = new GetGroupDAO();

        String id_group_raw = request.getParameter("group_id");
        String group_name = request.getParameter("group_name");
        String id_mentor_raw = request.getParameter("mentor_id");

        // Initialize variables for error handling and redirection
        String error = null;
        String redirectURL = null;

        // Check if id_group, group_name, or id_mentor are null or empty
        if (id_group_raw == null || group_name == null || id_mentor_raw == null
                || id_group_raw.trim().isEmpty() || group_name.trim().isEmpty() || id_mentor_raw.trim().isEmpty()) {
            error = "All fields are required and cannot be empty!";
            redirectURL = String.format("AddGroup.jsp?mentor_id=%s&newGroupId=%s", id_mentor_raw, id_group_raw);
        } else {
            try {
                int id_group = Integer.parseInt(id_group_raw);
                int mentor_id = Integer.parseInt(id_mentor_raw);

                // Check if group name exists for the given mentor_id
                boolean isGroupNameExists = getGroupDAO.groupNameExists(group_name, mentor_id);

                if (isGroupNameExists) {
                    error = "Group name '" + group_name + "' already exists for this mentor.";
                } else if (!group_name.matches("[a-zA-Z0-9 ]+")) {
                    error = "Group name can only contain letters and numbers.";
                } else {

                    Group group = new Group(id_group, group_name, mentor_id);
                    internDAO.insert(group);
                    response.sendRedirect("displayGroup");
                    return;
                }

                redirectURL = String.format("AddGroup.jsp?mentor_id=%d&newGroupId=%d", mentor_id, id_group);

            } catch (NumberFormatException e) {
                error = "Group ID and Mentor ID must be numbers!";
                redirectURL = String.format("AddGroup.jsp?mentor_id=%s&newGroupId=%s", id_mentor_raw, id_group_raw);
            } catch (Exception e) {
                error = "An error occurred while processing your request.";
                redirectURL = String.format("AddGroup.jsp?mentor_id=%s&newGroupId=%s", id_mentor_raw, id_group_raw);
            }
        }

        // Set the error attribute and redirect to the AddGroup.jsp page with parameters
        if (error != null && redirectURL != null) {
            request.setAttribute("error", error);
            request.getRequestDispatcher(redirectURL).forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Servlet for adding a new group";
    }
}
