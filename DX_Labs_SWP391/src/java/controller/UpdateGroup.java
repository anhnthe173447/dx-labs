package controller;

import DAL.GetGroupDAO;
import DAL.InternDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Accounts;
import models.Group;

public class UpdateGroup extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String group_id_raw = request.getParameter("group_id");
        InternDAO internDAO = new InternDAO();

        int group_id;
        try {
            group_id = Integer.parseInt(group_id_raw);
            Group group = internDAO.getGroupById(group_id);
            request.setAttribute("group", group);
            request.getRequestDispatcher("UpdateGroup.jsp").forward(request, response);

        } catch (NumberFormatException e) {

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        GetGroupDAO groupDAO = new GetGroupDAO();
        InternDAO internDAO = new InternDAO();
        HttpSession session = request.getSession();
        Accounts u = (Accounts) session.getAttribute("acc");
        if (u == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        String group_id_raw = request.getParameter("group_id");
        String group_name = request.getParameter("group_name");
        String mentor_id_raw = request.getParameter("mentor_id");
        int group_id = Integer.parseInt(group_id_raw);

        String error = null;

        // Check if parameters are null or empty
        if (group_id_raw == null || group_name == null || mentor_id_raw == null
                || group_id_raw.trim().isEmpty() || group_name.trim().isEmpty() || mentor_id_raw.trim().isEmpty()) {
            error = "All fields are required and cannot be empty!";
        } else {
            try {

                int mentor_id = Integer.parseInt(mentor_id_raw);

                // Check if group_name exists for the same mentor_id
                boolean isGroupNameExistsForMentor = groupDAO.groupNameExists(group_name, mentor_id);

                if (isGroupNameExistsForMentor) {
                    error = "Group Name '" + group_name + "' already exists for this mentor.";
                } else {
                    if (!group_name.matches("[a-zA-Z0-9 ]+")) {
                        error = "Group name can only contain letters and numbers.";
                    } else {
                        // Create and update the Group
                        Group group = new Group(group_id, group_name, mentor_id);
                        internDAO.updateGroup(group);
                        response.sendRedirect("displayGroup");
                        return;
                    }
                }
            } catch (NumberFormatException e) {
                error = "Group ID and Mentor ID must be numbers!";
            } catch (Exception e) {
                error = "An error occurred while processing your request.";
            }
        }

        // Forward back to the form with the error message
        session.setAttribute("error", error);
        request.setAttribute("group_id", group_id_raw);
        request.setAttribute("mentor_id", mentor_id_raw);
        request.setAttribute("group_name", group_name);

        response.sendRedirect("updateGroup?group_id=" + group_id);
    }

    @Override
    public String getServletInfo() {
        return "Servlet for updating a group";
    }
}
