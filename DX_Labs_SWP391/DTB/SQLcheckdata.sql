﻿--B1
insert into Roles(Role_ID, RoleName, User_id)
values (1, 'Mentor', 1),
		(2,'Intern', 2);
--insert data

select *
from Roles -- check data
---------------------------------------------------------------------------------
--B2
Select *
from Account;-- check data

insert into Account(User_ID, UserName, Password, Role_ID)
values (1, 'Mentor1', 1, 1),
		(2, 'Intern1', 1, 2);

		--insert data
--------------------------------------------------------------------------------------
--B3
select * 
from Mentor;-- check data

insert into Mentor(Mentor_ID, phone_number, Full_name, Email, Account_ID)
values (1, 012346789, 'Nguyễn Văn A', 'abcxyz@gmail.com', 1);
--insert data
------------------------------------------------------------------------------
Set IDENTITY_INSERT Announcement ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT Announcement OFF;

select * 
from Announcement;-- check data

insert into Announcement(Announcement_ID, Announcement_content, Mentor_ID)
values (1, 'thực tập sinh được nghỉ vào ngày Giỗ Tổ Hùng Vương', 1),
		(2, 'Task mới được giao vào ngày 6/4/2024', 1);
		--insert data

-----------------------------------------------------------------------------------
select *  
from Task;-- check data

insert into Task(task_id, Mentor_id, Group_ID, task_name, Description, task_deadline,  status_id, Task_comment)
values			(1, 1, 1, 'Làm quen với API', 'task này làm về API', '1999/02/01', 1 , 'task này giúp các em làm quen về API'),
				(2,1,1, 'Làm quen với Google','task này làm về lập trình nhúng GG','2024-10-1',2,'task này giúp các em làm quen về lập trình nhúng GG')
				--insert data
-------------------------------------------------------------------------------------------------------
select *
from [dbo].[Group];-- check data

insert into [dbo].[Group](Group_ID, Group_Name)
values
						(1, 'Nhóm 1'),
						(2, 'Nhóm 2');
						--insert data
--------------------------------------------------------------------------------------------------------
select *
from semester-- check data
Set IDENTITY_INSERT semester ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT semester OFF;

insert into semester(Semester_id, Semester_name, Start_date, End_date, Mentor_id)
values				(1, 'Spring24', '2024-1-1', '2024-4-30', 1),
					(2, 'Summer24', '2024-5-1', '2024-8-31', 1),
					(3, 'Fall24', '2024-9-1', '2024-12-31', 1)

					update semester
					set Semester_name ='Spring24',
					Start_date ='2024-1-1',
					End_date ='2024-4-30'
					where Semester_id = 2;
--insert data

SELECT Semester_id FROM Semester WHERE GETDATE() BETWEEN Start_date AND End_date
--------------------------------------------------------------------------------------------------------
select *
from Intern-- check data

Insert into Intern(Intern_ID, User_ID, ID_Student , Group_ID, Semester_id, Status_intern)
values			 
				  ('2',			2,	'S001', 1, 1, 1)
				  --insert data
---------------------------------------------------------------------------------------------------------
select *
from TaskStatus-- check data

Set IDENTITY_INSERT TaskStatus ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT TaskStatus OFF;

insert into TaskStatus(status_id, Status_name)
values (1, 'Chưa xong'),
	   (2,'Đã xong'),
	   (3,'Đang Làm'),
	   (4,'Chậm DL')
	   --insert data
---------------------------------------------------------------------------------------------------------
select *
from Status_intern-- check data

Set IDENTITY_INSERT Status_intern ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT Status_intern OFF;

insert into Status_intern(Status_intern, Status_name)
values (1, 'active'),
		(2, 'unactive')

		UPDATE Status_intern
		SET Status_name = 'unactive'
		WHERE Status_intern = 2;
		--insert data
---------------------------------------------------------------------------------------------------------
select *
from Student-- check data

Insert into Student(Roll_number, FullName, Address, Phone_number, ID_Card, DOB, Major, Company, Job_tile, Link_CV, status_CV)
values 
					('S001', 'Nguyễn Trần Văn A', 'Lào Cai', 0123456789,0000000, '2000-5-5', 'SE', 'FPT', 'kobiet','link',1)
---------------------------------------------------------------------------------------------------------
select *
from Beginning_stage-- check data

Set IDENTITY_INSERT Beginning_stage ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT Beginning_stage OFF;

insert into Beginning_stage(Id_intern, ID_beginning_stage, Major_skills_point, Soft_skill_point, Attitude_point, Comment)
values	('2', 2, null, null, null, null);
--insert data
---------------------------------------------------------------------------------------------------------
select *
from Ending_stage-- check data

Set IDENTITY_INSERT Ending_stage ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT Ending_stage OFF;

insert into Ending_stage(Id_intern, ID_ending_stage, Major_skills_point, Soft_skill_point, Attitude_point, Comment)
values	('1', 1, null, null, null, 'gà');
--insert data
---------------------------------------------------------------------------------------------------------
select * 
from InternshipEvaluations-- check data

insert into InternshipEvaluations(Id_intern, mentor_Id, Comment, ID_beginning_stage, ID_Ending_stage, Final_Result, ID_Semester)
values ('2',1,null,2,2,null,1);
--insert data
---------------------------------------------------------------------------------------------------------
select * 
from CV_status --check data

Set IDENTITY_INSERT CV_status ON;--set về on thì mới insert được dữ liệu
Set IDENTITY_INSERT CV_status OFF;

insert into CV_status(status_cv, Status_name)
values (1, 'link')
--insert data

------------------------------------------------------------------------------------------------------------
--Phần làm riêng của Tuấn Anh
ALTER TABLE Beginning_stage
ADD COLUMN Intern_ID INT,
ADD FOREIGN KEY (Intern_ID) REFERENCES Intern(Id_Intern);

SELECT Student.FullName, Beginning_stage.Major_skills_point, Beginning_stage.Soft_skill_point, 
Beginning_stage.Attitude_point, Beginning_stage.Comment

FROM Beginning_stage JOIN Intern
ON Beginning_stage.Intern_ID = Intern.Intern_ID
JOIN Student 
ON Intern.ID_student = Student.Roll_number


SELECT * FROM Ending_stage WHERE Intern_ID is not null
Select * from Intern

select *
from Intern

insert into Intern(Intern_ID, User_ID, Group_ID, Semester_id, ID_student, Status_intern)
values('UIEHK31245',4,1,1,'UIE123IA',1)

select *
from Student

insert into Student(Roll_number, FullName, Address, Phone_number, ID_Card, DOB,  Major, Job_tile, Link_CV, status_CV)
values('UIE123IA', N'Nguyễn Hoàng Văn C', N'Hà Nội', '0989787656', 012012012, '2000-2-12', 'HK', 'JS', 'link', 1)

select *
from Account

insert into Account(User_ID,Role_ID,Password,UserName,image)
values
					(5,2,1,'Intern4','links')

					select *
					from Ending_stage
					delete from Ending_stage where Intern_ID is null

					select *
					from InternshipEvaluations
					delete from InternshipEvaluations where InternshipEvaluations.Id_intern =2

					UPDATE InternshipEvaluations
					SET Comment = N'học giỏi đấy'
					WHERE Comment = 'học giỏi đấy';

					select *
					from Beginning_stage

					select *
					from Announcement

					DESCRIBE Announcement;

					ALTER TABLE Announcement MODIFY COLUMN Date TIMESTAMP;


					SELECT Announcement_ID, DATE_FORMAT(Date, '%Y-%m-%d %H:%i:%s') AS Date
					FROM Announcement;


					UPDATE Announcement
					SET Date = CURRENT_TIMESTAMP
					WHERE Date is null;

					ALTER TABLE anounncement
					MODIFY COLUMN [dbo].[Date] TIMESTAMP;

					--gọi sinh viên, nối điểm, nối id từng kì

select Intern.Intern_ID, Intern.Semester_id, Beginning_stage.Major_skills_point, Beginning_stage.Soft_skill_point,
	   Beginning_stage.Attitude_point, Beginning_stage.Comment
from Beginning_stage join Intern on Beginning_stage.Id_intern = Intern.Intern_ID






select * from InternshipEvaluations

/*CREATE TRIGGER add_default_point
ON Intern
AFTER INSERT
AS
BEGIN
    INSERT INTO Beginning_stage (Id_intern, ID_beginning_stage, Major_skills_point, Soft_skill_point, Attitude_point, Comment)
    SELECT Intern_ID, NULL, NULL, NULL, NULL, NULL
    FROM inserted;
END;*/


ALTER TABLE Beginning_stage
ALTER COLUMN Major_skills_point float NULL;


UPDATE Ending_stage
Set Major_skills_point = 4,
Soft_skill_point = 4,
Attitude_point= 4,
Comment =N'Không Tốt'
WHERE Id_intern = 1;



select *
from Beginning_stage

select *
from Intern

update InternshipEvaluations
set Comment ='ok',
Final_Result = 7.2
where InternshipEvaluations.Id_intern = '1'

ALTER TABLE Beginning_stage
ADD CONSTRAINT AutoIncrease beginningID  AUTO_INCREMENT;

ALTER TABLE Ending_stage
ADD CONSTRAINT CHK_Soft_skill_point_E CHECK (Soft_skill_point >= 0 AND Soft_skill_point <= 10);

ALTER TABLE Ending_stage
ADD CONSTRAINT CHK_Attitude_point_E CHECK (Attitude_point >= 0 AND Attitude_point <= 10);

SET IDENTITY_INSERT [Beginning_stage] ON;

INSERT INTO Beginning_stage (Id_intern, Major_skills_point, Soft_skill_point, Attitude_point, Comment)
VALUES ('1', 8.5, 9.5, 10,'ok' )

ALTER TABLE Beginning_stage DROP COLUMN ID_beginning_stage;

ALTER TABLE Beginning_stage ADD ID_beginning_stage INT IDENTITY(1,1);

SELECT MAX(ID_ending_stage) FROM Ending_stage

select *
from semester

SELECT COUNT(*) 
FROM Beginning_stage 
JOIN Ending_stage  ON Beginning_stage.Id_intern = Ending_stage.Id_intern 
WHERE Beginning_stage.Id_intern = '1'

SELECT Semester_id FROM semester WHERE GETDATE() BETWEEN Start_date AND End_date

				INSERT INTO InternshipEvaluations 
					(Id_intern, mentor_Id, comment, ID_beginning_stage, ID_Ending_stage, Final_Result, ID_Semester) 
                SELECT Beginning_stage.Id_intern, ?, ?,Beginning_stage.ID_beginning_stage, Ending_stage.ID_ending_stage, ?, 1 
                FROM Beginning_stage  JOIN Ending_stage  ON Beginning_stage.Id_intern = Ending_stage.Id_intern 
                WHERE Beginning_stage.Id_intern = ?;
				
		select *
		from Beginning_stage

DELETE FROM Ending_stage 

WHERE;

select * from semester

DELETE FROM semester WHERE Semester_id = 1;
select * from semester
				