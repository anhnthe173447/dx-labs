CREATE TABLE Roles (
    Role_ID INT PRIMARY KEY,   -- Khóa chính (Primary Key)
    RoleName NVARCHAR(255),
	[User_id] NVARCHAR(255),

);

CREATE TABLE Account (
    [User_ID] INT PRIMARY KEY,   -- Khóa chính (Primary Key)
    Role_ID INT,               -- ID vai trò (Role ID)
    Password VARCHAR(255),     -- Mật khẩu (Password)
    UserName VARCHAR(255), 
    image NVARCHAR(MAX),
    FOREIGN KEY (Role_ID) REFERENCES Roles(Role_ID)
);

CREATE TABLE Mentor (
    Mentor_ID NVARCHAR(100) PRIMARY KEY,
    phone_number NVARCHAR(15),
    Full_name NVARCHAR(100),
    Email NVARCHAR(225),
    Gender BIT,
    DOB DATE,
    [Address] NVARCHAR(225),
    ID_Card INT,
    Account_ID INT,
    FOREIGN KEY (Account_ID) REFERENCES Account([User_ID])
);

CREATE TABLE CV_status (
    status_cv INT IDENTITY(1,1) PRIMARY KEY,
    Status_name NVARCHAR(255)
);

CREATE TABLE Student (
    Roll_number NVARCHAR(100) PRIMARY KEY,
    FullName NVARCHAR(250),
    [Address] NVARCHAR(MAX),
    Phone_number NVARCHAR(15),
	Email NVARCHAR(MAX),
    ID_Card NVARCHAR(15),
    DOB DATE,
    Major NVARCHAR(225),
    Company NVARCHAR(225),
    Job_tile NVARCHAR(225),
    Link_CV NVARCHAR(225),
    status_CV INT,
    FOREIGN KEY (status_CV) REFERENCES CV_status(status_CV)
);

CREATE TABLE [Group] (
    Group_ID INT PRIMARY KEY,
    Group_Name NVARCHAR(100)
);

CREATE TABLE semester (
    Semester_id INT IDENTITY(1,1) PRIMARY KEY,
    Semester_name NVARCHAR(255),
    Start_date DATE,
    End_date DATE,
    Mentor_id NVARCHAR(100),
    FOREIGN KEY (Mentor_id) REFERENCES Mentor(Mentor_ID)
);

CREATE TABLE Status_intern (
    Status_intern INT IDENTITY(1,1) PRIMARY KEY,
    Status_name NVARCHAR(255)
);

CREATE TABLE Intern (
    Intern_ID NVARCHAR(100) PRIMARY KEY,
    [User_ID] INT,
    ID_student NVARCHAR(100),
    Group_ID INT,
    Semester_id INT,
    Status_intern INT,
    FOREIGN KEY ([User_ID]) REFERENCES Account([User_ID]),
    FOREIGN KEY (Group_ID) REFERENCES [Group](Group_ID),
    FOREIGN KEY (ID_student) REFERENCES Student(Roll_number),
    FOREIGN KEY (Semester_id) REFERENCES semester(Semester_id),
    FOREIGN KEY (Status_intern) REFERENCES Status_intern(Status_intern)
);

CREATE TABLE Announcement (
    Announcement_ID INT IDENTITY(1,1),
    Announcement_content NVARCHAR(MAX),
    [Description] NVARCHAR(MAX),
    Mentor_ID NVARCHAR(100),
    [Date] DATE,
    FOREIGN KEY (Mentor_ID) REFERENCES Mentor(Mentor_ID)
);

CREATE TABLE TaskStatus (
    status_id INT IDENTITY(1,1) PRIMARY KEY,
    Status_name NVARCHAR(255)
);

CREATE TABLE Task (
    task_id INT IDENTITY(1,1) PRIMARY KEY,
    Mentor_id NVARCHAR(100),
    Group_ID INT,
    task_name NVARCHAR(255),
    [Description] NVARCHAR(MAX),
    task_deadline DATE,
    status_id INT,
    Task_comment NVARCHAR(MAX),
	Active Int,
    FOREIGN KEY (Mentor_id) REFERENCES Mentor(Mentor_id),
    FOREIGN KEY (Group_ID) REFERENCES [Group](Group_ID),
    FOREIGN KEY (status_id) REFERENCES TaskStatus(status_id)
);

CREATE TABLE Beginning_stage (
    ID_beginning_stage INT IDENTITY(1,1) PRIMARY KEY,
    Major_skills_point FLOAT,
    Soft_skill_point FLOAT,
    Attitude_point FLOAT,
    Comment NVARCHAR(MAX)
);

CREATE TABLE Ending_stage (
    ID_ending_stage INT IDENTITY(1,1) PRIMARY KEY,
    Major_skills_point FLOAT,
    Soft_skill_point FLOAT,
    Attitude_point FLOAT,
    Comment NVARCHAR(MAX)
);

CREATE TABLE InternshipEvaluations (
    Id_intern NVARCHAR(100),
    mentor_Id NVARCHAR(100),
    Comment NVARCHAR(MAX),
    ID_beginning_stage INT,
    ID_Ending_stage INT,
    Final_Result FLOAT,
    ID_Semester INT,
    FOREIGN KEY (Id_intern) REFERENCES Intern(Intern_ID),
    FOREIGN KEY (mentor_Id) REFERENCES Mentor(Mentor_ID),
    FOREIGN KEY (ID_beginning_stage) REFERENCES Beginning(ID_beginning_stage),
    FOREIGN KEY (ID_Ending_stage) REFERENCES Ending_stage(ID_ending_stage),
    FOREIGN KEY (ID_Semester) REFERENCES semester(Semester_id)
);
