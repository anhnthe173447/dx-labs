USE [master]
GO
/****** Object:  Database [DXLab4]    Script Date: 6/8/2024 12:45:49 AM ******/
CREATE DATABASE [DXLab4]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DXLab4', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\DXLab4.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DXLab4_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\DXLab4_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [DXLab4] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DXLab4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DXLab4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DXLab4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DXLab4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DXLab4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DXLab4] SET ARITHABORT OFF 
GO
ALTER DATABASE [DXLab4] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DXLab4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DXLab4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DXLab4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DXLab4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DXLab4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DXLab4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DXLab4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DXLab4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DXLab4] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DXLab4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DXLab4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DXLab4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DXLab4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DXLab4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DXLab4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DXLab4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DXLab4] SET RECOVERY FULL 
GO
ALTER DATABASE [DXLab4] SET  MULTI_USER 
GO
ALTER DATABASE [DXLab4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DXLab4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DXLab4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DXLab4] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DXLab4] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DXLab4] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'DXLab4', N'ON'
GO
ALTER DATABASE [DXLab4] SET QUERY_STORE = OFF
GO
USE [DXLab4]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[User_ID] [int] NOT NULL,
	[Role_ID] [int] NULL,
	[Password] [varchar](255) NULL,
	[UserName] [varchar](255) NULL,
	[image] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Announcement](
	[Announcement_ID] [int] IDENTITY(1,1) NOT NULL,
	[Announcement_content] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Mentor_ID] [nvarchar](100) NULL,
	[Date] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Beginning_stage]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_stage](
	[Id_intern] [nvarchar](100) NULL,
	[ID_beginning_stage] [int] IDENTITY(1,1) NOT NULL,
	[Major_skills_point] [float] NULL,
	[Soft_skill_point] [float] NULL,
	[Attitude_point] [float] NULL,
	[Comment] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_beginning_stage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CV_status]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CV_status](
	[status_cv] [int] IDENTITY(1,1) NOT NULL,
	[Status_name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[status_cv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ending_stage]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ending_stage](
	[Id_intern] [nvarchar](100) NULL,
	[ID_ending_stage] [int] IDENTITY(1,1) NOT NULL,
	[Major_skills_point] [float] NULL,
	[Soft_skill_point] [float] NULL,
	[Attitude_point] [float] NULL,
	[Comment] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_ending_stage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[Group_ID] [int] NOT NULL,
	[Group_Name] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Group_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Intern]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Intern](
	[Intern_ID] [nvarchar](100) NOT NULL,
	[User_ID] [int] NULL,
	[ID_student] [nvarchar](100) NULL,
	[Group_ID] [int] NULL,
	[Semester_id] [int] NULL,
	[Status_intern] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Intern_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InternshipEvaluations]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InternshipEvaluations](
	[Id_intern] [nvarchar](100) NULL,
	[mentor_Id] [nvarchar](100) NULL,
	[Comment] [nvarchar](max) NULL,
	[ID_beginning_stage] [int] NULL,
	[ID_Ending_stage] [int] NULL,
	[Final_Result] [float] NULL,
	[ID_Semester] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mentor]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mentor](
	[Mentor_ID] [nvarchar](100) NOT NULL,
	[phone_number] [nvarchar](15) NULL,
	[Full_name] [nvarchar](100) NULL,
	[Email] [nvarchar](225) NULL,
	[Gender] [bit] NULL,
	[DOB] [date] NULL,
	[Address] [nvarchar](225) NULL,
	[ID_Card] [int] NULL,
	[Account_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Mentor_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Role_ID] [int] NOT NULL,
	[RoleName] [nvarchar](255) NULL,
	[User_id] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Role_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[semester]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[semester](
	[Semester_id] [int] IDENTITY(1,1) NOT NULL,
	[Semester_name] [nvarchar](255) NULL,
	[Start_date] [date] NULL,
	[End_date] [date] NULL,
	[Mentor_id] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Semester_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status_intern]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status_intern](
	[Status_intern] [int] IDENTITY(1,1) NOT NULL,
	[Status_name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Status_intern] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[Roll_number] [nvarchar](100) NOT NULL,
	[FullName] [nvarchar](250) NULL,
	[Address] [nvarchar](max) NULL,
	[Phone_number] [nvarchar](15) NULL,
	[Email] [nvarchar](max) NULL,
	[ID_Card] [nvarchar](15) NULL,
	[DOB] [date] NULL,
	[Major] [nvarchar](225) NULL,
	[Company] [nvarchar](225) NULL,
	[Job_tile] [nvarchar](225) NULL,
	[Link_CV] [nvarchar](225) NULL,
	[status_CV] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Roll_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Task]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task](
	[task_id] [int] IDENTITY(1,1) NOT NULL,
	[Mentor_id] [nvarchar](100) NULL,
	[Group_ID] [int] NULL,
	[task_name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[task_deadline] [date] NULL,
	[status_id] [int] NULL,
	[Task_comment] [nvarchar](max) NULL,
	[Active] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[task_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskStatus]    Script Date: 6/8/2024 12:45:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskStatus](
	[status_id] [int] IDENTITY(1,1) NOT NULL,
	[Status_name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([User_ID], [Role_ID], [Password], [UserName], [image]) VALUES (1, 1, N'1', N'Mentor', NULL)
GO
INSERT [dbo].[Account] ([User_ID], [Role_ID], [Password], [UserName], [image]) VALUES (2, 2, N'1', N'Intern', NULL)
GO
SET IDENTITY_INSERT [dbo].[Announcement] ON 
GO
INSERT [dbo].[Announcement] ([Announcement_ID], [Announcement_content], [Description], [Mentor_ID], [Date]) VALUES (1, N'Thuc tap dang nghi ngay gio to hinh vuong', N'Chi tiet hon', N'1', CAST(N'2024-05-31' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Announcement] OFF
GO
SET IDENTITY_INSERT [dbo].[Beginning_stage] ON 
GO
INSERT [dbo].[Beginning_stage] ([Id_intern], [ID_beginning_stage], [Major_skills_point], [Soft_skill_point], [Attitude_point], [Comment]) VALUES (N'1', 1, 10, 8, 8, N'tot')
GO
SET IDENTITY_INSERT [dbo].[Beginning_stage] OFF
GO
SET IDENTITY_INSERT [dbo].[CV_status] ON 
GO
INSERT [dbo].[CV_status] ([status_cv], [Status_name]) VALUES (1, N'Pass')
GO
INSERT [dbo].[CV_status] ([status_cv], [Status_name]) VALUES (2, N'Not Pass')
GO
SET IDENTITY_INSERT [dbo].[CV_status] OFF
GO
SET IDENTITY_INSERT [dbo].[Ending_stage] ON 
GO
INSERT [dbo].[Ending_stage] ([Id_intern], [ID_ending_stage], [Major_skills_point], [Soft_skill_point], [Attitude_point], [Comment]) VALUES (N'1', 1, 10, 8, 8, N'tot')
GO
SET IDENTITY_INSERT [dbo].[Ending_stage] OFF
GO
INSERT [dbo].[Group] ([Group_ID], [Group_Name]) VALUES (1, N'Nhom 1')
GO
INSERT [dbo].[Group] ([Group_ID], [Group_Name]) VALUES (2, N'Nhom 2')
GO
INSERT [dbo].[Intern] ([Intern_ID], [User_ID], [ID_student], [Group_ID], [Semester_id], [Status_intern]) VALUES (N'1', 2, N'HE170470', 1, 1, 1)
GO
INSERT [dbo].[InternshipEvaluations] ([Id_intern], [mentor_Id], [Comment], [ID_beginning_stage], [ID_Ending_stage], [Final_Result], [ID_Semester]) VALUES (N'1', N'1', N'xuat sac', 1, 1, 8.9, 1)
GO
INSERT [dbo].[Mentor] ([Mentor_ID], [phone_number], [Full_name], [Email], [Gender], [DOB], [Address], [ID_Card], [Account_ID]) VALUES (N'1', N'0998889390', N'Hoang Van A', N'Ajm@gmail.com', 1, CAST(N'1990-01-12' AS Date), N'Ha Noi', 12313234, 1)
GO
INSERT [dbo].[Roles] ([Role_ID], [RoleName], [User_id]) VALUES (1, N'Mentor', NULL)
GO
INSERT [dbo].[Roles] ([Role_ID], [RoleName], [User_id]) VALUES (2, N'Intern', NULL)
GO
SET IDENTITY_INSERT [dbo].[semester] ON 
GO
INSERT [dbo].[semester] ([Semester_id], [Semester_name], [Start_date], [End_date], [Mentor_id]) VALUES (1, N'Summer', CAST(N'2023-03-12' AS Date), CAST(N'2023-06-12' AS Date), N'1')
GO
SET IDENTITY_INSERT [dbo].[semester] OFF
GO
SET IDENTITY_INSERT [dbo].[Status_intern] ON 
GO
INSERT [dbo].[Status_intern] ([Status_intern], [Status_name]) VALUES (1, N'Active')
GO
INSERT [dbo].[Status_intern] ([Status_intern], [Status_name]) VALUES (2, N'Completed')
GO
INSERT [dbo].[Status_intern] ([Status_intern], [Status_name]) VALUES (3, N'Pading')
GO
SET IDENTITY_INSERT [dbo].[Status_intern] OFF
GO
INSERT [dbo].[Student] ([Roll_number], [FullName], [Address], [Phone_number], [Email], [ID_Card], [DOB], [Major], [Company], [Job_tile], [Link_CV], [status_CV]) VALUES (N'HE170470', N'Hoang Van A', N'Ha Noi', N'0985972272', N'ama@gmail.com', N'123123124', CAST(N'2003-08-20' AS Date), N'CNTT', N'LAB', N'intern', N'link', 1)
GO
INSERT [dbo].[Student] ([Roll_number], [FullName], [Address], [Phone_number], [Email], [ID_Card], [DOB], [Major], [Company], [Job_tile], [Link_CV], [status_CV]) VALUES (N'HS1213', N'Nguyen Van B', N'Ha Noi', N'0892782891', N'mm@gmail.com', N'222324', CAST(N'2003-09-12' AS Date), N'CNTT', N'LAB', N'intern', N'link', 1)
GO
SET IDENTITY_INSERT [dbo].[Task] ON 
GO
INSERT [dbo].[Task] ([task_id], [Mentor_id], [Group_ID], [task_name], [Description], [task_deadline], [status_id], [Task_comment], [Active]) VALUES (1, N'1', 1, N'Code java', N'Tap trung code vao 1 noi dung', CAST(N'2024-12-20' AS Date), 2, N'Chu y', 1)
GO
SET IDENTITY_INSERT [dbo].[Task] OFF
GO
SET IDENTITY_INSERT [dbo].[TaskStatus] ON 
GO
INSERT [dbo].[TaskStatus] ([status_id], [Status_name]) VALUES (1, N'Done')
GO
INSERT [dbo].[TaskStatus] ([status_id], [Status_name]) VALUES (2, N'Doing')
GO
SET IDENTITY_INSERT [dbo].[TaskStatus] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([Role_ID])
REFERENCES [dbo].[Roles] ([Role_ID])
GO
ALTER TABLE [dbo].[Announcement]  WITH CHECK ADD FOREIGN KEY([Mentor_ID])
REFERENCES [dbo].[Mentor] ([Mentor_ID])
GO
ALTER TABLE [dbo].[Beginning_stage]  WITH CHECK ADD FOREIGN KEY([Id_intern])
REFERENCES [dbo].[Intern] ([Intern_ID])
GO
ALTER TABLE [dbo].[Ending_stage]  WITH CHECK ADD FOREIGN KEY([Id_intern])
REFERENCES [dbo].[Intern] ([Intern_ID])
GO
ALTER TABLE [dbo].[Intern]  WITH CHECK ADD FOREIGN KEY([Group_ID])
REFERENCES [dbo].[Group] ([Group_ID])
GO
ALTER TABLE [dbo].[Intern]  WITH CHECK ADD FOREIGN KEY([ID_student])
REFERENCES [dbo].[Student] ([Roll_number])
GO
ALTER TABLE [dbo].[Intern]  WITH CHECK ADD FOREIGN KEY([Semester_id])
REFERENCES [dbo].[semester] ([Semester_id])
GO
ALTER TABLE [dbo].[Intern]  WITH CHECK ADD FOREIGN KEY([Status_intern])
REFERENCES [dbo].[Status_intern] ([Status_intern])
GO
ALTER TABLE [dbo].[Intern]  WITH CHECK ADD FOREIGN KEY([User_ID])
REFERENCES [dbo].[Account] ([User_ID])
GO
ALTER TABLE [dbo].[InternshipEvaluations]  WITH CHECK ADD FOREIGN KEY([ID_beginning_stage])
REFERENCES [dbo].[Beginning_stage] ([ID_beginning_stage])
GO
ALTER TABLE [dbo].[InternshipEvaluations]  WITH CHECK ADD FOREIGN KEY([ID_Ending_stage])
REFERENCES [dbo].[Ending_stage] ([ID_ending_stage])
GO
ALTER TABLE [dbo].[InternshipEvaluations]  WITH CHECK ADD FOREIGN KEY([Id_intern])
REFERENCES [dbo].[Intern] ([Intern_ID])
GO
ALTER TABLE [dbo].[InternshipEvaluations]  WITH CHECK ADD FOREIGN KEY([ID_Semester])
REFERENCES [dbo].[semester] ([Semester_id])
GO
ALTER TABLE [dbo].[InternshipEvaluations]  WITH CHECK ADD FOREIGN KEY([mentor_Id])
REFERENCES [dbo].[Mentor] ([Mentor_ID])
GO
ALTER TABLE [dbo].[Mentor]  WITH CHECK ADD FOREIGN KEY([Account_ID])
REFERENCES [dbo].[Account] ([User_ID])
GO
ALTER TABLE [dbo].[semester]  WITH CHECK ADD FOREIGN KEY([Mentor_id])
REFERENCES [dbo].[Mentor] ([Mentor_ID])
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD FOREIGN KEY([status_CV])
REFERENCES [dbo].[CV_status] ([status_cv])
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD FOREIGN KEY([Group_ID])
REFERENCES [dbo].[Group] ([Group_ID])
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD FOREIGN KEY([Mentor_id])
REFERENCES [dbo].[Mentor] ([Mentor_ID])
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD FOREIGN KEY([status_id])
REFERENCES [dbo].[TaskStatus] ([status_id])
GO
USE [master]
GO
ALTER DATABASE [DXLab4] SET  READ_WRITE 
GO
