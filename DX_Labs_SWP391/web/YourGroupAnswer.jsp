<%-- 
    Document   : YourGroupAnswer
    Created on : Jul 12, 2024, 10:27:56 AM
    Author     : Laptop K1
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>                     
                <th>File Name</th>
                <th>Answer Time</th>
                <th>Group Name</th>                      
            </tr>
            <c:forEach items="${list}" var="s">
                <tr>

                    <td>${s.getContent()}</td>
                    <td>${s.getAnswerTime()}</td>
                    <td>${s.getGroupid()}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
