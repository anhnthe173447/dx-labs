<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mid-term Point Evaluation</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DX LAB</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #e6f7ff;
                color: #333;
                margin: 0;
                padding: 20px;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
            }
            th, td {
                padding: 12px;
                text-align: left;
            }
            th {
                color: black;
                font-weight: bold;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }
            td {
                border-bottom: 1px solid #ddd;
            }
            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }
            .btn-edit:hover {
                background-color: #45a049;
            }
            .btn-delete {
                background-color: #f44336;
                color: white;
            }
            .btn-delete:hover {
                background-color: #da190b;
            }
            .table-responsive {
                overflow-x: auto;
            }
            .modal {
                display: none;
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgb(0, 0, 0);
                background-color: rgba(0, 0, 0, 0.4);
            }
            .modal-content {
                background-color: #fefefe;
                margin: 15% auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }
            .table {
                width: 50%;
            }

            .table th, .table td {
                white-space: nowrap;
            }
        </style>
    </head>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
        <!-- Popper.js -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>

        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel"></div>
            <!-- Header-->
        <jsp:include page="rightandheader.jsp"></jsp:include>
            <!-- Notification User -->
        <c:if test="${sessionScope.notification != null}">
            <div class="alert ${sessionScope.typeNoti} alert-dismissible fade show" role="alert">
                <strong>${sessionScope.notification}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <% 
                session.removeAttribute("notification");
                session.removeAttribute("typeNoti");
            %>
        </c:if>

        <div class="container">
            <h1>Mid-term Point Evaluation</h1>
            <hr>
            <form id="formFilter" action="${contextPath}/midterm-evaluate" method="get">
                Semester:
                <select name="semester_ID" id="semester_ID" onchange="submitCombinedForm()">
                    <option <c:if test="${2 == sesId}">
                            selected
                        </c:if> value="2">This semester</option>
                    <option <c:if test="${-1 == sesId}">
                            selected
                        </c:if> value="-1">All</option>
                    <c:forEach items="${ses}" var="s">
                        <option <c:if test="${s.getSemester_ID() == sesId}">
                                selected
                            </c:if> value="${s.getSemester_ID()}">${s.getSemester_Name()}</option>
                    </c:forEach>
                </select>
            </form>

            <form id="formGroupFilter" action="${contextPath}/midterm-evaluate" method="get">
                Group:
                <select name="group_ID" id="group_ID" onchange="submitCombinedForm()">
                    <option <c:if test="${2 == group_ID}">
                            selected
                        </c:if> value="-1">All
                    </option>
                    <c:forEach items="${group}" var="g">
                        <option <c:if test="${g.getGroup_id() == group_ID}">
                                selected
                            </c:if> value="${g.getGroup_id()}">${g.getGroup_name()}</option>
                    </c:forEach>
                </select>
            </form>

            <div class="table-responsive">

                <form action="midterm-evaluate" method="post">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Intern ID</th>
                                <th>Full Name</th>
                                <th>Group Name</th>
                                <th>Major Skill Point (0-10)</th>
                                <th>Soft Skill Point (0-10)</th>
                                <th>Attitude Point (0-10)</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="intern" items="${interns}">
                                <tr>
                                    <td><a href="javascript:void(0)" 
                                           data-intern-id="${intern.getIntern_ID()}"
                                           data-full-name="${intern.getFullName()}"
                                           data-email="${intern.getEmail()}"
                                           data-phone="${intern.getPhone_number()}"
                                           onclick="showModal(this);">${intern.getIntern_ID()}</a></td>
                                    <td>${intern.getFullName()}</td>
                                    <td>${intern.getGroup_Name()}</td>
                                    <td><input type="number" name="majorSkillPoint_${intern.getIntern_ID()}" step="0.1" min="0" max="10" value="${empty intern.getMajorSkillPoint_Midterm() ? 0 : intern.getMajorSkillPoint_Midterm()}"></td>
                                    <td><input type="number" name="softSkillPoint_${intern.getIntern_ID()}" step="0.1" min="0" max="10" value="${empty intern.getSoftSkillPoint_Midterm() ? 0 : intern.getSoftSkillPoint_Midterm()}"></td>
                                    <td><input type="number" name="attitudePoint_${intern.getIntern_ID()}" step="0.1" min="0" max="10" value="${empty intern.getAttitudePoint_Midterm() ? 0 : intern.getAttitudePoint_Midterm()}"></td>
                                    <td><input type="text" name="comment_${intern.getIntern_ID()}" value="${empty intern.getComment_Midterm() ? '' : intern.getComment_Midterm()}"></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!-- Save Button -->
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary" name="saveButton">Save</button>
                    </div>
                </form>
                <div class="text-center">
                    <a href="${contextPath}/ManagerInternByMentor">Back</a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="internModal" class="modal">
            <div class="modal-content">
                <span id="closeModalBtn" class="close">&times;</span>
                <h2>Intern Details</h2>
                <p><strong>Intern ID:</strong> <span id="modalInternID"></span></p>
                <p><strong>Full Name:</strong> <span id="modalFullName"></span></p>
                <p><strong>Email:</strong> <span id="modalEmail"></span></p>
                <p><strong>Phone:</strong> <span id="modalPhone"></span></p>
                <p><strong>Group Name:</strong> <span id="modalGroupName"></span></p>
                <p><strong>Address:</strong> <span id="modalAddress"></span></p>
                <p><strong>ID Card:</strong> <span id="modalIDCard"></span></p>
                <p><strong>Major:</strong> <span id="modalMajor"></span></p>
                <p><strong>Job Title:</strong> <span id="modalJobTitle"></span></p>
                <p><strong>Company:</strong> <span id="modalCompany"></span></p>
                <p><strong>Link CV:</strong> <span id="modalLinkCV"></span></p>
                <p><strong>DOB:</strong> <span id="modalDob"></span></p>
                <p><strong>Gender:</strong> <span id="modalGender"></span></p>
                <p><strong>Intern Status:</strong> <span id="modalInternStatus"></span></p>
                <p><strong>Group ID:</strong> <span id="modalGroupID"></span></p>
                <p><strong>Term ID:</strong> <span id="modalTermID"></span></p>
            </div>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var selectElement = document.getElementById('semester_ID');
                var options = selectElement.options;
                var currentTime = new Date();
                var currentMonth = currentTime.getMonth() + 1; // JavaScript months are 0-based
                var currentYear = currentTime.getFullYear();

                // Loop through the options to find the one that matches the current semester ID
                for (var i = 0; i < options.length; i++) {
                    var option = options[i];
                    var semesterID = option.value; // Assumes semester ID is a string that can be compared with current time
                    var semesterStartMonth = parseInt(semesterID.split('-')[1]); // Assumes semester ID is in the format 'YYYY-MM'
                    var semesterYear = parseInt(semesterID.split('-')[0]); // Assumes semester ID is in the format 'YYYY-MM'

                    // Adjust these conditions based on how your semester IDs are structured
                    if (semesterYear === currentYear && semesterStartMonth === currentMonth) {
                        option.selected = true;
                        break;
                    }
                }
            });


            if (document.querySelector('.alert'))
            {
                document.querySelectorAll('.alert').forEach(function ($el) {
                    setTimeout(() => {
                        $el.classList.remove('show');
                    }, 3000);
                });
            }

            function submitCombinedForm() {
                var semesterId = document.getElementById('semester_ID').value;
                var groupId = document.getElementById('group_ID').value;
                var url = "${contextPath}/midterm-evaluate?semester_ID=" + semesterId + "&group_ID=" + groupId;
                window.location.href = url;
            }
        </script>

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Other Scripts -->
    </body>
</html>
