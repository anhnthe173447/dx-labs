<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DX LAB - Submission</title>
    
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <style>
        body {
            background-color: #f8f9fa;
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 900px;
            margin: 50px auto;
            padding: 20px;
            background: white;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0,0,0,0.1);
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid #dee2e6;
        }
        th, td {
            padding: 12px;
            text-align: center; /* Center-align table cells */
        }
        th {
            background-color: #e9ecef;
        }
        .submitted {
            color: #28a745;
        }
        .status {
            font-weight: bold;
        }
        .time {
            font-size: 0.9em;
        }
        .download-link {
            color: #007bff;
            text-decoration: none;
        }
        .download-link:hover {
            text-decoration: underline;
        }
        .error {
            color: #dc3545;
            margin-top: 5px;
        }
        .btn-save {
            background-color: #17a2b8;
            color: white;
            padding: 10px 20px;
            font-size: 1em;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            display: block; /* Center the button */
            margin: 20px auto; /* Center the button */
        }
        .btn-save:hover {
            background-color: #138496;
        }
        .btn-back {
            background-color: #6c757d;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            text-align: center;
            display: inline-block;
            margin-top: 10px;
        }
        .btn-back:hover {
            background-color: #5a6268;
        }
        .form-control.comment-box {
            width: 100%;
            height: 150px;
        }
    </style>
</head>
<body>
    <jsp:include page="LeftPanel.jsp"></jsp:include>

    <div id="right-panel" class="right-panel"></div>

    <jsp:include page="rightandheader.jsp"></jsp:include>

    <div class="container">
        <h2 class="text-center mb-4">SUBMISSION</h2>
        <form action="givePoint" method="post">
            <c:if test="${not empty noAnswerMessage}">
                <div class="alert alert-warning">${noAnswerMessage}</div>
                <button type="button" class="btn-back" onclick="window.location.href = 'tasks'">Back</button>
            </c:if>
            <c:if test="${empty noAnswerMessage}">
                <table>
                    <thead>
                        <tr>
                            <th>Group</th>
                            <th>Submission Time</th>
                            <th>File Task</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${list1}" var="s" varStatus="loop">
                            <input type="hidden" id="taskID" name="taskID" value="${s.getTaskID()}" required>
                            <tr>
                                <td>${s.getGroupName()}</td>
                                <td class="submitted">
                                    <div class="status">
                                        <c:choose>
                                            <c:when test="${s.getStatusName() == 1}">
                                                Haven't submitted
                                            </c:when>
                                            <c:otherwise>
                                                Have submitted
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="time">${s.getAnswertime()}</div>
                                </td>
                                <td><a class="download-link" onclick="downloadFile('./uploads/answer/${s.getAnswerContent()}')">${s.getAnswerContent()}</a></td>
                                <td>
                                    <textarea id="comment" name="comment" class="form-control comment-box" required>${s.getComment()}</textarea>
                                    <c:if test="${not empty errorMessage}">
                                        <span id="error-message" class="error">${errorMessage}</span>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <button type="submit" class="btn-save">Save</button>
            </c:if>
        </form>
    </div>

    <script>
        const downloadFile = (file) => {
            window.location.href = "download?file=" + file;
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.6/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>
