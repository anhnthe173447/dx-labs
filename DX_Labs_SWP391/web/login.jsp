<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="https://unpkg.com/bootstrap@5.3.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://unpkg.com/bs-brain@2.0.4/components/logins/login-12/assets/css/login-12.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .text-end {
                margin-bottom: 15px;
                margin-right: 28px;
            }
            body {
                background-color: #f0f8ff;
                background-image: url('./images/ảnh backroundlogin.png');
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                height: 100vh;
                margin: 0;
            }
            .container {
                background-color: white;
                width: 40%;
                padding: 30px;
                border-radius: 10px;
                border-top:  2px solid black; /* Viền màu đen */
                box-shadow: 0 0 0 1px grey; /* Viền màu hồng */
            }

        </style>
    <body>
        <!-- Login 12 - Bootstrap Brain Component -->
        <section class="py-3 py-md-5 py-xl-8" >

            <div class="container" style="background-color: white;width: 40%">
                <div class="row">
                    <div class="col-12" style="margin-top: 50px;">
                        <div class="mb-5">
                            <h2 class="display-5  text-center" style="font-family: lato">SIGN IN</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center" >
                    <div class="col-12 col-lg-10 col-xl-8" style="margin-bottom: 50px">
                        <div class="row gy-5 justify-content-center">
                            <div class="col-12 col-lg-5" style="width: 100%;">
                                <form action="login" method="post">
                                    <div class="row gy-3 overflow-hidden">
                                        <div class="col-12">
                                            <div class="form-floating mb-3">
                                                <input type="text" class="form-control border-1 border-bottom rounded-1" name="user" id="email" placeholder="name@example.com" required>
                                                <label for="email" class="form-label">UserName</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-floating mb-3">
                                                <input type="password" class="form-control border-1 border-bottom rounded-1" name="pass" id="password"  placeholder="Password" required>
                                                <label for="password" class="form-label">Password</label>
                                            </div>
                                            ${mess}
                                        </div>

                                        <div class="text-end">
                                            <a href="requestPassword" class="link-secondary text-decoration-none">Forgot Password</a>
                                        </div>

                                    </div>

                                    <div class="col-12">
                                        <div class="d-grid">
                                            <button class="btn btn-lg btn-dark rounded-0 fs-6" type="submit">Log in</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>

</html>
