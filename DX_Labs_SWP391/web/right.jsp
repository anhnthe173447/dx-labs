<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="right-panel" class="right-panel" >
    <header id="header" class="header" style="color: white">
        <!-- Header Intern Start -->
        <c:if test="${sessionScope.acc.role==2}">
            <div class="top-left">
                <div class="navbar-header">

                    <a class="navbar-brand" href="home.jsp"> DX LAB - INTERN</a>

                </div>
            </div>
        </c:if>
        <!-- Header Intern End -->


        <!-- Header Mentor Start -->
        <c:if test="${sessionScope.acc.role==1}"> 

            <div class="top-left">
                <div class="navbar-header">

                    <a class="navbar-brand" href="home.jsp"> DX LAB - MENTOR</a>

                </div>
            </div>

        </c:if>
        <c:if test="${sessionScope.acc.role==3}"> 

            <div class="top-left">
                <div class="navbar-header">

                    <a class="navbar-brand" href="home.jsp"> DX LAB - HR</a>

                </div>
            </div>

        </c:if>
        <!-- Header Mentor End -->

        <div class="top-right">
            <div class="header-menu"> 
                <img src="images/logo-home-Djb_K2V0.png" width="14%" height="14%" alt="logo-home-Djb_K2V0" style="margin-right: 420px"/>    
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="${sessionScope.acc.img}" alt="User Avatar">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <c:if test="${sessionScope.acc.role == 2}">
                            <a class="nav-link" href="ProfileManager"><i class="fa fa- user"></i>My Profile</a>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 1}">
                            <a class="nav-link" href="ProfileMentor"><i class="fa fa- user"></i>My Profile</a>
                        </c:if>
                        <a class="nav-link" href="ChangePassword.jsp"><i class="fa fa- user"></i>Change Password</a>
                        <a class="nav-link" href="logout"><i class="fa fa-power -off"></i>Logout</a>
                    </div>
                </div>

            </div>
        </div>
    </header>



    <div>
        <div class="body" >

            <img src="images/BackGroundHome.jpg" width="100%" height="100%"/>

        </div>
    </div>
</div>
