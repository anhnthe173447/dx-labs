<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--right panel start-->
<div id="right-panel" class="right-panel">
    <header id="header" class="header">
        <!-- Header Intern Start -->

        <!--                top left start-->


        <!-- Header Intern End -->


        <!-- Header Mentor Start -->

        <c:if test="${sessionScope.acc.role==1}"> 
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home.jsp"> DX LAB - MENTOR</a>
                </div>
            </div>
        </c:if>
        <!-- Header Mentor End -->

        <c:if test="${sessionScope.acc.role==2}"> 
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home.jsp"> DX LAB - INTERN</a>
                </div>
            </div>
        </c:if>

        <c:if test="${sessionScope.acc.role==3}"> 
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home.jsp"> DX LAB - HR</a>
                </div>
            </div>
        </c:if>

        <c:if test="${sessionScope.acc.role==null}"> 
            <c:redirect url="login.jsp"/>
        </c:if>
        <!--               //top-right start-->
        <div class="top-right">
            <div class="header-menu"> 
                <img src="images/logo-home-Djb_K2V0.png" width="14%" height="14%" alt="logo-home-Djb_K2V0" style="margin-right: 420px"/>    


<!--                <div class="header-left">


                    <div class="dropdown for-message">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-envelope"></i>
                            <span class="count bg-primary">4</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">You have 4 Mails</p>
                            <a class="dropdown-item media" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <div class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Just now</span>
                                    <p>Hello, this is an example msg</p>
                                </div>
                            </a>
                            <a class="dropdown-item media" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <div class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 minutes ago</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                            </a>
                            <a class="dropdown-item media" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <div class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 minutes ago</span>
                                    <p>Hello, this is an example msg</p>
                                </div>
                            </a>
                            <a class="dropdown-item media" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <div class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 minutes ago</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>-->

                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="${sessionScope.acc.img}" alt="User Avatar">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <c:if test="${sessionScope.acc.role == 3}">
                            <a class="nav-link" href="ChangePassword.jsp"><i class="fa fa- user"></i>Change Password</a>
                            <a class="nav-link" href="logout"><i class="fa fa-power -off"></i>Logout</a>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 2}">
                            <a class="nav-link" href="ProfileManager"><i class="fa fa- user"></i>My Profile</a>
                            <a class="nav-link" href="ChangePassword.jsp"><i class="fa fa- user"></i>Change Password</a>
                            <a class="nav-link" href="logout"><i class="fa fa-power -off"></i>Logout</a>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 1}">
                            <a class="nav-link" href="ProfileMentor"><i class="fa fa- user"></i>My Profile</a>
                            <a class="nav-link" href="ChangePassword.jsp"><i class="fa fa- user"></i>Change Password</a>
                            <a class="nav-link" href="logout"><i class="fa fa-power -off"></i>Logout</a>
                        </c:if>


                    </div>
                </div>

            </div>
        </div>
    </header>
