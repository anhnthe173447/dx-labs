<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add New Announcement</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            padding: 20px;
        }
        .card {
            max-width: 500px;
            margin: 0 auto;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .card-body {
            padding: 20px;
        }
        .form-group {
            margin-bottom: 20px;
        }
        .btn-save {
            background-color: #007bff;
            color: #fff;
            border-color: #007bff;
        }
        .btn-save:hover {
            background-color: #0056b3;
            border-color: #0056b3;
        }
        .btn-edit {
            background-color: #6c757d;
            color: #fff;
            border-color: #6c757d;
        }
        .btn-edit:hover {
            background-color: #5a6268;
            border-color: #545b62;
        }
    </style>
</head>
<body>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Add New Announcement</h5>
            <form action="AddAnnouncement" method="post">
                <div class="form-group">
                    <label for="announcement_content">Content:</label>
                    <input type="text" id="announcement_content" name="announcement_content" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" id="description" name="description" class="form-control" required>
                </div>

                <div class="form-group">
    <label for="date">Date:</label>
    <input type="text" id="date" name="date" class="form-control" value="<%= java.time.LocalDate.now() %>" readonly required>
</div>

                <div class="form-group" style="color: red; font-weight: 700;">${msg}</div>

                <button type="submit" class="btn btn-save btn-block">Save</button>
                <a href="ListAnnouncement" class="btn btn-edit btn-block">Back</a>
            </form>
        </div>
    </div>
</body>
</html>
