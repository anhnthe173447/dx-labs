<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DX LAB</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

        <style>
            /*       ------------*/
            body {
                font-family: Arial, sans-serif;
                background-color: #e6f7ff;
                color: #333;
                margin: 0;
                padding: 20px;
            }

            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
            }

            th,
            td {
                padding: 12px;
                text-align: left;
            }

            th {
                color: black;
                font-weight: bold;
            }

            tr:nth-child(even) {
                background-color: #f2faff;
            }

            td {
                border-bottom: 1px solid #ddd;
            }

            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }

            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }

            .btn-edit:hover {
                background-color: #45a049;
            }

            .btn-delete {
                background-color: #f44336;
                color: white;
            }

            .btn-delete:hover {
                background-color: #da190b;
            }

            /*       ----------------*/
            #weatherWidget .currentDesc {
                color: #ffffff !important;
            }

            .traffic-chart {
                min-height: 335px;
            }

            #flotPie1 {
                height: 150px;
            }

            #flotPie1 td {
                padding: 3px;
            }

            #flotPie1 table {
                top: 20px !important;
                right: -10px !important;
            }

            .chart-container {
                display: table;
                min-width: 270px;
                text-align: left;
                padding-top: 10px;
                padding-bottom: 10px;
            }

            #flotLine5 {
                height: 105px;
            }

            #flotBarChart {
                height: 150px;
            }

            #cellPaiChart {
                height: 160px;
            }
            .table {
                width: 50%;
            }

            .table th, .table td {
                white-space: nowrap;
            }
        </style>
    </head>

    <body>


        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel"></div>
            <!-- Header-->
        <jsp:include page="rightandheader.jsp"></jsp:include>
            <!-- Right Panel -->
            <!-- Content -->
            <div class="content">
                <div class="body">
                    <!--        <form action="searchIntern" method="post">
                                <table>
                                    <tr>
                                        <td><input style="width: 100%" type="text" name="search" value="${search}"></td>
                                        <td>
                                            <select name="searchID" style="width: 100%;height: 30px;">
                                                <option value="1" ${searchID=="1" ? 'selected' : '' }>Name</option>
                                                <option value="2" ${searchID=="2" ? 'selected' : '' }>ID Student</option>
                                                <option value="3" ${searchID=="3" ? 'selected' : '' }>ID Intern</option>
                                            </select>
                                        </td>
                                        <td>
                                            <button>Search</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>-->
                <h5 style="font-weight: 600;color: red">${requestScope.error}</h5>
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-10">
                                <h2>Report <b>Overall</b></h2>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-">
                                    <a href="ExportInternData" class="btn btn-edit">Export to Excel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="managerIntern" method="post" >
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Student ID</th>
                                    <th rowspan="2">Staff ID</th>
                                    <th rowspan="2">Full Name</th>
                                    <th rowspan="2">Division</th>
                                    <th rowspan="2">Line Manager</th>
                                    <th rowspan="2">Allowance (if any)</th>
                                    <th rowspan="2">Comment</th>
                                    <th colspan="3">Evaluation</th>
                                    <th rowspan="2">Final Result (100%)</th>
                                    <th rowspan="2">Actions</th>
                                </tr>
                                <tr>
                                    <th>Major Knowledge and Skills (40%)</th>
                                    <th>Soft Skills (30%)</th>
                                    <th>Attitude (30%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${list}" var="o" varStatus="status">
                                    <tr>
                                        <td>${status.index + 1}</td>
                                        <td>${o.getStudentID()}</td>
                                        <td>${o.internID}</td>
                                        <td><a href="DetailIntern?id=${o.internID}">${o.internName}</a></td>
                                        <td>${o.division}</td>
                                        <td>${o.mentorName}</td>
                                        <td>${o.allowance}</td>
                                        <td>${o.comment}</td>
                                        <td style="text-align: center">${o.majorSkillPoint}</td>
                                        <td style="text-align: center">${o.softSkillPoint}</td>
                                        <td style="text-align: center">${o.attitudePoint}</td>
                                        <td style="text-align: center">${o.finalResult}</td>
                                        <td>
                                            <div class="row">
                                                <button style="margin-bottom: 10px; width: 90px" class="btn btn-success">
                                                    <a href="EditReportOverall?id=${o.internID}">Edit</a>
                                                </button>
                                                <button style="margin-bottom: 10px; width: 90px" class="btn btn-delete">
                                                    <a href="#" onclick="return confirmDelete('DeleterReportOverall?id=${o.internID}');">Delete</a>
                                                </button> 
                                            </div>

                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>

        <!-- /#right-panel -->

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script
        src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>

        <!--  Chart js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

        <!--Chartist Chart-->
        <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
        <script
        src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
        <script src="assets/js/init/weather-init.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
        <script src="assets/js/init/fullcalendar-init.js"></script>

        <!--Local Stuff-->
        <script>
                                                        jQuery(document).ready(function ($) {
                                                            "use strict";

                                                            // Pie chart flotPie1
                                                            var piedata = [
                                                                {label: "Desktop visits", data: [[1, 32]], color: '#5c6bc0'},
                                                                {label: "Tab visits", data: [[1, 33]], color: '#ef5350'},
                                                                {label: "Mobile visits", data: [[1, 35]], color: '#66bb6a'}
                                                            ];

                                                            $.plot('#flotPie1', piedata, {
                                                                series: {
                                                                    pie: {
                                                                        show: true,
                                                                        radius: 1,
                                                                        innerRadius: 0.65,
                                                                        label: {
                                                                            show: true,
                                                                            radius: 2 / 3,
                                                                            threshold: 1
                                                                        },
                                                                        stroke: {
                                                                            width: 0
                                                                        }
                                                                    }
                                                                },
                                                                grid: {
                                                                    hoverable: true,
                                                                    clickable: true
                                                                }
                                                            });
                                                            // Pie chart flotPie1  End
                                                            // cellPaiChart
                                                            var cellPaiChart = [
                                                                {label: "Direct Sell", data: [[1, 65]], color: '#5b83de'},
                                                                {label: "Channel Sell", data: [[1, 35]], color: '#00bfa5'}
                                                            ];
                                                            $.plot('#cellPaiChart', cellPaiChart, {
                                                                series: {
                                                                    pie: {
                                                                        show: true,
                                                                        stroke: {
                                                                            width: 0
                                                                        }
                                                                    }
                                                                },
                                                                legend: {
                                                                    show: false
                                                                }, grid: {
                                                                    hoverable: true,
                                                                    clickable: true
                                                                }

                                                            });
                                                            // cellPaiChart End
                                                            // Line Chart  #flotLine5
                                                            var newCust = [[0, 3], [1, 5], [2, 4], [3, 7], [4, 9], [5, 3], [6, 6], [7, 4], [8, 10]];

                                                            var plot = $.plot($('#flotLine5'), [{
                                                                    data: newCust,
                                                                    label: 'New Data Flow',
                                                                    color: '#fff'
                                                                }],
                                                                    {
                                                                        series: {
                                                                            lines: {
                                                                                show: true,
                                                                                lineColor: '#fff',
                                                                                lineWidth: 2
                                                                            },
                                                                            points: {
                                                                                show: true,
                                                                                fill: true,
                                                                                fillColor: "#ffffff",
                                                                                symbol: "circle",
                                                                                radius: 3
                                                                            },
                                                                            shadowSize: 0
                                                                        },
                                                                        points: {
                                                                            show: true,
                                                                        },
                                                                        legend: {
                                                                            show: false
                                                                        },
                                                                        grid: {
                                                                            show: false
                                                                        }
                                                                    });
                                                            // Line Chart  #flotLine5 End
                                                            // Traffic Chart using chartist
                                                            if ($('#traffic-chart').length) {
                                                                var chart = new Chartist.Line('#traffic-chart', {
                                                                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                                                                    series: [
                                                                        [0, 18000, 35000, 25000, 22000, 0],
                                                                        [0, 33000, 15000, 20000, 15000, 300],
                                                                        [0, 15000, 28000, 15000, 30000, 5000]
                                                                    ]
                                                                }, {
                                                                    low: 0,
                                                                    showArea: true,
                                                                    showLine: false,
                                                                    showPoint: false,
                                                                    fullWidth: true,
                                                                    axisX: {
                                                                        showGrid: true
                                                                    }
                                                                });

                                                                chart.on('draw', function (data) {
                                                                    if (data.type === 'line' || data.type === 'area') {
                                                                        data.element.animate({
                                                                            d: {
                                                                                begin: 2000 * data.index,
                                                                                dur: 2000,
                                                                                from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                                                                to: data.path.clone().stringify(),
                                                                                easing: Chartist.Svg.Easing.easeOutQuint
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                            // Traffic Chart using chartist End
                                                            //Traffic chart chart-js
                                                            if ($('#TrafficChart').length) {
                                                                var ctx = document.getElementById("TrafficChart");
                                                                ctx.height = 150;
                                                                var myChart = new Chart(ctx, {
                                                                    type: 'line',
                                                                    data: {
                                                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
                                                                        datasets: [
                                                                            {
                                                                                label: "Visit",
                                                                                borderColor: "rgba(4, 73, 203,.09)",
                                                                                borderWidth: "1",
                                                                                backgroundColor: "rgba(4, 73, 203,.5)",
                                                                                data: [0, 2900, 5000, 3300, 6000, 3250, 0]
                                                                            },
                                                                            {
                                                                                label: "Bounce",
                                                                                borderColor: "rgba(245, 23, 66, 0.9)",
                                                                                borderWidth: "1",
                                                                                backgroundColor: "rgba(245, 23, 66,.5)",
                                                                                pointHighlightStroke: "rgba(245, 23, 66,.5)",
                                                                                data: [0, 4200, 4500, 1600, 4200, 1500, 4000]
                                                                            },
                                                                            {
                                                                                label: "Targeted",
                                                                                borderColor: "rgba(40, 169, 46, 0.9)",
                                                                                borderWidth: "1",
                                                                                backgroundColor: "rgba(40, 169, 46, .5)",
                                                                                pointHighlightStroke: "rgba(40, 169, 46,.5)",
                                                                                data: [1000, 5200, 3600, 2600, 4200, 5300, 0]
                                                                            }
                                                                        ]
                                                                    },
                                                                    options: {
                                                                        responsive: true,
                                                                        tooltips: {
                                                                            mode: 'index',
                                                                            intersect: false
                                                                        },
                                                                        hover: {
                                                                            mode: 'nearest',
                                                                            intersect: true
                                                                        }

                                                                    }
                                                                });
                                                            }
                                                            //Traffic chart chart-js End
                                                            // Bar Chart #flotBarChart
                                                            $.plot("#flotBarChart", [{
                                                                    data: [[0, 18], [2, 8], [4, 5], [6, 13], [8, 5], [10, 7], [12, 4], [14, 6], [16, 15], [18, 9], [20, 17],
                                                                        [22, 7], [24, 4], [26, 9], [28, 11]],
                                                                    bars: {
                                                                        show: true,
                                                                        lineWidth: 0,
                                                                        fillColor: '#ffffff8a'
                                                                    }
                                                                }], {
                                                                grid: {
                                                                    show: false
                                                                }
                                                            });
                                                            // Bar Chart #flotBarChart End
                                                        });
        </script>
        <script>
            function confirmAddAllAccount() {
                if (confirm('Do you want to add all accounts automatically for interns who dont have accounts?')) {
                    // N?u ng??i d�ng ch?n OK trong h?p tho?i x�c nh?n
                    window.location.href = 'addAllAccount'; // Th?c hi?n h�nh ??ng chuy?n h??ng
                } else {
                    // N?u ng??i d�ng ch?n Cancel trong h?p tho?i x�c nh?n
                    // Kh�ng l�m g� c?
                }
            }
        </script>

        <script>
            function confirmDelete(url) {
                if (confirm("Are you sure you want to delete this report?")) {
                    window.location.href = url;
                    return true;
                }
                return false;
            }
        </script>


    </body>

</html>