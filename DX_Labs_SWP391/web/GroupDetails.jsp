<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Group Details</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet">

        <!-- Custom Styles -->
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #F1F2F7;
                color: #333;
                margin: 0;
                padding: 0;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin: 20px 0;
                margin-top: -20px;
            }
            .container {
                margin-left: -250px;
                padding: 20px;
                background-color: #F1F2F7;
                width: 120%;
            }
            .table-container {
                background-color: #ffffff;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                padding: 20px;
                border-radius: 8px;
                width: 100%;
                overflow-x: auto;
                margin-top: -40px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
            }
            th, td {
                padding: 16px;
                text-align: left;
            }
            th {
                color: black;
                font-weight: bold;
                font-size: 1.1em;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }
            td {
                border-bottom: 1px solid #ddd;
                font-size: 1em;
            }
            .btn {
                padding: 10px 15px;
                margin: 5px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-primary {
                background-color: #007bff;
                color: white;
                margin-top: -60px;
            }
            .btn-primary:hover {
                background-color: #0056b3;
            }
            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }
            .btn-edit:hover {
                background-color: #45a049;
            }
            .btn-delete {
                background-color: #f44336;
                color: white;
            }
            .btn-delete:hover {
                background-color: #da190b;
            }
            .btn-leader {
                background-color: #ffc107;
                color: white;
            }
            .btn-leader:hover {
                background-color: #e0a800;
            }
            .alert {
                padding: 10px 20px;
                background-color: #ffdddd;
                color: #333;
                margin-bottom: 20px;
                border-radius: 4px;
                border: 1px solid #f5c6cb;
                font-size: 1em;
                text-align: center;
            }
        </style>

        <!-- JavaScript -->
        <script type="text/javascript">
            function doDelete(idIntern) {
                if (confirm("Are you sure you want to delete Intern " + idIntern + " from the group?")) {
                    window.location = "deleteInternToGroup?intern_id=" + idIntern;
                }
            }

            function doRemoveLeader(idIntern) {
                if (confirm("Are you sure you want to remove the leader from Intern " + idIntern + "?")) {
                    document.getElementById('removeLeaderForm-' + idIntern).submit();
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>

            <div id="right-panel" class="right-panel">
            <jsp:include page="rightandheader.jsp"></jsp:include>

                <div class="content">
                    <div class="container">
                        <h1>List Intern in Group ${groupId}</h1>

                    <div style="margin-bottom: 10px;">
                        <a href="displayInternNoGroup?groupId=${groupId}" class="btn btn-primary" style="margin-top: 0px;margin-bottom: 0px">Add Intern</a>
                        
                    </div>
                        <div class="table-container" style="margin-top: 10px">
                        <table class="table table-striped table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID Intern</th>
                                    <th>Full Name</th>
                                    <th>Student ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${interns}" var="i">
                                    <c:forEach items="${students}" var="s">
                                        <c:if test="${i.idStudent eq s.roll_number}">
                                            <c:if test="${i.semesterID == 2}">
                                                <tr>
                                                    <td>${i.idIntern}</td>
                                                    <td>${s.fullname}</td>
                                                    <td>${i.idStudent}</td>
                                                    <td>
                                                        <c:if test="${i.isLeader != 1}">
                                                            <a class="btn btn-delete" onclick="doDelete('${i.idIntern}')">Delete</a>
                                                        </c:if>
                                                        <c:if test="${i.isLeader == 1}">
                                                            <form id="removeLeaderForm-${i.idIntern}" action="GroupDetail" method="post" style="display:inline;">
                                                                <input type="hidden" name="action" value="removeLeader" />
                                                                <input type="hidden" name="internId" value="${i.idIntern}" />
                                                                <input type="hidden" name="groupId" value="${groupId}" />
                                                                <button type="button" class="btn btn-leader" onclick="doRemoveLeader('${i.idIntern}')">Remove Leader</button>
                                                            </form>
                                                        </c:if>
                                                        <c:if test="${not hasLeader}">
                                                            <form action="GroupDetail" method="post" style="display:inline;">
                                                                <input type="hidden" name="action" value="setLeader" />
                                                                <input type="hidden" name="internId" value="${i.idIntern}" />
                                                                <input type="hidden" name="groupId" value="${groupId}" />
                                                                <button type="submit" class="btn btn-leader">Set as Leader</button>
                                                            </form>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- JavaScript Libraries -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Chart js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

        <!--Chartist Chart-->
        <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
        <script src="assets/js/init/weather-init.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
        <script src="assets/js/init/fullcalendar-init.js"></script>
    </body>
</html>