<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<html class="no-js" lang=""> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DX LAB</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
        <style>
            /*       ------------*/
            body {
                font-family: Arial, sans-serif;
                background-color: #F1F2F7;
                color: #333;
                margin: 0;
                padding: 20px;
            }

            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #F1F2F7;
            }

            th,
            td {
                padding: 12px;
                text-align: left;
            }

            th {
                color: black;
                font-weight: bold;
            }

            tr:nth-child(even) {
                background-color: #f2faff;
            }

            td {
                border-bottom: 1px solid #ddd;
            }

            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }

            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }

            .btn-edit:hover {
                background-color: #45a049;
            }

            .btn-delete {
                background-color: #f44336;
                color: white;
            }

            .btn-delete:hover {
                background-color: #da190b;
            }

            /*       ----------------*/
            #weatherWidget .currentDesc {
                color: #ffffff !important;
            }

            .traffic-chart {
                min-height: 335px;
            }

            #flotPie1 {
                height: 150px;
            }

            #flotPie1 td {
                padding: 3px;
            }

            #flotPie1 table {
                top: 20px !important;
                right: -10px !important;
            }

            .chart-container {
                display: table;
                min-width: 270px;
                text-align: left;
                padding-top: 10px;
                padding-bottom: 10px;
            }

            #flotLine5 {
                height: 105px;
            }

            #flotBarChart {
                height: 150px;
            }

            #cellPaiChart {
                height: 160px;
            }

            .search-bar input[type="text"] {
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 4px;
                width: 500px; /* Increase the width here */
                margin-right: 10px;
            }
            .search-bar button {
                padding: 10px 20px;
                background-color: #005b96;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .search-bar button:hover {
                background-color: #F1F2F7;
            }
            .search-bar select {
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 4px;
                width: 500px; /* Adjust the width if needed */
                margin-right: 10px;
                box-sizing: border-box; /* Ensure padding and border are included in the element's total width and height */
                height: 45px; /* Ensure the height matches the input field */
            }
        </style>
    </head>

    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel"> </div>
            <!-- Header-->
        <jsp:include page="rightandheader.jsp"></jsp:include>
            <!-- Right Panel -->
            <!--            Content -->



            <!--            Content -->

            <div class="content">


                <!--                <div>  <div class="body" 
                                    <form>
                                        <div style="text-align: center">
                                            <input style="width: 50%;height: 35px;padding-top: 4px" id="search" type="text" placeholder="What are you looking for?" />
                                            <button style="height: 35px;color: white;background-color: rgb(45, 98, 211);border: 0px;font-size: 20px;cursor: pointer; position: relative;z-index: 0;font-weight: 300;" class="btn-search" type="button">SEARCH</button>
                                        </div>
                                    </form>
                                </div>-->

                <div class="search-bar" style="margin-left: 80px;background-color: #F1F2F7">
                    <form action="SearchInternByMentor" method="post">
                        <table>
                            <tr>
                                <td><input style="width: 100%" type="text" name="search" value="${search}"></td>
                            <td>
                                <select name="searchID" style="width: 100%">
                                    <option value="1" ${searchID=="1" ? 'selected' : '' }>Name</option>
                                    <option value="2" ${searchID=="2" ? 'selected' : '' }>ID Student</option>
                                    <option value="3" ${searchID=="3" ? 'selected' : '' }>ID Intern</option>
                                </select>
                            </td>
                            <td>
                                <button type="submit">Search</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Intern</b></h2>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-10">
                        <form id="statusForm" action="ManagerInternByMentor" method="post"> Semester
                            <select name="statusFilter" id="statusFilter" style="width: 30%;height: 30px;" onchange="document.getElementById('statusForm').submit()">
                                <option value="0" ${semester=="0" ? 'selected' : ''}>All</option>
                                <option value="1" ${semester=="1" ? 'selected' : ''}>Spring 24</option>
                                <option value="2" ${semester=="2" ? 'selected' : ''}>Summer 24</option>
                                <option value="3" ${semester=="3" ? 'selected' : ''}>Fall 24</option>
                            </select>
                        </form>
                    </div>

                    <div class="col-sm-2">
                        <a onclick="confirmAddAllAccount()" style="color: white" class="btn btn-edit">Add all Account</a>
                    </div>
                </div>
                <div class="body">                   
                    <h5 style="font-weight: 600;color: red">${requestScope.error}</h5>
                    <form action="ManagerInternByMentor" method="post">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>id student</th>
                                    <th style="text-align: center">Image</th>
                                    <th>Full name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listI}" var="o" varStatus="status1">
                                    <c:set var="s" value="${listS[status1.index]}" />
                                    <c:set var="a" value="${listA[status1.index]}" />

                                    <tr>
                                        <td>${status1.index + 1}</td>
                                        <td>${s.getRollNumber()}</td>
                                        <td style="text-align: center">
                                            <img width="130px" height="146px" src="${a.getImg()}" />
                                        </td>
                                        <td>
                                            <a href="DetailIntern?id=${o.idIntern}">${s.getFullName()}</a>
                                        </td>
                                        <td>${s.getEmail()}</td>

                                        <c:if test="${o.status == true}">
                                            <td>Active</td>
                                        </c:if>
                                        <c:if test="${o.status == false}">
                                            <td>Stop working</td>
                                        </c:if>

                                    </tr>
                                </c:forEach>
                        </table>
                    </form>
                </div>

                <!-- /.site-footer -->
            </div>
            <!-- /#right-panel -->

            <!-- Scripts -->
            <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
            <script
            src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
            <script src="assets/js/main.js"></script>

            <!--  Chart js -->
            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

            <!--Chartist Chart-->
            <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
            <script
            src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
            <script src="assets/js/init/weather-init.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js">
            </script>
            <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
            <script src="assets/js/init/fullcalendar-init.js"></script>

            <!--Local Stuff-->
            <script>
                            jQuery(document).ready(function ($) {
                                "use strict";

                                // Pie chart flotPie1
                                var piedata = [
                                    {label: "Desktop visits", data: [[1, 32]], color: '#5c6bc0'},
                                    {label: "Tab visits", data: [[1, 33]], color: '#ef5350'},
                                    {label: "Mobile visits", data: [[1, 35]], color: '#66bb6a'}
                                ];

                                $.plot('#flotPie1', piedata, {
                                    series: {
                                        pie: {
                                            show: true,
                                            radius: 1,
                                            innerRadius: 0.65,
                                            label: {
                                                show: true,
                                                radius: 2 / 3,
                                                threshold: 1
                                            },
                                            stroke: {
                                                width: 0
                                            }
                                        }
                                    },
                                    grid: {
                                        hoverable: true,
                                        clickable: true
                                    }
                                });
                                // Pie chart flotPie1  End
                                // cellPaiChart
                                var cellPaiChart = [
                                    {label: "Direct Sell", data: [[1, 65]], color: '#5b83de'},
                                    {label: "Channel Sell", data: [[1, 35]], color: '#00bfa5'}
                                ];
                                $.plot('#cellPaiChart', cellPaiChart, {
                                    series: {
                                        pie: {
                                            show: true,
                                            stroke: {
                                                width: 0
                                            }
                                        }
                                    },
                                    legend: {
                                        show: false
                                    }, grid: {
                                        hoverable: true,
                                        clickable: true
                                    }

                                });
                                // cellPaiChart End
                                // Line Chart  #flotLine5
                                var newCust = [[0, 3], [1, 5], [2, 4], [3, 7], [4, 9], [5, 3], [6, 6], [7, 4], [8, 10]];

                                var plot = $.plot($('#flotLine5'), [{
                                        data: newCust,
                                        label: 'New Data Flow',
                                        color: '#fff'
                                    }],
                                        {
                                            series: {
                                                lines: {
                                                    show: true,
                                                    lineColor: '#fff',
                                                    lineWidth: 2
                                                },
                                                points: {
                                                    show: true,
                                                    fill: true,
                                                    fillColor: "#ffffff",
                                                    symbol: "circle",
                                                    radius: 3
                                                },
                                                shadowSize: 0
                                            },
                                            points: {
                                                show: true,
                                            },
                                            legend: {
                                                show: false
                                            },
                                            grid: {
                                                show: false
                                            }
                                        });
                                // Line Chart  #flotLine5 End
                                // Traffic Chart using chartist
                                if ($('#traffic-chart').length) {
                                    var chart = new Chartist.Line('#traffic-chart', {
                                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                                        series: [
                                            [0, 18000, 35000, 25000, 22000, 0],
                                            [0, 33000, 15000, 20000, 15000, 300],
                                            [0, 15000, 28000, 15000, 30000, 5000]
                                        ]
                                    }, {
                                        low: 0,
                                        showArea: true,
                                        showLine: false,
                                        showPoint: false,
                                        fullWidth: true,
                                        axisX: {
                                            showGrid: true
                                        }
                                    });

                                    chart.on('draw', function (data) {
                                        if (data.type === 'line' || data.type === 'area') {
                                            data.element.animate({
                                                d: {
                                                    begin: 2000 * data.index,
                                                    dur: 2000,
                                                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                                    to: data.path.clone().stringify(),
                                                    easing: Chartist.Svg.Easing.easeOutQuint
                                                }
                                            });
                                        }
                                    });
                                }
                                // Traffic Chart using chartist End
                                //Traffic chart chart-js
                                if ($('#TrafficChart').length) {
                                    var ctx = document.getElementById("TrafficChart");
                                    ctx.height = 150;
                                    var myChart = new Chart(ctx, {
                                        type: 'line',
                                        data: {
                                            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
                                            datasets: [
                                                {
                                                    label: "Visit",
                                                    borderColor: "rgba(4, 73, 203,.09)",
                                                    borderWidth: "1",
                                                    backgroundColor: "rgba(4, 73, 203,.5)",
                                                    data: [0, 2900, 5000, 3300, 6000, 3250, 0]
                                                },
                                                {
                                                    label: "Bounce",
                                                    borderColor: "rgba(245, 23, 66, 0.9)",
                                                    borderWidth: "1",
                                                    backgroundColor: "rgba(245, 23, 66,.5)",
                                                    pointHighlightStroke: "rgba(245, 23, 66,.5)",
                                                    data: [0, 4200, 4500, 1600, 4200, 1500, 4000]
                                                },
                                                {
                                                    label: "Targeted",
                                                    borderColor: "rgba(40, 169, 46, 0.9)",
                                                    borderWidth: "1",
                                                    backgroundColor: "rgba(40, 169, 46, .5)",
                                                    pointHighlightStroke: "rgba(40, 169, 46,.5)",
                                                    data: [1000, 5200, 3600, 2600, 4200, 5300, 0]
                                                }
                                            ]
                                        },
                                        options: {
                                            responsive: true,
                                            tooltips: {
                                                mode: 'index',
                                                intersect: false
                                            },
                                            hover: {
                                                mode: 'nearest',
                                                intersect: true
                                            }

                                        }
                                    });
                                }
                                //Traffic chart chart-js End
                                // Bar Chart #flotBarChart
                                $.plot("#flotBarChart", [{
                                        data: [[0, 18], [2, 8], [4, 5], [6, 13], [8, 5], [10, 7], [12, 4], [14, 6], [16, 15], [18, 9], [20, 17],
                                            [22, 7], [24, 4], [26, 9], [28, 11]],
                                        bars: {
                                            show: true,
                                            lineWidth: 0,
                                            fillColor: '#ffffff8a'
                                        }
                                    }], {
                                    grid: {
                                        show: false
                                    }
                                });
                                // Bar Chart #flotBarChart End
                            });
            </script>
            <script>
                function confirmAddAllAccount() {
                    if (confirm('Do you want to add all accounts automatically for interns who dont have accounts?')) {
                        // N?u ng??i d�ng ch?n OK trong h?p tho?i x�c nh?n
                        window.location.href = 'addAllAccount'; // Th?c hi?n h�nh ??ng chuy?n h??ng
                    } else {
                        // N?u ng??i d�ng ch?n Cancel trong h?p tho?i x�c nh?n
                        // Kh�ng l�m g� c?
                    }
                }
            </script>
            <script>
                function submitStatusForm() {
                    document.getElementById('statusForm').submit();
                }
            </script>


    </body>

</html>