<%-- 
    Document   : UpdateOverallPoint
    Created on : Jun 20, 2024, 2:46:39 PM
    Author     : admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Update Midterm Point</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DX LAB</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/tablestyle.css">
        <style>
            /*       ------------*/
            body {
                font-family: Arial, sans-serif;
                background-color: #e6f7ff;
                color: #333;
                margin: 0;
                padding: 20px;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
            }
            th, td {
                padding: 12px;
                text-align: left;
            }
            th {
                color: black;
                font-weight: bold;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }

            td {
                border-bottom: 1px solid #ddd;
            }
            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }
            .btn-edit:hover {
                background-color: #45a049;
            }
            .btn-delete {
                background-color: #f44336;
                color: white;
            }
            .btn-delete:hover {
                background-color: #da190b;
            }
            /*       ----------------*/
            #weatherWidget .currentDesc {
                color: #ffffff!important;
            }
            .traffic-chart {
                min-height: 335px;
            }
            #flotPie1  {
                height: 150px;
            }
            #flotPie1 td {
                padding:3px;
            }
            #flotPie1 table {
                top: 20px!important;
                right: -10px!important;
            }
            .chart-container {
                display: table;
                min-width: 270px ;
                text-align: left;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            #flotLine5  {
                height: 105px;
            }

            #flotBarChart {
                height: 150px;
            }
            #cellPaiChart{
                height: 160px;
            }
            .container {
                padding-top: 20px;
                padding-left: 20px; /* Giảm padding để loại bỏ khoảng trống thừa */
                padding-right: 20px; /* Giảm padding để loại bỏ khoảng trống thừa */
                width: 100%;
                position: relative;
            }
            .card {
                margin-bottom: 20px;
                border: 2px solid;
            }
            .card-header {
                text-align: center;
                background-color: #0099d4;
                color: black;
            }
            .card-body {
                padding: 10px;
            }
            .card-text i {
                margin-right: 5px;
            }
            .button-group {
                display: flex;
                justify-content: space-between;
                width: 100%;
            }
            .button-group .btn {
                flex: 1;
                margin: 5px;
            }
        </style>
        <style>
            .button {
                display: inline-block;
                padding: 6px 12px;
                margin: 4px 2px;
                font-size: 16px;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #f44336; /* Red */
                border: none;
                border-radius: 4px;
            }

            .button:hover {
                background-color: #d32f2f;
            } /* Darker red on hover */

            .buttonsubmit{
                display: inline-block;
                padding: 6px 12px;
                margin: 4px 2px;
                font-size: 16px;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: greenyellow;
                border: none;
                border-radius: 4px;
            }

            .buttonsubmit:hover {
                background-color: #45a049;
            }
        </style>
        <style>
        .message {
            margin: 20px 0;
            padding: 10px;
            border: 1px solid #4CAF50;
            background-color: #dff0d8;
            color: #3c763d;
        }
    </style>
    </head>
    <body>
        <!-- Validate Js -->
        <script>
            function validateForm() {
                var majorPoints = document.getElementById("majorPoints").value.trim();
                var softSkillPoints = document.getElementById("softSkillPoints").value.trim();
                var attitudePoints = document.getElementById("attitudePoints").value.trim();

                // Kiểm tra nếu không phải là số thực
                if (!isFloat(majorPoints) || !isFloat(softSkillPoints) || !isFloat(attitudePoints)) {
                    alert("Major Points, Soft Skill Points, and Attitude Points must be valid numbers.");
                    return false;
                }

                return true;
            }

            function isFloat(value) {
                return /^\d*\.?\d*$/.test(value);
            }
        </script>
        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel"></div>
            <!-- Header-->
        <jsp:include page="rightandheader.jsp"></jsp:include>
        <h2>Update Midterm Point for Intern: ${OverallPoint.internName}</h2>
        <form action="UpdateOverallPoint" method="POST">
            <table class="tableb">
                <input type="hidden" name="idIntern" value="${OverallPoint.internID}" />
                <tr>
                    <td><label for="majorpoint">Major Skill Point</label></td>
                    <td><input type="text" name="majorpoint" value="${OverallPoint.majorSkillPoint}" /></td>
                </tr>
                <tr>
                    <td><label for="softSkillPoint">Soft Skill Point</label></td>
                    <td><input type="text" name="softSkillPoint" value="${OverallPoint.softSkillPoint}" /></td>
                </tr>
                <tr>
                    <td><label for="attitudePoint">Attitude Point</label></td>
                    <td><input type="text" name="attitudePoint" value="${OverallPoint.attitudePoint}" /></td>
                </tr>
                <tr>
                    <td><label for="finalResult">Final Result</label></td>
                    <td><input type="text" name="finalResult" value="${OverallPoint.finalResult}" /></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <button type="submit" class="buttonsubmit">Update</button>
                        <a href="DetailOverallPoint_Mentor?id=${OverallPoint.internID}" class="button">Cancel</a>
                    </td>
                </tr>
            </table>
        </form>

    </body>
</html>
