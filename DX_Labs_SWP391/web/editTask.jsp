<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Edit Task</title>
        <!-- Bootstrap CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
        <style>
            body {
                background-color: #F5F5F5;
                font-family: 'Roboto', sans-serif;
            }
            .container {
                max-width: 600px;
                margin-top: 50px;
            }
            .card {
                border-radius: 10px;
                box-shadow: 0 4px 8px rgba(0,0,0,0.1);
            }
            .card-header {
                background-color: #007BFF;
                color: #fff;
                border-radius: 10px 10px 0 0;
            }
            .card-body {
                background-color: #fff;
            }
            .form-group {
                margin-bottom: 20px;
            }
            label {
                font-weight: bold;
            }
            .btn-save {
                background-color: #007BFF;
                color: #fff;
            }
            .btn-save:hover {
                background-color: #0056B3;
            }
            .custom-select {
                padding: 10px;
                height: auto;
                border-radius: 4px;
                box-shadow: none;
                background-color: #fff;
                border: 1px solid #ced4da;
            }
        </style>
    </head>
    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <!-- Right Panel -->
            <div id="right-panel" class="right-panel"> </div>
            <!-- Header-->
        <jsp:include page="rightandheader.jsp"></jsp:include>

            <div class="container">
    <div class="card">
        <div class="card-header">
            <h2 class="text-center mb-0">Edit Task</h2>
        </div>
        <div class="card-body">
            <form action="edit" method="post">
                <c:forEach items="${list}" var="s">
                    <input type="hidden" id="taskID" name="taskID" value="${s.getTaskID()}" required>

                    <div class="form-group">
                        <label for="mentorID">Mentor: </label>
                          <input type="hidden" id="mentorID" name="mentorID" class="form-control" readonly value="${s.mentorID}" required>
                        <input type="text" id="mentorName" name="mentorName" class="form-control" readonly value="${s.getMentorName()}" required>
                    </div>

                    <div class="form-group">
                        <label for="groupID">Group Name: </label>
                        <input type="text" id="groupID" name="groupID" class="form-control" readonly value="${s.getGroupID()}" required>
                    </div>

                    <div class="form-group">
                        <label for="taskname">Task Name:</label>
                        <input type="text" id="taskname" name="taskname" class="form-control" value="${s.getTaskName()}" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" id="description" name="description" class="form-control" value="${s.getDescription()}" required>
                    </div>

                    <div class="form-group">
                        <label for="start">Start Date</label>
                        <input type="date" id="start" name="start" class="form-control" value="${s.getStartDate()}" required>
                    </div> 

                    <div class="form-group">
                        <label for="end">End Date</label>
                        <input type="date" id="end" name="end" class="form-control" value="${s.getEnddate()}" required>
                    </div>   
                </c:forEach>
                <div class="form-group" style="color: red; font-weight: 700">
                    <c:if test="${not empty mess}">
                        ${mess}
                    </c:if>
                </div>

                <button type="submit" class="btn btn-save btn-block">Save</button>
            </form>
        </div>
    </div>
</div>


        <!-- Bootstrap JS -->
        <script>
            document.getElementById("mentorID").addEventListener("change", function () {
                var mentorID = this.value;
                var groupSelect = document.getElementById("groupID");

                fetch("edit?action=getGroupsByMentor&mentorID=" + mentorID)
                        .then(response => response.text())
                        .then(data => {
                            groupSelect.innerHTML = data;
                        })
                        .catch(error => console.error('Error:', error));
            });
            jQuery(document).ready(function ($) {
                "use strict";

                // Pie chart flotPie1
                var piedata = [
                    {label: "Desktop visits", data: [[1, 32]], color: '#5c6bc0'},
                    {label: "Tab visits", data: [[1, 33]], color: '#ef5350'},
                    {label: "Mobile visits", data: [[1, 35]], color: '#66bb6a'}
                ];

                $.plot('#flotPie1', piedata, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.65,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                threshold: 1
                            },
                            stroke: {
                                width: 0
                            }
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                });
                // Pie chart flotPie1  End
                // cellPaiChart
                var cellPaiChart = [
                    {label: "Direct Sell", data: [[1, 65]], color: '#5b83de'},
                    {label: "Channel Sell", data: [[1, 35]], color: '#00bfa5'}
                ];
                $.plot('#cellPaiChart', cellPaiChart, {
                    series: {
                        pie: {
                            show: true,
                            stroke: {
                                width: 0
                            }
                        }
                    },
                    legend: {
                        show: false
                    }, grid: {
                        hoverable: true,
                        clickable: true
                    }

                });
                // cellPaiChart End
                // Line Chart  #flotLine5
                var newCust = [[0, 3], [1, 5], [2, 4], [3, 7], [4, 9], [5, 3], [6, 6], [7, 4], [8, 10]];

                var plot = $.plot($('#flotLine5'), [{
                        data: newCust,
                        label: 'New Data Flow',
                        color: '#fff'
                    }],
                        {
                            series: {
                                lines: {
                                    show: true,
                                    lineColor: '#fff',
                                    lineWidth: 2
                                },
                                points: {
                                    show: true,
                                    fill: true,
                                    fillColor: "#ffffff",
                                    symbol: "circle",
                                    radius: 3
                                },
                                shadowSize: 0
                            },
                            points: {
                                show: true,
                            },
                            legend: {
                                show: false
                            },
                            grid: {
                                show: false
                            }
                        });
                // Line Chart  #flotLine5 End
                // Traffic Chart using chartist
                if ($('#traffic-chart').length) {
                    var chart = new Chartist.Line('#traffic-chart', {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                        series: [
                            [0, 18000, 35000, 25000, 22000, 0],
                            [0, 33000, 15000, 20000, 15000, 300],
                            [0, 15000, 28000, 15000, 30000, 5000]
                        ]
                    }, {
                        low: 0,
                        showArea: true,
                        showLine: false,
                        showPoint: false,
                        fullWidth: true,
                        axisX: {
                            showGrid: true
                        }
                    });

                    chart.on('draw', function (data) {
                        if (data.type === 'line' || data.type === 'area') {
                            data.element.animate({
                                d: {
                                    begin: 2000 * data.index,
                                    dur: 2000,
                                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                    to: data.path.clone().stringify(),
                                    easing: Chartist.Svg.Easing.easeOutQuint
                                }
                            });
                        }
                    });
                }
                // Traffic Chart using chartist End
                //Traffic chart chart-js
                if ($('#TrafficChart').length) {
                    var ctx = document.getElementById("TrafficChart");
                    ctx.height = 150;
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
                            datasets: [
                                {
                                    label: "Visit",
                                    borderColor: "rgba(4, 73, 203,.09)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(4, 73, 203,.5)",
                                    data: [0, 2900, 5000, 3300, 6000, 3250, 0]
                                },
                                {
                                    label: "Bounce",
                                    borderColor: "rgba(245, 23, 66, 0.9)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(245, 23, 66,.5)",
                                    pointHighlightStroke: "rgba(245, 23, 66,.5)",
                                    data: [0, 4200, 4500, 1600, 4200, 1500, 4000]
                                },
                                {
                                    label: "Targeted",
                                    borderColor: "rgba(40, 169, 46, 0.9)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(40, 169, 46, .5)",
                                    pointHighlightStroke: "rgba(40, 169, 46,.5)",
                                    data: [1000, 5200, 3600, 2600, 4200, 5300, 0]
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            }

                        }
                    });
                }
                //Traffic chart chart-js  End
                // Bar Chart #flotBarChart
                $.plot("#flotBarChart", [{
                        data: [[0, 18], [2, 8], [4, 5], [6, 13], [8, 5], [10, 7], [12, 4], [14, 6], [16, 15], [18, 9], [20, 17], [22, 7], [24, 4], [26, 9], [28, 11]],
                        bars: {
                            show: true,
                            lineWidth: 0,
                            fillColor: '#ffffff8a'
                        }
                    }], {
                    grid: {
                        show: false
                    }
                });
                // Bar Chart #flotBarChart End
            });
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </body>
</html>
