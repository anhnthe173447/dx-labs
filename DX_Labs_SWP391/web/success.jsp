

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Success Page</title>
    </head>
    <body>
        <table>
            <tr>                     
                <th>File Name</th>
                <th>Answer Time</th>
                <th>Group Name</th>                      
            </tr>
            <input type="hidden" name="taskID" value="${taskID}">
            <c:forEach items="${list}" var="s">
                <tr>

                    <th>${s.getContent()}</th>
                    <th>${s.getAnswerTime()}</th>
                    <th>${s.getGroupid()}</th>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>