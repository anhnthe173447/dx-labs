<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<style>
    .button-container {
        text-align: center;
        margin-top: 20px;
    }

    .button-container button {
        padding: 10px 20px;
        font-size: 16px;
        border: none;
        cursor: pointer;
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
        margin-right: 10px;
        display: inline-block;
    }

    .button-container button:hover {
        background-color: #45a049;
    }
</style>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#" id="menuToggle"><i class="menu-icon fa fa-bars"></i>Dashboard</a>

                    <c:if test="${sessionScope.acc.role == 1}">

                    <li class="menu-title">MANAGE</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Task</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="tasks">Task Management</a></li>
                            <!--                                    <li><i class="menu-icon fa fa-th"></i><a href="viewallanswer">View All Answer</a></li>-->
                        </ul>
                    </li>  

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="menu-icon fa fa-bar-chart "></i>Intern
                        </a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-info"></i><a href="ManagerInternByMentor">List Intern</a>
                            </li>
                            <li><i class="menu-icon fa fa-info"></i><a href="${contextPath}/midterm-evaluate">List Midterm
                                    Point</a></li>
                            <li><i class="menu-icon fa fa-info"></i><a href="${contextPath}/finalterm-evaluate">List Final-term
                                    Point</a>
                            </li>
                            <li><i class="menu-icon fa fa-info"></i><a href="${contextPath}/overall-evaluate">List Overall Point</a></li>

                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="displayGroup" class="menu-icon"  aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Groups</a>
                    </li>


                    <li class="menu-title">Attendance</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="attendance" class="menu-icon" aria-haspopup="true" aria-expanded="false">
                            <i class="menu-icon fa fa-tasks"></i>Attendance Management</a>
                    </li>

                    <li class="menu-title">ANNOUNCEMENTS</li>

                    <li class="menu-item-has-children dropdown">
                        <a href="AnnouncementForIntern" class="menu-icon" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>
                            Announcement checking</a>
                    </li>

                    <li class="menu-title">Evaluation</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="menu-icon fa fa-bar-chart "></i>Evaluation
                        </a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-info"></i><a href="ListMidtermPoint">Midterm Point</a>
                            </li>
                            <li><i class="menu-icon fa fa-info"></i><a href="ListFinalTermPoint">Final-term
                                    Point</a>
                            </li>
                            <li><i class="menu-icon fa fa-info"></i><a href="ListOverallPoint">Overall score for the
                                    whole semester</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Report about intern</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="ReportInternMidtern">Report Midterm Intern</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="reportOverall">Report Overall Intern</a></li>
                        </ul>
                    </li>
                </c:if>

                <c:if test="${sessionScope.acc.role==2}">

                    <% 
  //                     session.getAttribute("internID"); if (internID==null) { 
           //           request internID=(Integer) request.getAttribute("internID"); } 
                    %>

                    <li class="menu-title">MENU</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="taskForIntern" class="menu-icon" aria-haspopup="true" aria-expanded="false"> <i
                                class="menu-icon fa fa-tasks"></i>Task</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="task">TASK MANAGE</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="listGroupMate" class="menu-icon" aria-haspopup="true" aria-expanded="false"> <i
                                class="menu-icon fa fa-tasks"></i>My Group </a>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="AnnouncementForIntern" class="menu-icon" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>
                            Announcement checking</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="ListPointByIntern?id=1" class="menu-icon" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>
                            Report Grade</a>
                    </li>
                    <div class="button-container">
                        <button id="checkInButton">Check In</button>
                        <button id="checkOutButton">Check Out</button>
                    </div>
                </c:if>

                <c:if test="${sessionScope.acc.role==3}">
                    <li class="menu-title">ACCOUNT</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="ManagerAccount" class="menu-icon"  aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-paper-plane"></i> Account manage</a>

                    </li>
                    <li class="menu-title">MENTOR</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="displayMentors" class="menu-icon"  aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-paper-plane"></i> Mentor</a>
                    </li>
                    <li class="menu-title">INTERN</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="managerIntern" class="menu-icon"  aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-paper-plane"></i>Intern</a>

                    </li>
                    <li class="menu-title">ANNOUNCEMENTS</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-table"></i> Announcement manage</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="ListAnnouncement">Announcement
                                    List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-title">STUDENT</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Student</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="displayStudentAdd">Student List</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="importExcel.jsp">Import Data From Excel</a></li>
                        </ul>
                    </li>
                </c:if>
            </ul>

        </div><!-- /.navbar-collapse -->
    </nav>

</aside>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>

    function updateButtons() {
        $.ajax({
            url: 'CheckStatusServlet',
            method: 'POST',

            success: function (response) {
                if (response == 'CheckedIn') {
                    $('#checkInButton').hide();
                    $('#checkOutButton').show();
                } else if (response == 'CheckedOut') {
                    $('#checkInButton').hide();
                    $('#checkOutButton').hide();
                } else {
                    $('#checkInButton').show();
                    $('#checkOutButton').hide();
                }
            },
            error: function (xhr, status, error) {
                console.log('Error:', error);
            }
        });
    }
    $(document).ready(function () {

        updateButtons()
        $("#checkInButton").click(function () {
            $.ajax({
                url: "AttendanceServlet",
                method: "POST",
                data: {action: "checkin"},
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Th�nh c�ng!',
                        text: response
                    }).then(function () {
                        location.reload();
                    });
                },
                error: function (xhr, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: xhr.responseText
                    });
                }
            });
        });

        $("#checkOutButton").click(function () {
            $.ajax({
                url: "AttendanceServlet",
                method: "POST",
                data: {action: "checkout"},
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Th�nh c�ng!',
                        text: response
                    }).then(function () {
                        location.reload();
                    });
                },
                error: function (xhr, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: xhr.responseText
                    });
                }
            });
        });
    });
</script>