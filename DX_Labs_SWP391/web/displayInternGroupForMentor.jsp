<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>List Intern No Group</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet">

        <!-- Custom Styles -->
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #e6f7ff;
                color: #333;
                margin: 0;
                padding: 0;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin: 20px 0;
                margin-top: -20px;
                margin-left: -40px;
            }
            .container {
                padding: 20px;
                background-color: #e6f7ff;
                width: 100%;
            }
            .table-container {
                background-color: #ffffff;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                padding: 20px;
                border-radius: 8px;
                width: 120%;
                overflow-x: auto;
                margin-bottom: 20px;
                margin-left: -200px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
            }
            th, td {
                padding: 16px;
                text-align: left;
            }
            th {
                background-color: #f2f2f2;
                color: #333;
                font-weight: bold;
                font-size: 1.1em;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }
            td {
                border-bottom: 1px solid #ddd;
                font-size: 1em;
            }
            .btn {
                padding: 10px 15px;
                margin: 5px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-primary {
                background-color: #007bff;
                color: white;
            }
            .btn-primary:hover {
                background-color: #0056b3;
            }
            .btn-add-group {
                background-color: #28a745;
                color: white;
            }
            .btn-add-group:hover {
                background-color: #218838;
            }
            .search-form {
                margin: 20px 0;
                text-align: center;
            }
            .search-input {
                width: 300px;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .search-button {
                padding: 10px 20px;
                border: none;
                background-color: #007bff;
                color: white;
                border-radius: 4px;
                cursor: pointer;
            }
            .search-button:hover {
                background-color: #0056b3;
            }
            .alert {
                padding: 10px 20px;
                background-color: #ffdddd;
                color: #333;
                margin-bottom: 20px;
                border-radius: 4px;
                border: 1px solid #f5c6cb;
                font-size: 1em;
                text-align: center;
            }
            .search-form {
                display: none;
            }
        </style>
    </head>
    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>
            <div id="right-panel" class="right-panel">
            <jsp:include page="rightandheader.jsp"></jsp:include>
                <div class="content">
                    <div class="container">
                        <div class="table-container">
                            <div class="col-sm-6">
                                <h1>List Intern Mentor ${mentor.fullName}</h1>
                        </div>

                        <div class="search-form">
                            <form action="displayInternNoGroup" method="post">
                                <input type="text" name="studentId" class="search-input" placeholder="Enter Student ID" 
                                       value="${not empty requestScope.studentId ? requestScope.studentId : ''}"/>
                                <input type="hidden" name="groupId" value="${groupId}"/>
                                <button type="submit" class="search-button">Search</button>
                            </form>
                        </div>
                        <c:if test="${not empty mess}">
                            <div class="alert">${mess}</div>
                        </c:if>


                        <table class="table table-striped table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID Intern</th>
                                    <th>Full Name</th>
                                    <th>Student ID</th>
                                    <th>Group Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${interns}" var="i">
                                    <c:forEach items="${students}" var="s">
                                        <c:forEach items="${groups}" var="g">
                                            <c:if test="${i.idStudent eq s.roll_number}">
                                                <c:if test="${i.groupID eq g.group_id}">
                                                    <c:if test="${i.semesterID == 2}">
                                                        <tr>
                                                            <td>${i.idIntern}</td>
                                                            <td>${s.fullname}</td>
                                                            <td>${i.idStudent}</td>
                                                            <td>${g.group_name}</td>
                                                        </tr>
                                                    </c:if>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- JavaScript Libraries -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
        <script src="assets/js/init/weather-init.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
        <script src="assets/js/init/fullcalendar-init.js"></script>
    </body>
</html>