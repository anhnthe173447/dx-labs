<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DX LAB</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/logo.png">
        <link rel="shortcut icon" href="images/logo.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #F1F2F7;
                color: #333;
                margin: 0;
                padding: 20px;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
            }
            th, td {
                padding: 12px;
                text-align: left;
            }
            th {
                color: black;
                font-weight: bold;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }
            td {
                border-bottom: 1px solid #ddd;
            }
            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }
            .btn-edit:hover {
                background-color: #45a049;
            }
            .btn-delete {
                background-color: #f44336;
                color: white;
            }
            .btn-delete:hover {
                background-color: #da190b;
            }
            .btn-view {
                background-color: #78909c;
                color: white;
            }
            .btn-view:hover {
                background-color: #ddd;
            }
            .search-bar {
                display: flex;
                justify-content: center;
                margin-bottom: 20px;
            }
            .search-bar input[type="text"] {
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 4px;
                width: 500px; /* Increase the width here */
                margin-right: 10px;
            }
            .search-bar button {
                padding: 10px 20px;
                background-color: #005b96;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .search-bar button:hover {
                background-color: #004080;
            }
            .table-title h1 {
                color: white;
            }
        </style>
    </head>

    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>
        <!-- Right Panel -->
        <div id="right-panel" class="right-panel"> </div>
        <!-- Header -->
        <jsp:include page="rightandheader.jsp"></jsp:include>
        <div class="table-wrapper">
            <div class="table-title">
                <h1>TASK LIST</h1>
            </div>
            <div class="search-bar">
                <form method="post" action="tasks">
                    <input type="text" name="query" placeholder="Search tasks..." 
                           value="${not empty requestScope.searchValue ? requestScope.searchValue : ''}" />
                    <button type="submit">Search</button>
                </form>
            </div>
            <table>
                <tr>
                    <th>No</th>
                    <th>Mentor</th>
                    <th>Group Name</th>
                    <th>Task Name</th>                      
                    <th>Start Date</th>
                    <th>End Date</th>           
                    <th>Answer Status</th>
                    <th>Actions</th>
                </tr>
                <c:if test="${not empty message}">
                    <div style="color: red">${message}</div>
                </c:if>
                <c:forEach items="${listTask}" var="s" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>  ${s.getMentorName()}</td>
                        <td>${s.getGroupName()}</td>
                        <td><a href="givePoint?taskID=${s.getTaskID()}"> <b style="color: black">${s.getTaskName()}</b></a></td>
                        <td>${s.getStartDate()}</td>
                        <td>${s.getEnddate()}</td>
                        <td>
                            <c:choose>
                                <c:when test="${s.getStatusName() == 1}">
                                    <h5 style="color:red"><b>Haven't submitted</b></h5>
                                </c:when>
                                <c:otherwise>
                                    <h5 style="color:green"><b>Have submitted</b></h5>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <a href="edit?taskID=${s.getTaskID()}" class="btn btn-edit">Edit</a>
                            <a href="delete?taskID=${s.getTaskID()}" class="btn btn-delete">Delete</a>
                            
                        </td>
                    </tr> 
                </c:forEach>
            </table>
        </div>

        <a href="insert" class="btn btn-edit">Create new task</a>

        <!-- /.site-footer -->
        </div>
        <!-- /#right-panel -->

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Chart js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

        <!-- Chartist Chart -->
        <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-ui@1.12.1/jquery-ui.min.js"></script>
    </body>
</html>
