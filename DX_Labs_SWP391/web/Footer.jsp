<!--
    <footer class="bg-body-tertiary text-center text-lg-start">
   Grid container 
  <div class="container p-4">
    Grid row
    <div class="row">
      Grid column
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0 text-start">
  <h5 class="text-uppercase">ABOUT US</h5>
  <p>
    The Intern Management Project builds a system to help companies track and manage information, work progress, and evaluate intern performance. Main functions include managing interns, updating information, managing records, assigning and tracking tasks, and evaluating results.
  </p>
</div>


      Grid column

      Grid column
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
           Links 
          <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
          <p> HA NOI, HOA LAC, VN</p>
          <p>
           
            info@example.com
          </p>
          <p></i> + 01 234 567 88</p>
          <p> + 01 234 567 89</p>
        </div>
      Grid column
    </div>
    Grid row
  </div>
   Grid container 

   Copyright 
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.05);">
     2020 Copyright:
    <a class="text-body" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
   Copyright 
</footer>
-->
 <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; FPT UNIVERSITY
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="#">GROUP 3</a>
                    </div>
                </div>
            </div>
        </footer>
