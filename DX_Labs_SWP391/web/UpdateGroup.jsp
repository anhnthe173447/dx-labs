<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Update Group</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
        <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #F1F2F7;
                color: #333;
                margin: 0;
                padding: 20px;
            }
            h1 {
                color: #005b96;
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
            }
            th, td {
                padding: 12px;
                text-align: left;
            }
            th {
                color: black;
                font-weight: bold;
            }
            tr:nth-child(even) {
                background-color: #f2faff;
            }
            td {
                border-bottom: 1px solid #ddd;
            }
            .btn {
                padding: 5px 10px;
                margin: 2px;
                border: none;
                cursor: pointer;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s ease;
                display: inline-block;
            }
            .btn-edit {
                background-color: #4CAF50;
                color: white;
            }
            .btn-edit:hover {
                background-color: #45a049;
            }
            .btn-delete {
                background-color: #f44336;
                color: white;
            }
            .btn-delete:hover {
                background-color: #da190b;
            }
            .btn-view {
                background-color: #78909c;
                color: white;
            }
            .btn-view:hover {
                background-color: #ddd;
            }
            .action-buttons {
                display: flex;
            }
            .action-buttons a {
                margin-right: 10px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="LeftPanel.jsp"></jsp:include>           
            <jsp:include page="rightandheader.jsp"></jsp:include>
            <div class="content" style="background: white">
                    <h1>Update Group</h1>

                <c:set var="group" value="${requestScope.group}"/>
                <form action="updateGroup" method="post" class="form-group">
                    <div class="form-group">
                        <label for="group_id">Group ID:</label>
                        <input type="number" readonly class="form-control" name="group_id" id="group_id" value="${group.group_id}"/>
                    </div>
                    <div class="form-group">
                        <label for="group_name">Group Name:</label>
                        <input type="text" class="form-control" name="group_name" id="group_name" value="${group.group_name}" required/>
                    </div>
                    <input type="hidden" name="mentor_id" value="${group.mentor_id}"/>

                    <input type="submit" class="btn btn-primary" value="UPDATE"/>

                    <c:if test="${not empty sessionScope.error}">
                        <p style="color:red">${sessionScope.error}</p>
                    </c:if>
                </form>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>